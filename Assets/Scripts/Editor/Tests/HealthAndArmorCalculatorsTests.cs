﻿using System.Collections;
using System.Collections.Generic;
using Harmony;
using UnityEngine;
using NUnit.Framework;
using UnityEditor.SceneManagement;
using UnityEngine.SceneManagement;

namespace ProjetSynthese
{
    [TestFixture]
    public class HealthAndArmorCalculatorsTests
    {
        private HealthCalculator healthCalculator;
        private ArmorCalculator armorCalculator;

        [SetUp]
        public void SetUp()
        {
            healthCalculator = new HealthCalculator(100);
            armorCalculator = new ArmorCalculator(100);
        }

        //Health calculator tests.

        [Test]
        public void HealthCalculatorShouldLose10HPWhenReceives10Damages()
        {
            healthCalculator.ResetHealth(100);
            int hp = healthCalculator.UnitCurrentHealth;
            healthCalculator.ReceiveDamages(10);
            Assert.AreEqual(hp-10, healthCalculator.UnitCurrentHealth);
        }
        [Test]
        public void HealthCalculatorShouldReturnTrueToDeadWhenHPBelowZero()
        {
            healthCalculator.ResetHealth(18);
            healthCalculator.ReceiveDamages(10);
            Assert.AreEqual(false,healthCalculator.isDead());
            healthCalculator.ReceiveDamages(10);
            Assert.AreEqual(true, healthCalculator.isDead());
        }
        [Test]
        public void HealthCalculatorShouldGain10HPWhenReceives10Healing()
        {
            healthCalculator.ResetHealth(100);
            healthCalculator.ReceiveDamages(50);
            int hp = healthCalculator.UnitCurrentHealth;
            healthCalculator.ReceiveHealing(10);
            Assert.AreEqual(hp + 10, healthCalculator.UnitCurrentHealth);
        }

        [Test]
        public void HealthCanNotGoAboveMaximum()
        {
            healthCalculator.ResetHealth(100);
            int hp = healthCalculator.UnitCurrentHealth;
            healthCalculator.ReceiveHealing(10);
            Assert.AreEqual(healthCalculator.UnitMaxHealth, healthCalculator.UnitCurrentHealth);
        }

        [Test]
        public void HealthBonusShouldIncreaseMaxHealth()
        {
            healthCalculator.ResetHealth(100);
            healthCalculator.AddBonusMaxHealth(50);
            Assert.AreEqual(150, healthCalculator.UnitMaxHealth);
        }
        [Test]
        public void HealthBonusCanBeRemoved()
        {
            healthCalculator.ResetHealth(100);
            healthCalculator.AddBonusMaxHealth(50);
            healthCalculator.RemoveBonusMaxHealth(50);
            Assert.AreEqual(100, healthCalculator.UnitMaxHealth);
        }
        [Test]
        public void HealthPercentageBonusShouldIncreaseMaxHealth()
        {
            healthCalculator.ResetHealth(200);
            healthCalculator.AddMaxHealthPercentage(50);
            Assert.AreEqual(300, healthCalculator.UnitMaxHealth);
        }
        [Test]
        public void HealthPercentagesShouldAppliesBeforeHealthBonusInCalculation()
        {
            healthCalculator.ResetHealth(50);
            healthCalculator.AddBonusMaxHealth(50);
            healthCalculator.AddMaxHealthPercentage(100);
            Assert.AreEqual(150, healthCalculator.UnitMaxHealth);
        }

        //Armor calculator tests

        [Test]
        public void ArmorShouldCalculatorLoses5PointsAndBlocks5WhenFullAndReceives10Damages()
        {
            armorCalculator.ResetArmor(100);
            int ap = armorCalculator.UnitCurrentArmor;
            armorCalculator.AbsorbDamages(10);
            Assert.AreEqual(ap - 5, armorCalculator.UnitCurrentArmor);
        }

        [Test]
        public void ArmorShouldCalculatorAbsorbs4PointsAndBlocks5WhenAt80AndReceives10Damages()
        {
            armorCalculator.ResetArmor(80);
            int ap = armorCalculator.UnitCurrentArmor;
            int damage = armorCalculator.AbsorbDamages(10);
            Assert.AreEqual(ap - 4, armorCalculator.UnitCurrentArmor);
            Assert.AreEqual(1,damage);
        }

        [Test]
        public void ArmorShouldCalculatorAbsorbs100PointsAndBlock5WhenAt100AndReceives200Damages()
        {
            armorCalculator.ResetArmor(100);
            int damage = armorCalculator.AbsorbDamages(200);
            Assert.AreEqual(0, armorCalculator.UnitCurrentArmor);
            Assert.AreEqual(95, damage);
        }

        [Test]
        public void ArmorShouldAbosrbsLessDamageAfterEachHit()
        {
            armorCalculator.ResetArmor(100);
            int damage = armorCalculator.AbsorbDamages(25);
            Assert.AreEqual(80, armorCalculator.UnitCurrentArmor);
            Assert.AreEqual(0, damage);
            damage = armorCalculator.AbsorbDamages(25);
            Assert.AreEqual(64, armorCalculator.UnitCurrentArmor);
            Assert.AreEqual(4, damage);
        }

        [Test]
        public void ArmorShouldBeReparedWhenRepairIsCalled()
        {
            armorCalculator.ResetArmor(100);
            armorCalculator.AbsorbDamages(25);
            armorCalculator.Repair(10);
            Assert.AreEqual(90,armorCalculator.UnitCurrentArmor);
        }

        [Test]
        public void ArmorCannotBeReparedAboveMaximum()
        {
            armorCalculator.ResetArmor(100);
            armorCalculator.AbsorbDamages(25);
            armorCalculator.Repair(200);
            Assert.AreEqual(100, armorCalculator.UnitCurrentArmor);
        }

        [Test]
        public void ArmorBonusBeIncreasesMaximumArmor()
        {
            armorCalculator.ResetArmor(100);
            armorCalculator.AddBonusMaxArmor(20);
            Assert.AreEqual(120, armorCalculator.UnitMaxArmor);
        }

        [Test]
        public void ArmorBonusCanBeRemoved()
        {
            armorCalculator.ResetArmor(100);
            armorCalculator.AddBonusMaxArmor(20);
            armorCalculator.RemoveBonusMaxArmor(20);
            Assert.AreEqual(100, armorCalculator.UnitMaxArmor);
        }

        [Test]
        public void ArmorPercentageBonusShouldIncreaseArmorByPercentage()
        {
            armorCalculator.ResetArmor(50);
            armorCalculator.AddMaxArmorPercentage(20);
            Assert.AreEqual(60, armorCalculator.UnitMaxArmor);
        }

        [Test]
        public void ArmorPercentageBonusShouldApplyBeforeFlatBonus()
        {
            armorCalculator.ResetArmor(50);
            armorCalculator.AddBonusMaxArmor(10);
            armorCalculator.AddMaxArmorPercentage(20);
            armorCalculator.AddBonusMaxArmor(10);
            Assert.AreEqual(80, armorCalculator.UnitMaxArmor);
        }
    }
    
}
