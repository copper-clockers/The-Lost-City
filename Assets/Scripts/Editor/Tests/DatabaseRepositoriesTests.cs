﻿using System.Collections;
using System.Collections.Generic;
using Harmony;
using UnityEngine;
using NUnit.Framework;
using UnityEditor.SceneManagement;
using UnityEngine.SceneManagement;
using UnityEngine.TestTools;

namespace ProjetSynthese
{
    [TestFixture]
    public class DatabaseRepositoriesTests
    {
        private LevelRepository levelRepository;
        private AdventurerClassRepository adventurerClassRepository;
        private AchievementRepository achievementRepository;
        private GameScoreRepository gameScoreRepository;
        private TeamMemberRepository teamMemberRepository;
        private SqLiteConnectionFactory sqLiteConnectionFactory;
        private SqLiteParameterFactory sqLiteParameterFactory;

        /// <summary>
        /// Attention! Il est nécessaire de supprimer le fichier database dans Appdata a 
        /// chaque fois que l'on roule les tests!
        /// Si vous avez exécuté les tests et que vous voullez rouler le jeu vous devez aussi
        /// supprimer le fichier.
        /// </summary>
        [SetUp]
        public void SetUp()
        {
            EditorSceneManager.NewScene(NewSceneSetup.EmptyScene);

            GameObject appDependencies = new GameObject();
            appDependencies.tag = R.S.Tag.ApplicationDependencies;
            appDependencies.AddComponent<SqLiteConnectionFactory>();
            appDependencies.AddComponent<SqLiteParameterFactory>();
            sqLiteConnectionFactory = appDependencies.GetComponent<SqLiteConnectionFactory>();
            sqLiteConnectionFactory.UnitForTest();
            sqLiteParameterFactory = appDependencies.GetComponent<SqLiteParameterFactory>();

            GameObject repositories = new GameObject();
            repositories.AddComponent<LevelRepository>();
            repositories.AddComponent<AdventurerClassRepository>();
            repositories.AddComponent<AchievementRepository>();
            repositories.AddComponent<GameScoreRepository>();
            repositories.AddComponent<TeamMemberRepository>();

            levelRepository = repositories.GetComponent<LevelRepository>();
            adventurerClassRepository = repositories.GetComponent<AdventurerClassRepository>();
            achievementRepository = repositories.GetComponent<AchievementRepository>();
            gameScoreRepository = repositories.GetComponent<GameScoreRepository>();
            teamMemberRepository = repositories.GetComponent<TeamMemberRepository>();
        }

        #region Achievements
        [Test]
        public void TestSelectAndActivateAchievement()
        {
            achievementRepository.InjectForTests(sqLiteConnectionFactory, sqLiteParameterFactory);
            Achievement achievement = achievementRepository.GetAnAchievement(2);
            Assert.IsTrue(achievement.Name == "Alan Grant");
            Assert.IsTrue(achievement.Description == "For not being machine compatible");
            //Assert.IsTrue(!achievement.IsUnlocked); //mettre cette ligne en commentaire si vous ne voulez pas avoir à supprimer le fichier...
            achievementRepository.UnlockAchievement(2);
            achievement = achievementRepository.GetAnAchievement(2);
            Assert.IsTrue(achievement.IsUnlocked);
        }
        #endregion

        #region Adventurers
        [Test]
        public void TestSelectAnAdventurerMachinist()
        {
            adventurerClassRepository.InjectForTests(sqLiteConnectionFactory,sqLiteParameterFactory);
            AdventurerClass adventurerClass = adventurerClassRepository.GetAnAdventurerClass("Machinist");
            Assert.IsTrue(adventurerClass.NameAdventurer.Equals("Mattie Laura Bennet"));
        }

        [Test]
        public void TestSelectAnAdventurerEnforcer()
        {
            adventurerClassRepository.InjectForTests(sqLiteConnectionFactory, sqLiteParameterFactory);
            AdventurerClass adventurerClass = adventurerClassRepository.GetAnAdventurerClass("Enforcer");
            Assert.IsTrue(adventurerClass.NameAdventurer.Equals("Cole Hannibal Flint"));
        }

        [Test]
        public void TestSelectAnAdventurerSpecter()
        {
            adventurerClassRepository.InjectForTests(sqLiteConnectionFactory, sqLiteParameterFactory);
            AdventurerClass adventurerClass = adventurerClassRepository.GetAnAdventurerClass("Specter");
            Assert.IsTrue(adventurerClass.NameAdventurer.Equals("Eudora Permelia Sparks"));
        }

        [Test]
        public void TestSelectAnAdventurerScientist()
        {
            adventurerClassRepository.InjectForTests(sqLiteConnectionFactory, sqLiteParameterFactory);
            AdventurerClass adventurerClass = adventurerClassRepository.GetAnAdventurerClass("Scientist");
            Assert.IsTrue(adventurerClass.NameAdventurer.Equals("Gilbert Cyrus Kingsford"));
        }
        #endregion

        #region Level
        [Test]
        public void TestSelectALevelOne()
        {
            levelRepository.InjectForTests(sqLiteConnectionFactory, sqLiteParameterFactory);
            Level level = levelRepository.GetALevelFromId(1);
            Assert.IsTrue(level.NameLevel.Equals("Cave of the Lost"));
        }

        [Test]
        public void TestSelectALevelTwo()
        {
            levelRepository.InjectForTests(sqLiteConnectionFactory, sqLiteParameterFactory);
            Level level = levelRepository.GetALevelFromId(2);
            Assert.IsTrue(level.NameLevel.Equals("Cave of Trials"));
        }

        [Test]
        public void TestSelectALevelThree()
        {
            levelRepository.InjectForTests(sqLiteConnectionFactory, sqLiteParameterFactory);
            Level level = levelRepository.GetALevelFromId(3);
            Assert.IsTrue(level.NameLevel.Equals("Cave of Inferno"));
        }

        [Test]
        public void TestSelectALevelFour()
        {
            levelRepository.InjectForTests(sqLiteConnectionFactory, sqLiteParameterFactory);
            Level level = levelRepository.GetALevelFromId(4);
            Assert.IsTrue(level.NameLevel.Equals("Lair of the Leviathan"));
        }
        #endregion

        #region GameScore
        [Test]
        public void TestCreateAGameScore()
        {
            levelRepository.InjectForTests(sqLiteConnectionFactory, sqLiteParameterFactory);
            gameScoreRepository.InjectForTests(sqLiteConnectionFactory, sqLiteParameterFactory, levelRepository);
            GameScore gameScore = new GameScore();
            gameScore.TimeGame = "TEST - 00:03:00";
            gameScore.GameDuration = "00:02:03";
            gameScore.GameWon = true;
            gameScore.MetalQuantityGathered = 10;
            gameScore.MetalQuantitySpent = 32;
            gameScore.NbConstructedTurret = 32;
            gameScore.NbDestructedTurret = 102;
            gameScore.NbConstructedExtractor = 84;
            gameScore.NbDestructedExtractor = 12;
            gameScore.NbEnemiesKilled = 5;
            gameScore.LevelName = "Cave of the Lost";

            gameScoreRepository.AddGameScore(gameScore);

            GameScore gameScoreTest = gameScoreRepository.GetAGameScore((int)gameScore.Id);

            Assert.IsTrue(gameScoreTest.TimeGame.Equals(gameScore.TimeGame));
            Assert.IsTrue(gameScoreTest.LevelName.Equals("Cave of the Lost"));
        }
        #endregion

        #region TeamMember
        [Test]
        public void TestCreateATeamMember()
        {
            levelRepository.InjectForTests(sqLiteConnectionFactory, sqLiteParameterFactory);
            gameScoreRepository.InjectForTests(sqLiteConnectionFactory, sqLiteParameterFactory, levelRepository);
            teamMemberRepository.InjectForTests(sqLiteConnectionFactory, sqLiteParameterFactory);
            adventurerClassRepository.InjectForTests(sqLiteConnectionFactory, sqLiteParameterFactory);
            GameScore gameScore = new GameScore();
            gameScore.TimeGame = "TEST - 00:03:00";
            gameScore.GameDuration = "00:02:03";
            gameScore.GameWon = true;
            gameScore.MetalQuantityGathered = 10;
            gameScore.MetalQuantitySpent = 32;
            gameScore.NbConstructedTurret = 32;
            gameScore.NbDestructedTurret = 102;
            gameScore.NbConstructedExtractor = 84;
            gameScore.NbDestructedExtractor = 12;
            gameScore.NbEnemiesKilled = 5;
            gameScore.LevelName = "Cave of the Lost";

            gameScoreRepository.AddGameScore(gameScore);

            TeamMember teamMember = new TeamMember();
            teamMember.IdAdventurerClass = adventurerClassRepository.GetAnAdventurerClass("Machinist").Id;
            teamMember.IdGameScore = gameScore.Id;
            teamMember.LivingDuration = "00:00:10";
            teamMember.WasKilled = true;

            teamMemberRepository.AddTeamMember(teamMember);

            IList<TeamMember> teamMemberTest = teamMemberRepository.GetAllTeamMembers((int)gameScore.Id);

            Assert.IsTrue(teamMemberTest.Count == 1);
        }
        #endregion
    }
}