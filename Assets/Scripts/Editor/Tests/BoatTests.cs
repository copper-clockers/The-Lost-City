﻿using NUnit.Framework;
using UnityEditor.SceneManagement;
using UnityEngine.SceneManagement;
using UnityEngine;
using Harmony;
using System.Collections;
using System.Collections.Generic;

namespace ProjetSynthese
{
    [TestFixture]
    public class BoatTests
    {
        public const string CaveOfInfernoPath = "Assets/Scenes/Levels/CaveOfInferno.unity";


        private GameObject[] gameObjects;
        private GameObject[] boats;
        private GameObject lavaRiver;
        private GameObject enemy;
        private List<GameObject> lavaTiles;


        [SetUp]
        public void SetUp()
        {
            Scene usedScene = EditorSceneManager.OpenScene(CaveOfInfernoPath);
            gameObjects = usedScene.GetRootGameObjects();
            boats = new GameObject[4];
            int boatIncrementor = 0;

            for (int i = 0; i < gameObjects.Length; i++)
            {
                if (gameObjects[i].tag == R.S.Tag.Boat)
                {
                    boats[boatIncrementor] = gameObjects[i];
                    boatIncrementor++;
                }
                if (gameObjects[i].GetComponentInChildren<StayOnLava>() != null)
                {
                    lavaRiver = gameObjects[i];
                }
                if(gameObjects[i].layer == LayerMask.NameToLayer("Enemy"))
                {
                    enemy = gameObjects[i];
                }
            }

        }

        [Test]
        public void BoatStartsOnRightStartTile()
        {
            boats[0].GetComponentInChildren<BoatController>().StartTileID = 0;
            GameObject lavaTile = lavaRiver.transform.GetChild(0).gameObject;

            Assert.AreEqual(boats[0].transform.position, lavaTile.transform.position);
        }
        
        [Test]
        public void BoatChangesDestination()
        {
            boats[0].GetComponentInChildren<BoatController>().StartTileID = 0;
            BoatController boatController = boats[0].GetComponentInChildren<BoatController>();
            GameObject nextLavaTile = lavaRiver.transform.GetChild(1).gameObject;

            Assert.AreNotEqual(boatController.Destination, nextLavaTile);
        }
        
    }
}
