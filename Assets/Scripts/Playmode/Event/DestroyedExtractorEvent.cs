﻿using Harmony;

namespace ProjetSynthese
{
    public class DestroyedExtractorEvent : IEvent
    {
        public int ExtractorAmountDestroyed { get; private set; }

        public DestroyedExtractorEvent()
        {
            ExtractorAmountDestroyed = 1;
        }
    }
}