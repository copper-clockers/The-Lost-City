﻿using Harmony;
using UnityEngine;

namespace ProjetSynthese
{
    [AddComponentMenu("Game/Event/CreatedTurretEventChannel")]
    public class CreatedTurretEventChannel : EventChannel<CreatedTurretEvent>
    {
    }
}