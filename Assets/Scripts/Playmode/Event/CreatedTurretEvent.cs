﻿using Harmony;

namespace ProjetSynthese
{
    public class CreatedTurretEvent : IEvent
    {
        public int TurretAmountCreated { get; private set; }

        public CreatedTurretEvent()
        {
            TurretAmountCreated = 1;
        }
    }
}