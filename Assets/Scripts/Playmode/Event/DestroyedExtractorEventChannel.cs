﻿using Harmony;
using UnityEngine;

namespace ProjetSynthese
{
    [AddComponentMenu("Game/Event/DestroyedExtractorEventChannel")]
    public class DestroyedExtractorEventChannel : EventChannel<DestroyedExtractorEvent>
    {
    }
}