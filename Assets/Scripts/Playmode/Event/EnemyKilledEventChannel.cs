﻿using Harmony;
using UnityEngine;

namespace ProjetSynthese
{
    [AddComponentMenu("Game/Event/EnemyKilledEventChannel")]
    public class EnemyKilledEventChannel : EventChannel<EnemyKilledEvent>
    {
    }
}