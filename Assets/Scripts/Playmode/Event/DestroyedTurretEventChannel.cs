﻿using Harmony;
using UnityEngine;

namespace ProjetSynthese
{
    [AddComponentMenu("Game/Event/DestroyedTurretEventChannel")]
    public class DestroyedTurretEventChannel : EventChannel<DestroyedTurretEvent>
    {
    }
}