﻿using Harmony;

namespace ProjetSynthese
{
    public class EnemyKilledEvent : IEvent
    {
        public int EnemyAmountKilled { get; private set; }

        public EnemyKilledEvent()
        {
            EnemyAmountKilled = 1;
        }
    }
}