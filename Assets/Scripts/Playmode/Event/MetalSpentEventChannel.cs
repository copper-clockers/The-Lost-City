﻿using Harmony;
using UnityEngine;

namespace ProjetSynthese
{
    [AddComponentMenu("Game/Event/MetalSpentEventChannel")]
    public class MetalSpentEventChannel : EventChannel<MetalSpentEvent>
    {
    }
}