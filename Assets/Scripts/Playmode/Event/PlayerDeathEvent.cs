﻿using Harmony;

namespace ProjetSynthese
{
    public class PlayerDeathEvent : IEvent
    {
        public int PlayerNumber { get; private set; }
        public bool PlayerDead { get; private set; }

        public PlayerDeathEvent(int playerNumber)
        {
            PlayerNumber = playerNumber;
            PlayerDead = true;
        }
    }
}
