﻿using Harmony;
using UnityEngine;

namespace ProjetSynthese
{
    [AddComponentMenu("Game/Event/ArtefactPickedUpEventChannel")]
    public class ArtefactPickedUpEventChannel : EventChannel<ArtefactPickedUpEvent>
    {
    }
}