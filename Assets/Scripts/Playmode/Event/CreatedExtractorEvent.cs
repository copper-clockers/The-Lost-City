﻿using Harmony;

namespace ProjetSynthese
{
    public class CreatedExtractorEvent : IEvent
    {
        public int ExtractorAmountBuilded { get; private set; }

        public CreatedExtractorEvent()
        {
            ExtractorAmountBuilded=1;
        }
    }
}