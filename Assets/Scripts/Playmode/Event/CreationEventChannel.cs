﻿using Harmony;
using UnityEngine;

namespace ProjetSynthese
{ 
    [AddComponentMenu("Game/Event/CreationEventChannel")]
    public class CreationEventChannel : EventChannel<CreationEvent>
    {
    }
}