﻿using Harmony;

namespace ProjetSynthese
{
    public class MetalSpentEvent : IEvent
    {
        public int MetalAmountSpent { get; private set; }

        public MetalSpentEvent(int amountOfMetal)
        {
            MetalAmountSpent = amountOfMetal;
        }
    }
}