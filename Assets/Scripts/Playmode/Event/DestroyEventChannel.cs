﻿using Harmony;
using UnityEngine;

namespace ProjetSynthese
{
    [AddComponentMenu("Game/Event/DestroyEventChannel")]
    public class DestroyEventChannel : EventChannel<DestroyEvent>
    {
    }
}