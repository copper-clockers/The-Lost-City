﻿using Harmony;
using UnityEngine;

namespace ProjetSynthese
{
    [AddComponentMenu("Game/Event/DepositExtractionEventChannel")]
    public class DepositExtractionEventChannel : EventChannel<DepositExtractionEvent>
    {
    }
}


