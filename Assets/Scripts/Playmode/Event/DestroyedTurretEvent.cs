﻿using Harmony;

namespace ProjetSynthese
{
    public class DestroyedTurretEvent : IEvent
    {
        public int TurretAmountDestroyed { get; private set; }

        public DestroyedTurretEvent()
        {
            TurretAmountDestroyed = 1;
        }
    }
}