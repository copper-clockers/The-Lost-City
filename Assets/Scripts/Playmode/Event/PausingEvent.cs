﻿using Harmony;
using UnityEngine;

namespace ProjetSynthese
{
    public class PausingEvent : IEvent
    {
        public bool isPaused;
        public PausingEvent(bool isPaused)
        {
            this.isPaused = isPaused;
        }
    }
}