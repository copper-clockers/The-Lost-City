﻿using UnityEngine;
using Harmony;

namespace ProjetSynthese
{
    [AddComponentMenu("Game/Event/CreatedExtractorEventChannel")]
    public class CreatedExtractorEventChannel : EventChannel<CreatedExtractorEvent>
    {
    }
}