﻿using Harmony;
using UnityEngine;

namespace ProjetSynthese
{
    [AddComponentMenu("Game/Event/PausingEventChannel")]
    public class PausingEventChannel : EventChannel<PausingEvent>
    {
    }
}


