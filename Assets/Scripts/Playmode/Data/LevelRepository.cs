﻿using System.Collections;
using System.Collections.Generic;
using Harmony;
using JetBrains.Annotations;
using UnityEngine;

namespace ProjetSynthese
{
    [AddComponentMenu("Game/Data/LevelRepository")]
    public class LevelRepository : GameScript
    {
        private Repository repository;

        public void InjectLevelRepository([ApplicationScope] IDbConnectionFactory connectionFactory,
            [ApplicationScope] IDbParameterFactory parameterFactory)
        {
            repository = new Repository(this, connectionFactory, parameterFactory, new LevelMapper());
        }

        public void Awake()
        {
            InjectDependencies("InjectLevelRepository");
        }

        public void InjectForTests(IDbConnectionFactory connectionFactory, IDbParameterFactory parameterFactory)
        {
            repository = new Repository(this, connectionFactory, parameterFactory, new LevelMapper());
        }

        public virtual Level GetALevelFromName(string levelName)
        {
            return repository.GetALevelFromName(levelName);
        }

        public virtual Level GetALevelFromId(long levelId)
        {
            return repository.GetALevelFromId(levelId);
        }

        public virtual IList<Level> GetAllLevels()
        {
            return repository.GetAllLevels();
        }

        #region Repository
        private class Repository : DbRepository<Level>
        {
            private readonly LevelRepository levelRepository;

            public Repository(LevelRepository levelRepository,
                [NotNull] IDbConnectionFactory connectionFactory,
                [NotNull] IDbParameterFactory parameterFactory,
                [NotNull] IDbDataMapper<Level> dataMapper)
                : base(connectionFactory, parameterFactory, dataMapper)
            {
                this.levelRepository = levelRepository;
            }

            public Level GetALevelFromName(string levelName)
            {
                return ExecuteSelectOne("SELECT * FROM Level WHERE nameLevel=?;", new object[] { levelName });
            }

            public Level GetALevelFromId(long levelId)
            {
                return ExecuteSelectOne("SELECT * FROM Level WHERE id=?;", new object[] { levelId });
            }

            public IList<Level> GetAllLevels()
            {
                return ExecuteSelectAll("SELECT * FROM Level;", new object[] { });
            }
        }
        #endregion
    }
}
