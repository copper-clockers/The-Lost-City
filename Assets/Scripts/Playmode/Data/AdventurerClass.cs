﻿namespace ProjetSynthese
{
    public class AdventurerClass
    {
        public long Id { get; set; }
        public string NameClass { get; set; }
        public string NameAdventurer { get; set; }
        public string MeleeWeapon { get; set; }
        public string RangedWeaponOne { get; set; }
        public string RangedWeaponTwo { get; set; }
        public string SkillOne { get; set; }
        public string SkillTwo { get; set; }
        public string StoryAdventurer { get; set; }
    }
}