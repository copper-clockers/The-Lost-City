﻿using System.Collections;
using System.Collections.Generic;
using Harmony;
using JetBrains.Annotations;
using UnityEngine;

namespace ProjetSynthese
{
    [AddComponentMenu("Game/Data/AchievementRepository")]
    public class AchievementRepository : GameScript
    {
        private Repository repository;

        public void InjectAchievementRepository([ApplicationScope] IDbConnectionFactory connectionFactory,
                                                [ApplicationScope] IDbParameterFactory parameterFactory)
        {
            repository = new Repository(this, connectionFactory, parameterFactory, new AchievementMapper());
        }

        public void Awake()
        {
            InjectDependencies("InjectAchievementRepository");
        }

        public void InjectForTests(IDbConnectionFactory connectionFactory, IDbParameterFactory parameterFactory)
        {
            repository = new Repository(this, connectionFactory, parameterFactory, new AchievementMapper());
        }

        public virtual Achievement GetAnAchievement(int achievementId)
        {
            return repository.GetAnAchievement(achievementId);
        }

        public virtual void UnlockAchievement(int achievementId)
        {
            repository.UnlockAchievement(achievementId);
            Debug.Log("achievement " + achievementId + " is unlocked!");
        }

        public virtual IList<Achievement> GetAllAchievements()
        {
            return repository.GetAllAchievements();
        }

        #region Repository
        private class Repository : DbRepository<Achievement>
        {
            private readonly AchievementRepository achievementRepository;

            public Repository(AchievementRepository achievementRepository,
                [NotNull] IDbConnectionFactory connectionFactory,
                [NotNull] IDbParameterFactory parameterFactory,
                [NotNull] IDbDataMapper<Achievement> dataMapper)
                : base(connectionFactory, parameterFactory, dataMapper)
            {
                this.achievementRepository = achievementRepository;
            }

            public void UnlockAchievement(int achievementId)
            {
                ExecuteUpdate("UPDATE Achievement SET is_unlocked = 1 WHERE id = ?;", new object[]
                {
                    achievementId
                });
            }

            public Achievement GetAnAchievement(int achievementId)
            {
                return ExecuteSelectOne("SELECT * FROM Achievement WHERE id = ?;", new object[] { achievementId });
            }

            public IList<Achievement> GetAllAchievements()
            {
                return ExecuteSelectAll("SELECT * FROM Achievement;", new object[] { });
            }
        }
        #endregion
    }
}
