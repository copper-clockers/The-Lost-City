﻿namespace ProjetSynthese
{
    public class Level
    {
        public long Id { get; set; }
        public string NameLevel { get; set; }
        public string DescriptionLevel { get; set; }
    }
}
