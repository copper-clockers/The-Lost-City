﻿namespace ProjetSynthese
{
    public class HighScore
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public uint ScorePoints { get; set; }
    }
}