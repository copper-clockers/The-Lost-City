﻿using System;
using System.Collections;
using System.Collections.Generic;
using Harmony;
using JetBrains.Annotations;
using UnityEngine;

namespace ProjetSynthese
{
    [AddComponentMenu("Game/Data/GameScoreRepository")]
    public class GameScoreRepository : GameScript
    {
        [SerializeField]
        [Tooltip("Quantity of game scores to keep.")]
        private uint nbGameScoresToKeep = 10;

        private Repository repository;

        public void InjectGameScoreRepository([ApplicationScope] IDbConnectionFactory connectionFactory,
                                              [ApplicationScope] IDbParameterFactory parameterFactory,
                                              [GameObjectScope] LevelRepository levelRepository)
        {
            repository = new Repository(this, levelRepository, connectionFactory, parameterFactory, new GameScoreMapper(levelRepository));
        }

        public void Awake()
        {
            InjectDependencies("InjectGameScoreRepository");
        }

        public void InjectForTests(IDbConnectionFactory connectionFactory, IDbParameterFactory parameterFactory, LevelRepository levelRepository)
        {
            repository = new Repository(this, levelRepository, connectionFactory, parameterFactory, new GameScoreMapper(levelRepository));
        }

        public virtual void AddGameScore(GameScore gameScore)
        {
            if (IsLeaderboardFull())
            {
                repository.DeleteTooOldScores();
            }
            repository.AddGameScore(gameScore);
        }

        public virtual bool IsLeaderboardFull()
        {
            return repository.Count() >= nbGameScoresToKeep;
        }

        public virtual IList<GameScore> GetAllGameScores()
        {
            return repository.GetAllGameScores();
        }

        public virtual GameScore GetAGameScore(int index)
        {
            return repository.GetAGameScore(index);
        }

        #region Repository
        private class Repository : DbRepository<GameScore>
        {
            private readonly GameScoreRepository gameScoreRepository;
            private readonly LevelRepository levelRepository;

            public Repository(GameScoreRepository gameScoreRepository,
                LevelRepository levelRepository,
                [NotNull] IDbConnectionFactory connectionFactory,
                [NotNull] IDbParameterFactory parameterFactory,
                [NotNull] IDbDataMapper<GameScore> dataMapper)
                : base(connectionFactory, parameterFactory, dataMapper)
            {
                this.gameScoreRepository = gameScoreRepository;
                this.levelRepository = levelRepository;
            }

            public void AddGameScore(GameScore gameScore)
            {
                gameScore.Id = ExecuteInsert("INSERT INTO GameScore (timeGame,gameDuration,gameWon,metalQuantityGathered,metalQuantitySpent," +
                                             "nbConstructedTurret,nbDestructedTurret,nbConstructedExtractor,nbDestructedExtractor," +
                                             "nbEnemiesKilled,fkIdLevel) " +
                                             "VALUES (?,?,?,?,?,?,?,?,?,?,?);", new object[]
                {
                    gameScore.TimeGame,
                    gameScore.GameDuration,
                    gameScore.GameWon ? 1L : 0L,
                    gameScore.MetalQuantityGathered,
                    gameScore.MetalQuantitySpent,
                    gameScore.NbConstructedTurret,
                    gameScore.NbDestructedTurret,
                    gameScore.NbConstructedExtractor,
                    gameScore.NbDestructedExtractor,
                    gameScore.NbEnemiesKilled,
                    levelRepository.GetALevelFromName(gameScore.LevelName).Id
                });
            }

            public IList<GameScore> GetAllGameScores()
            {
                return ExecuteSelectAll("SELECT * FROM GameScore;", new object[] { });
            }

            public void DeleteTooOldScores()
            {
                GameScore scoreToDelete = GetLowestGameScore();

                ExecuteDelete("DELETE FROM GameScore WHERE id=?", new object[] { scoreToDelete.Id });
            }

            public GameScore GetAGameScore(int index)
            {
                return ExecuteSelectOne("SELECT * FROM GameScore WHERE id=?", new object[] { index });
            }

            public long Count()
            {
                return ExecuteScallar("SELECT COUNT(*) FROM GameScore;", new object[] { });
            }

            private GameScore GetLowestGameScore()
            {
                IList<GameScore> allScores = GetAllGameScores();
                GameScore scoreToReturn = null;
                int lowestScore = Int32.MaxValue;

                foreach (GameScore score in allScores)
                {
                    int scorePoint = 0;
                    if (score.GameWon)
                    {
                        scorePoint += 10000;
                    }
                    else
                    {
                        scorePoint -= 10000;
                    }
                    scorePoint += (int)score.MetalQuantityGathered;
                    scorePoint -= (int)score.MetalQuantitySpent;
                    scorePoint += (int)score.NbConstructedExtractor;
                    scorePoint -= (int)score.NbDestructedExtractor;
                    scorePoint += (int)score.NbConstructedTurret;
                    scorePoint -= (int)score.NbDestructedTurret;
                    scorePoint += (int)score.NbEnemiesKilled;
                    if (scorePoint < lowestScore)
                    {
                        lowestScore = scorePoint;
                        scoreToReturn = score;
                    }
                }
                return scoreToReturn;
            }
        }
        #endregion
    }
}