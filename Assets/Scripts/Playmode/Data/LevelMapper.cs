﻿using System.Data.Common;

namespace ProjetSynthese
{
    public class LevelMapper : SqLiteDataMapper<Level>
    {
        public override Level GetObjectFromReader(DbDataReader reader)
        {
            return new Level
            {
                Id = reader.GetInt32(reader.GetOrdinal("id")),
                NameLevel = reader.GetString(reader.GetOrdinal("nameLevel")),
                DescriptionLevel = reader.GetString(reader.GetOrdinal("descriptionLevel"))
            };

        }
    }
}
