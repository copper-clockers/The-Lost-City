﻿using UnityEngine;
using Harmony;

namespace ProjetSynthese
{
    public class PutGameOnPause : GameScript
    {
        [SerializeField]
        [Tooltip("The menu loaded when you pause the game.")]
        private Menu pauseMenu;

        private ActivityStack activityStack;

        private void InjectPutGameOnPause([ApplicationScope] ActivityStack activityStack)
        {
            this.activityStack = activityStack;
        }
        
	    private void Awake ()
        {
            InjectDependencies("InjectPutGameOnPause");
	    }
	
	    public void PauseGame()
        {
            activityStack.StartMenu(pauseMenu);
        }
    }
}
