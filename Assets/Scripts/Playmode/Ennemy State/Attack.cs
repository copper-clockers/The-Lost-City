﻿using System.Collections;
using System.Collections.Generic;
using Harmony;
using UnityEngine;

namespace ProjetSynthese
{
    [AddComponentMenu("Game/EnnemyState/Attack")]
    public class Attack : EnnemyState
    {
        public Attack(GameObject target, AiPath pathFinder, EnnemyController ennemy, List<GameObject> objectsDetected)
        {
            this.ennemy = ennemy;
            this.pathFinder = pathFinder;
            this.objectsDetected = objectsDetected;
            FindTargetWithMostPriority();
            pathFinder.PathfindingMethod = PathfindingMethod.Basic;
            this.target = target;
        }

        public override void OnPlayerEnterDetected(GameObject player)
        {
            if (!isObjectAlreadyThere(player))
            {
                objectsDetected.Add(player);
            }
            FindTargetWithMostPriority();
            if (target == null)
            {
                if (ennemy.StartState == EnnemyController.StartingStates.Patrol)
                {
                    ennemy.State = new Patrol(ennemy.PatrolPoints, pathFinder, ennemy, objectsDetected);
                }
                else
                {
                    target = ennemy.MainTarget;
                    ChooseAttackOrChase();
                }
            }
            else
            {
                ChooseAttackOrChase();
            }
        }

        public override void OnPlayerExitDetected(GameObject player)
        {
            objectsDetected.Remove(player);
            FindTargetWithMostPriority();
            if (target == null)
            {
                if (ennemy.StartState == EnnemyController.StartingStates.Patrol)
                {
                    ennemy.State = new Patrol(ennemy.PatrolPoints, pathFinder, ennemy, objectsDetected);
                }
                else
                {
                    ChooseAttackOrChase();
                }
            }

        }

        public override void OnPlayerInRangeDetected(GameObject player)
        {
            if (!isObjectAlreadyThere(player))
            {
                objectsDetected.Add(player);
            }
            FindTargetWithMostPriority();
            if (target == null)
            {
                if (ennemy.StartState == EnnemyController.StartingStates.Patrol)
                {
                    ennemy.State = new Patrol(ennemy.PatrolPoints, pathFinder, ennemy, objectsDetected);
                }
                else
                {
                    target = ennemy.MainTarget;
                    ChooseAttackOrChase();
                }
            }
        }

        public override void OnPlayerOutOfRangeDetected(GameObject player)
        {
            FindTargetWithMostPriority();
            if (target == null)
            {
                if (ennemy.StartState == EnnemyController.StartingStates.Patrol)
                {
                    ennemy.State = new Patrol(ennemy.PatrolPoints, pathFinder, ennemy, objectsDetected);
                }
                else
                {
                    target = ennemy.MainTarget;
                    ChooseAttackOrChase();
                }
            }
            else
            {
                if (player.Equals(target))
                {
                    ChooseAttackOrChase();
                }
            }
        }

        // Update is called once per frame
        public override void Update()
        {
            if (target == null || !target.activeInHierarchy)
            {
                objectsDetected.Remove(target);
                FindTargetWithMostPriority();
                if (target == null)
                {
                    if (ennemy.StartState == EnnemyController.StartingStates.Patrol)
                    {
                        ennemy.State = new Patrol(ennemy.PatrolPoints, pathFinder, ennemy, objectsDetected);
                    }
                    else
                    {
                        ChooseAttackOrChase();
                    }
                }
                else
                {
                    ChooseAttackOrChase();
                }
            }
            if (target != null && target.activeInHierarchy)
            {
                if (objectsDetected.Count > 0)
                {
                    bool isThereVisibleTarget = false;
                    for (int i = 0; i < objectsDetected.Count; i++)
                    {
                        if (!IsWalInbetween(objectsDetected[i].transform.position))
                        {
                            isThereVisibleTarget = true;
                            break;
                        }
                    }
                    if (isThereVisibleTarget)
                    {
                        FindTargetWithMostPriority();
                        if (target == null)
                        {
                            if (ennemy.StartState == EnnemyController.StartingStates.Patrol)
                            {
                                ennemy.State = new Patrol(ennemy.PatrolPoints, pathFinder, ennemy, objectsDetected);
                            }
                            else
                            {
                                target = ennemy.MainTarget;
                                ChooseAttackOrChase();
                            }
                        }
                        else
                        {
                            ChooseAttackOrChase();
                        }
                    }

                }
                ennemy.Rotate(target.transform.position);
                ennemy.Attack();
                ennemy.IsMoving(false);
            }            
        }
    }
}