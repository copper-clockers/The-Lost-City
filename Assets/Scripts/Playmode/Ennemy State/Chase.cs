﻿using System.Collections;
using System.Collections.Generic;
using Harmony;
using UnityEngine;

namespace ProjetSynthese
{
    [AddComponentMenu("Game/EnnemyState/Chase")]
    public class Chase : EnnemyState
    {

        private int nbNodesMoved;

        public Chase(GameObject target, AiPath pathFinder, EnnemyController ennemy, List<GameObject> objectsDetected, bool isPathPreCalculated)
        {
            this.ennemy = ennemy;
            this.pathFinder = pathFinder;
            this.objectsDetected = objectsDetected;
            if (!isObjectAlreadyThere(target))
            {
                objectsDetected.Add(target);
            }
            FindTargetWithMostPriority();

            if (!isPathPreCalculated)
            {
                pathFinder.SetNewPath(target.transform.position);
            }
            nbNodesMoved = 0;
        }

        public override void OnPlayerEnterDetected(GameObject player)
        {
            if (!isObjectAlreadyThere(player))
            {
                objectsDetected.Add(player);
                FindTargetWithMostPriority();
                if (target == null)
                {
                    if (ennemy.StartState == EnnemyController.StartingStates.Patrol)
                    {
                        ennemy.State = new Patrol(ennemy.PatrolPoints, pathFinder, ennemy, objectsDetected);
                    }
                    else
                    {
                        target = ennemy.MainTarget;
                        ChooseAttackOrChase();
                    }
                }
                else
                {
                    ChooseAttackOrChase();
                }
            }
        }

        public override void OnPlayerExitDetected(GameObject player)
        {
            if (player.GetTopParent().tag == R.E.Tag.Player.ToString())
            {
                objectsDetected.Remove(player);
            }
            FindTargetWithMostPriority();
            if (target == null)
            {
                if (ennemy.StartState == EnnemyController.StartingStates.Patrol)
                {
                    ennemy.State = new Patrol(ennemy.PatrolPoints, pathFinder, ennemy, objectsDetected);
                }
                else
                {
                    target = ennemy.MainTarget;
                    ChooseAttackOrChase();
                }
            }
            else
            {
                ChooseAttackOrChase();
            }
        }

        public override void OnPlayerInRangeDetected(GameObject player)
        {
            if (!isObjectAlreadyThere(player))
            {
                objectsDetected.Add(player);
            }
            FindTargetWithMostPriority();
            if (target == null)
            {
                if (ennemy.StartState == EnnemyController.StartingStates.Patrol)
                {
                    ennemy.State = new Patrol(ennemy.PatrolPoints, pathFinder, ennemy, objectsDetected);
                }
                else
                {
                    target = ennemy.MainTarget;
                    ChooseAttackOrChase();
                }
            }
            else
            {
                if (IsInPriority(player))
                {
                    ennemy.State = new Attack(player, pathFinder, ennemy, objectsDetected);
                }
            }
        }

        public override void OnPlayerOutOfRangeDetected(GameObject player)
        {

        }

        // Update is called once per frame
        public override void Update()
        {
            ennemy.IsMoving(false);
            if (target == null || !target.activeInHierarchy)
            {
                objectsDetected.Remove(target);
                FindTargetWithMostPriority();
                if (target == null)
                {
                    if (ennemy.StartState == EnnemyController.StartingStates.Patrol)
                    {
                        ennemy.State = new Patrol(ennemy.PatrolPoints, pathFinder, ennemy, objectsDetected);
                    }
                    else
                    {
                        target = ennemy.MainTarget;
                        ChooseAttackOrChase();
                    }
                }
                else
                {
                    ChooseAttackOrChase();
                }
            }
            if (!pathFinder.IsThreadRunning())
            {
                if (objectsDetected.Count > 0)
                {
                    bool isThereVisibleTarget = false;
                    for (int i = 0; i < objectsDetected.Count; i++)
                    {
                        if (!IsWalInbetween(objectsDetected[i].transform.position))
                        {
                            isThereVisibleTarget = true;
                            break;
                        }
                    }
                    if (isThereVisibleTarget)
                    {
                        FindTargetWithMostPriority();
                        if (target == null)
                        {
                            if (ennemy.StartState == EnnemyController.StartingStates.Patrol)
                            {
                                ennemy.State = new Patrol(ennemy.PatrolPoints, pathFinder, ennemy, objectsDetected);
                            }
                            else
                            {
                                target = ennemy.MainTarget;
                                ChooseAttackOrChase();
                            }
                        }
                        else
                        {
                            ChooseAttackOrChase();
                        }
                    }

                }
                Node destinationNode = pathFinder.GetDestinationNode(ennemy.gameObject.transform.position);
                if ((object)destinationNode != null)
                {
                    ennemy.Move(destinationNode.Position);
                    ennemy.IsMoving(true);
                }
                else
                {
                    FindTargetWithMostPriority();
                    if (target == null)
                    {
                        if (ennemy.StartState == EnnemyController.StartingStates.Patrol)
                        {
                            ennemy.State = new Patrol(ennemy.PatrolPoints, pathFinder, ennemy, objectsDetected);
                        }
                        else
                        {
                            target = ennemy.MainTarget;
                            ChooseAttackOrChase();
                        }
                    }
                    else
                    {
                        ChooseAttackOrChase();
                    }
                }
            }
        }
    }
}
