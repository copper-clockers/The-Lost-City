﻿using System.Collections;
using System.Collections.Generic;
using Harmony;
using UnityEngine;

namespace ProjetSynthese
{
    public enum targetTypes
    {
        Player = 0,
        Turret = 1,
        Extractor = 2,
        Sub = 3,
        None = 4
    }

    [AddComponentMenu("Game/EnnemyState/EnnemyState")]
    public abstract class EnnemyState
    {
        protected EnnemyController ennemy;

        protected AiPath pathFinder;

        protected GameObject target;

        protected List<GameObject> objectsDetected;

        // Update is called once per frame
        public abstract void Update();

        /// <summary>
        /// appelé lorsque le joueur entre dans la vue
        /// </summary>
        /// <param name="player">le gameobject du joueur détecté</param>
        public abstract void OnPlayerEnterDetected(GameObject player);

        /// <summary>
        /// appelé lorsque le joueur sort de la vue
        /// </summary>
        /// <param name="player">le gameobject du joueur détecté</param>
        public abstract void OnPlayerExitDetected(GameObject player);

        /// <summary>
        /// appelé lorsque le joueur est assez proche pour être attaqué
        /// </summary>
        /// <param name="player">le gameobject du joueur détecté</param>
        public abstract void OnPlayerInRangeDetected(GameObject player);

        /// <summary>
        /// appelé lorsque le joueur n'est plus assez proche pour être attaqué
        /// </summary>
        /// <param name="player">le gameobject du joueur détecté</param>
        public abstract void OnPlayerOutOfRangeDetected(GameObject player);

        //seulement pour les ennemies avec un roaming area
        public void RemoveTarget(GameObject target)
        {
            bool removedSomething = false;
            for (int i = 0; i < objectsDetected.Count; i++)
            {
                if (objectsDetected[i].GetTopParent() == target.GetTopParent())
                {
                    Debug.Log("Removing " + objectsDetected[i].name);
                    objectsDetected.RemoveAt(i);
                    removedSomething = true;
                }
            }
            if (removedSomething)
            {
                FindTargetWithMostPriority();
            }
        }

        private void RemoveNullTargets()
        {
            for (int i = 0; i < objectsDetected.Count; i++)
            {
                if (objectsDetected[i] == null || !objectsDetected[i].activeInHierarchy)
                {
                    objectsDetected.RemoveAt(i);
                    i--;
                }
            }
        }

        protected bool isObjectAlreadyThere(GameObject target)
        {
            for (int i = 0; i < objectsDetected.Count; i++)
            {
                if (objectsDetected[i].GetTopParent().tag == R.E.Tag.Submarine.ToString() && target.GetTopParent().tag == R.E.Tag.Submarine.ToString())
                {
                    if (objectsDetected[i].name == "Submarine")
                    {
                        objectsDetected[i] = target;
                    }
                    return true;
                }
                if (objectsDetected[i] == target)
                {
                    return true;
                }
            }
            return false;
        }

        protected void FindTargetWithMostPriority()
        {
            RemoveNullTargets();
            GameObject currentObjectInPriority = null;
            targetTypes currentObjectType = targetTypes.None;
            for (int i = 0; i < objectsDetected.Count; i++)
            {
                //si il y a un mur entre les deux
                if (IsWalInbetween(objectsDetected[i].transform.position))
                {

                }
                //si c'est le sous-marin
                else if (objectsDetected[i].GetTopParent().tag == R.E.Tag.Submarine.ToString())
                {
                    if (currentObjectType > targetTypes.Sub)
                    {
                        currentObjectType = targetTypes.Sub;
                        currentObjectInPriority = objectsDetected[i];
                    }
                }
                else if (objectsDetected[i].GetTopParent().tag == R.E.Tag.Construct.ToString())
                {
                    //si c'est un extracteur
                    if (objectsDetected[i].GetTopParent().layer == LayerMask.NameToLayer(R.S.Layer.MetalExtractor))
                    {
                        if (currentObjectType > targetTypes.Extractor)
                        {
                            currentObjectType = targetTypes.Extractor;
                            currentObjectInPriority = objectsDetected[i];
                        }
                    }
                    //si c'est une tourelle
                    else
                    {
                        if (currentObjectType > targetTypes.Turret)
                        {
                            currentObjectType = targetTypes.Turret;
                            currentObjectInPriority = objectsDetected[i];
                        }
                    }
                }
                //si c'est un player
                else if (objectsDetected[i].GetTopParent().tag == R.E.Tag.Player.ToString())
                {
                    if (currentObjectType > targetTypes.Player)
                    {
                        currentObjectType = targetTypes.Player;
                        currentObjectInPriority = objectsDetected[i];
                    }
                }
            }
            target = currentObjectInPriority;
            if (target != null)
            {
                DeterminePathfindingMethod(target.transform.position);
            }
        }

        protected bool IsWalInbetween(Vector2 targetPosition)
        {
            foreach (RaycastHit2D hit in Physics2D.LinecastAll(ennemy.gameObject.transform.position, targetPosition))
            {
                if (hit.transform.gameObject.layer == LayerMask.NameToLayer(R.S.Layer.WallDetection))
                {
                    return true;
                }
            }
            return false;
        }

        protected void DeterminePathfindingMethod(Vector2 destination)
        {
            bool isWallInBetweenTarget = false;
            foreach (RaycastHit2D hit in Physics2D.LinecastAll(ennemy.gameObject.transform.position, destination, LayerMask.NameToLayer(R.S.Layer.WallDetection)))
            {
                if (hit.transform.gameObject.layer == LayerMask.NameToLayer(R.S.Layer.WallDetection))
                {
                    pathFinder.PathfindingMethod = pathFinder.StartPathfindingMethod;
                    isWallInBetweenTarget = true;
                    break;
                }
            }
            if (!isWallInBetweenTarget)
            {
                pathFinder.PathfindingMethod = PathfindingMethod.Basic;
            }
        }

        protected bool IsInPriority(GameObject target)
        {
            targetTypes currentObjectType = targetTypes.None;
            //si il y a un mur entre les deux
            if (IsWalInbetween(target.transform.position))
            {
                return false;
            }
            else if (target.GetTopParent().tag == R.E.Tag.Submarine.ToString())
            {
                currentObjectType = targetTypes.Sub;
            }
            else if (target.GetTopParent().tag == R.E.Tag.Construct.ToString())
            {
                //si c'est un extracteur
                if (target.GetTopParent().layer == LayerMask.NameToLayer(R.S.Layer.MetalExtractor))
                {
                    currentObjectType = targetTypes.Extractor;
                }
                //si c'est une tourelle
                else
                {
                    currentObjectType = targetTypes.Turret;
                }
            }
            //si c'est un player
            else if (target.GetTopParent().tag == R.E.Tag.Player.ToString())
            {
                if (currentObjectType > targetTypes.Player)
                {
                    currentObjectType = targetTypes.Player;
                    return true;
                }
            }
            for (int i = 0; i < objectsDetected.Count; i++)
            {
                //si il y a un mur entre les deux
                if (IsWalInbetween(objectsDetected[i].transform.position))
                {

                }
                //si c'est le sous-marin
                else if (objectsDetected[i].GetTopParent().tag == R.E.Tag.Submarine.ToString())
                {
                    if (currentObjectType > targetTypes.Sub)
                    {
                        return false;
                    }
                }
                else if (objectsDetected[i].GetTopParent().tag == R.E.Tag.Construct.ToString())
                {
                    //si c'est un extracteur
                    if (objectsDetected[i].GetTopParent().layer == LayerMask.NameToLayer(R.S.Layer.MetalExtractor))
                    {
                        if (currentObjectType > targetTypes.Extractor)
                        {
                            return false;
                        }
                    }
                    //si c'est une tourelle
                    else
                    {
                        if (currentObjectType > targetTypes.Turret)
                        {
                            return false;
                        }
                    }
                }
                //si c'est un player
                else if (objectsDetected[i].GetTopParent().tag == R.E.Tag.Player.ToString())
                {
                    if (currentObjectType > targetTypes.Player)
                    {
                        return false;
                    }
                }
            }
            return true;
        }

        protected void ChooseAttackOrChase()
        {
            if (!(this is Attack) && ennemy.GetComponentInChildrensParentsOrSiblings<PlayerInRangeSensor>().GetComponent<Collider2D>().IsTouching(
                            target.GetComponent<Collider2D>()))
            {
                ennemy.State = new Attack(target, pathFinder, ennemy, objectsDetected);
            }
            else
            {
                if (!(this is Chase))
                {
                    ennemy.State = new Chase(target, pathFinder, ennemy, objectsDetected, false);
                }
                else
                {
                    pathFinder.SetNewPath(target.transform.position);
                }
            }

        }

    }
}
