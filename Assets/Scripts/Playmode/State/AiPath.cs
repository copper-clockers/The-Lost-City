﻿using System.Collections.Generic;
using System;
using Harmony;
using UnityEngine;
using System.Collections;
using System.Threading;

namespace ProjetSynthese
{
    [AddComponentMenu("Game/State/AiPath")]
    public class AiPath : GameScript
    {
        [Tooltip("How the ennemi should find it's paths")]
        [SerializeField]
        private PathfindingMethod pathfindingMethod;

        public PathfindingMethod StartPathfindingMethod { get; private set; }

        //chemin à suivre
        public List<Node> currentPath { get; private set; }

        //Ensemble des nodes sur la carte
        public Grid nodes { get; private set; }

        //Node sur lequel l'utilisteur se trouve
        private Node currentNode;

        private int pathPosition;

        private Vector2 topParentPosition;

        private Thread thread;

        public PathfindingMethod PathfindingMethod
        {
            get
            {
                return pathfindingMethod;
            }

            set
            {
                pathfindingMethod = value;
            }
        }

        //seulement pour les tests
        public void SetGrid(Grid grid, Vector2 topParentPosition)
        {
            nodes = grid.CopyGrid();
            nodes.CopyNodes();
            pathPosition = 0;
            PathfindingMethod = nodes.PathfindingMethod;
            StartPathfindingMethod = PathfindingMethod;
            this.topParentPosition = topParentPosition;
            currentNode = GetNodeByPoint(topParentPosition);
        }

        public void InsertPath(List<Node> path)
        {
            currentPath = path;
            pathPosition = 0;
        }

        public bool IsThreadRunning()
        {
            if (thread == null)
            {
                return false;
            }

            return thread.ThreadState == ThreadState.Running;
        }

        private void InjectGrid([SceneScope] Grid grid, [TopParentScope] Transform topParentPosition)
        {
            nodes = grid.CopyGrid();
            this.topParentPosition = topParentPosition.position;
            currentNode = GetNodeByPoint(this.topParentPosition);
            nodes.CopyNodes();
        }

        private void Awake()
        {
            StartPathfindingMethod = PathfindingMethod;
            InjectDependencies("InjectGrid");
            pathPosition = 0;
        }

        private void OnDestroy()
        {
            if (IsThreadRunning())
            {
                nodes.Kill();
            }
        }

        private void OnApplicationQuit()
        {
            if (IsThreadRunning())
            {
                nodes.Kill();
            }
        }

        /// <summary>
        /// retourne le point à atteindre pour continuer le chemin
        /// stoque le node courrant
        /// si le point à atteindre est atteinds, il retournera le prochain
        /// si la fin du chemin est atteinte, il retournera null
        /// </summary>
        /// <param name="position">la position courante de l'utilisateur du chemin</param>
        /// <returns>retourne le node à atteindre. Si la fin du chemin est attinds, retourne null</returns>
        public Node GetDestinationNode(Vector2 position)
        {
            if (currentPath == null)//failsafe, les thread peuvent rarement être arrêtés par unity
            {
                //Debug.Log("path failed, must retry path of ennemy at position: " + gameObject.transform.position);
                return null;
            }
            //si nous avons atteinds la fin du chemin
            if (pathPosition >= currentPath.Count - 1)
            {
                return null;
            }
            //si l'utilisateur est rendu sur le node
            if ((int)position.x == (int)currentPath[pathPosition].Position.x && (int)position.y == (int)currentPath[pathPosition].Position.y)
            {
                currentNode = currentPath[pathPosition]; //Car on sais que l'utilisateur est exactement sur ce node
                pathPosition++;
            }

            return currentPath[pathPosition];
        }
        /// <summary>
        /// crée un nouveau chemin à suivre
        /// </summary>
        /// <param name="target">destination finale du chemin</param>
        public void SetNewPath(Vector2 target)
        {
            if (currentPath == null || GetNodeByPoint(target) != currentPath[currentPath.Count - 1])// si le chemin courant ne donnem pas déjà vers la même destination
            {
                if (IsThreadRunning())
                {
                    nodes.Kill();
                    while (IsThreadRunning())
                    {
                        //just waithing for thread to stop
                    }
                    nodes.Resurect();
                    thread = null;
                }
                this.thread = new Thread(() => this.getPath(target, currentNode, 0));
                this.thread.Start();
            }
        }

        private void getPath(Vector2 targetPosition, Node current, int nbRetries)
        {
            Node target = GetNodeByPoint(targetPosition);
            nodes.UnvisitAllNodes();
            currentPath = null;
            currentPath = nodes.getPath(target, current, PathfindingMethod);
            pathPosition = 0;
        }

        /// <summary>
        /// donne le premier node qui touche le collider donné
        /// </summary>
        /// <param name="coliderTarget">collider dans lequel nous devons trouver</param>
        /// <returns></returns>
        public Node GetNodeByPoint(Vector2 coliderTarget)
        {
            return nodes.GetNodeByPoint(coliderTarget);
        }
    }
}
