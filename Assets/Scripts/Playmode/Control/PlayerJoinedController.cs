﻿using System;
using Harmony;
using UnityEngine;
using System.Collections;

namespace ProjetSynthese
{
    public delegate void PlayersAtSubEventHandler();

    [AddComponentMenu("Game/Control/PlayerJoinedController")]
    public class PlayerJoinedController : GameScript
    {
        [SerializeField]
        [Tooltip("HintDisplayed to get ammo when player is near submarine.")]
        private GameObject hint;

        private int nbPlayers;

        private int playersAtSub;

        private PlayerDetectSensor playerDetect;

        public event PlayersAtSubEventHandler OnPlayersAtSub;
        public event PlayersAtSubEventHandler OnPlayersNotAtSubAnymore;

        private PlayerDeathEventChannel playerDeathEventChannel;

        private void InjectPlayerJoinedController([ApplicationScope] GameActivityParameters gameActivityParameters,
                                                  [EntityScope] PlayerDetectSensor playerDetect,
                                                  [EventChannelScope] PlayerDeathEventChannel playerDeathEventChannel)
        {
            this.playerDetect = playerDetect;
            nbPlayers = gameActivityParameters.NumberOfPlayers;
            this.playerDeathEventChannel = playerDeathEventChannel;
        }

        private void Awake()
        {
            InjectDependencies("InjectPlayerJoinedController");
        }

        private void OnEnable()
        {
            playerDetect.OnDetectPlayerEnter += OnPlayerEnter;
            playerDetect.OnDetectPlayerExit += OnPlayerExit;
            playerDeathEventChannel.OnEventPublished += OnDeath;
        }

        private void OnDisable()
        {
            playerDetect.OnDetectPlayerEnter -= OnPlayerEnter;
            playerDetect.OnDetectPlayerExit -= OnPlayerExit;
            playerDeathEventChannel.OnEventPublished -= OnDeath;
        }

        private void OnPlayerEnter(GameObject player)
        {
            if (player.GetTopParent().tag == R.E.Tag.Player.ToString())
            {
                playersAtSub++;
                if (playersAtSub == 1)
                {
                    hint.SetActive(true);
                }
                player.GetTopParent().GetComponentInChildren<PlayerController>().EnterSubmarineRange();
            }
            if (playersAtSub >= nbPlayers)
            {
                if (OnPlayersAtSub != null) OnPlayersAtSub();
            }
            
        }

        private void OnPlayerExit(GameObject player)
        {
            if (player.GetTopParent().tag == R.E.Tag.Player.ToString())
            {
                playersAtSub--;
                if (playersAtSub == 0)
                {
                    hint.SetActive(false);
                }
                player.GetTopParent().GetComponentInChildren<PlayerController>().LeaveSubmarineRange();
            }
            if (playersAtSub < nbPlayers)
            {
                if (OnPlayersNotAtSubAnymore != null) OnPlayersNotAtSubAnymore();
            }
            
        }

        private void OnDeath(PlayerDeathEvent playerDeathEvent)
        {
            nbPlayers--;
        }
    }
}
