﻿using Harmony;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace ProjetSynthese
{
    [AddComponentMenu("Game/Control/GameStatsMenuController")]
    public class GameStatsMenuController : GameScript, IMenuController
    {
        [SerializeField]
        private Selectable LeftArrow;

        [SerializeField]
        private Selectable RightArrow;

        [SerializeField]
        private GameObject LevelNameText;

        [SerializeField]
        private GameObject DateText;

        [SerializeField]
        private GameObject ResultText;

        [SerializeField]
        private GameObject PlayTimeText;

        [SerializeField]
        private GameObject[] PlayerSlots;

        [SerializeField]
        private GameObject BuitExtractorsText;

        [SerializeField]
        private GameObject DestroyedExtractorsText;

        [SerializeField]
        private GameObject MetalGatheredText;

        [SerializeField]
        private GameObject KilledEnnemiesText;

        [SerializeField]
        private GameObject BuitTurretsText;

        [SerializeField]
        private GameObject DestroyedTurretsText;

        [SerializeField]
        private Sprite[] CharactersImage;

        [SerializeField]
        private Sprite[] CharactersDeathImage;

        private GameScoreRepository gamesRepository;

        private TeamMemberRepository teamRepository;

        private List<GameScore> games;

        private int currentId;

        private float timeToGo;

        private const float timeDelay = 0.25f;

        private PlayerInputSensor playerInputSensor;

        private ActivityStack activityStack;

        private GameActivityParameters gameActivityParameters;

        private void InjectGamesStatsMenuController([ApplicationScope] ActivityStack activityStack,
                                                    [ApplicationScope] GameActivityParameters gameActivityParameters,
                                                    [ApplicationScope] PlayerInputSensor playerInputSensor,
                                                    [ApplicationScope] GameScoreRepository gamesRepository,
                                                    [ApplicationScope] TeamMemberRepository teamRepository)
        {
            this.activityStack = activityStack;
            this.gameActivityParameters = gameActivityParameters;
            this.playerInputSensor = playerInputSensor;
            this.gamesRepository = gamesRepository;
            this.teamRepository = teamRepository;
        }

        private void Awake()
        {
            InjectDependencies("InjectGamesStatsMenuController");
        }

        private void OnEnable()
        {
            playerInputSensor.Players[0].OnBuild += OnExit;
            playerInputSensor.Players[0].OnMoveX += OnMove;
        }

        private void OnDisable()
        {
            playerInputSensor.Players[0].OnBuild -= OnExit;
            playerInputSensor.Players[0].OnMoveX -= OnMove;
        }

        public void OnCreate(params object[] parameters)
        {
            games = (List<GameScore>)gamesRepository.GetAllGameScores();
        }

        public void OnResume()
        {
            if (games.Count > 0)
            {
                LoadGame(0);
            }
            RightArrow.Select();
        }

        public void OnPause()
        {
            //Nothing to do
        }

        public void OnStop()
        {
            //Nothing to do
        }

        [CalledOutsideOfCode]
        public void OpenGameBeforeCurrent()
        {
            currentId--;
            if (currentId < 0)
            {
                currentId = games.Count-1;
            }
            if (games.Count > 0)
            {
                LoadGame(currentId);
            }
        }

        [CalledOutsideOfCode]
        public void OpenGameAfterCurrent()
        {
            currentId++;
            if (currentId >= games.Count)
            {
                currentId = 0;
            }
            if (games.Count > 0)
            {
                LoadGame(currentId);
            }
        }

        private void OnExit()
        {
            activityStack.StopCurrentMenu();
        }

        private void OnMove(float xAxe)
        {
            if (Time.time > timeToGo)
            {
                if (xAxe > 0)
                {
                    OpenGameAfterCurrent();
                }
                else if (xAxe < 0)
                {
                    OpenGameBeforeCurrent();
                }
                timeToGo = Time.time + timeDelay;
            }
        }

        private void LoadGame(int idInList)
        {
            currentId = idInList;
            GameScore game = games[idInList];
            //top
            LevelNameText.GetComponent<Text>().text = game.LevelName;
            DateText.GetComponent<Text>().text = game.TimeGame;
            if (game.GameWon)
            {
                ResultText.GetComponent<Text>().text = "WON";
            }
            else
            {
                ResultText.GetComponent<Text>().text = "LOST";
            }
            PlayTimeText.GetComponent<Text>().text = game.GameDuration;
            //middle
            List<TeamMember> team = (List<TeamMember>)teamRepository.GetAllTeamMembers((int)game.Id);
            for (int i = 0; i < 4; i++)
            {
                if (i + 1 > team.Count)
                {
                    PlayerSlots[i].SetActive(false);
                }
                else
                {
                    PlayerSlots[i].SetActive(true);
                    List<GameObject> childs = (List<GameObject>)PlayerSlots[i].GetAllChildrens();
                    for (int j = 0; j < childs.Count; j++)
                    {
                        if (childs[j].GetComponent<Image>() != null)
                        {
                            if (!team[i].WasKilled)
                            {
                                childs[j].GetComponent<Image>().sprite = CharactersImage[team[i].IdAdventurerClass-1];
                            }
                            else
                            {
                                childs[j].GetComponent<Image>().sprite = CharactersDeathImage[team[i].IdAdventurerClass-1];
                            }
                        }
                        else
                        {
                            if (team[i].WasKilled)
                            {
                                childs[j].GetComponent<Text>().text = team[i].LivingDuration;
                                childs[j].SetActive(true);
                            }
                            else
                            {
                                childs[j].SetActive(false);
                            }
                        }
                    }
                }
            }
            //bottom
            BuitExtractorsText.GetComponent<Text>().text = "Extractors Constructed: " + game.NbConstructedExtractor;
            DestroyedExtractorsText.GetComponent<Text>().text = "Extractors Destroyed: " + game.NbDestructedExtractor;
            MetalGatheredText.GetComponent<Text>().text = "Metal Quantity Gathered: " + game.MetalQuantityGathered;
            KilledEnnemiesText.GetComponent<Text>().text = "Enemies Killed: " + game.NbEnemiesKilled;
            BuitTurretsText.GetComponent<Text>().text = "Turrets Constructed: " + game.NbConstructedTurret;
            DestroyedTurretsText.GetComponent<Text>().text = "Turrets destroyed: " + game.NbDestructedTurret;
        }
    }
}