﻿using System.Collections;
using System.Collections.Generic;
using Harmony;
using UnityEngine;

namespace ProjetSynthese
{
    [AddComponentMenu("Game/Control/Secret")]
    public class SecretController : GameScript
    {

        private AchievementRepository achivementsRepository;
        private PlayerDetectSensor playerDetection;

        private const int achivementId = 8;

        private void InjectSecretController([EntityScope] PlayerDetectSensor playerDetect,
                            [ApplicationScope] AchievementRepository achievementsRepository)
        {
            this.achivementsRepository = achievementsRepository;
            this.playerDetection = playerDetect;
        }

        private void Awake()
        {
            InjectDependencies("InjectSecretController");
        }

        private void OnEnable()
        {
            playerDetection.OnDetectPlayerEnter += ObtainAchivementWhenPlayerEnters;
        }

        private void OnDisable()
        {
            playerDetection.OnDetectPlayerEnter -= ObtainAchivementWhenPlayerEnters;
        }

        private void ObtainAchivementWhenPlayerEnters(GameObject player)
        {
            if (!achivementsRepository.GetAnAchievement(achivementId).IsUnlocked)
            {
                achivementsRepository.UnlockAchievement(achivementId);
            }
            gameObject.GetTopParent().SetActive(false);
            
        }
    }
}


