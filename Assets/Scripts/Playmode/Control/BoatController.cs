﻿using System.Collections;
using System.Collections.Generic;
using Harmony;
using UnityEngine;

namespace ProjetSynthese
{
    public class BoatController : GameScript
    {
        private const float Speed = 4.5f;

        [SerializeField]
        [Tooltip("The start point of the boat according to the ID of the tile.")]
        private int startTileID;
        public int StartTileID
        {
            get { return startTileID; }
            set { startTileID = value; }
        }

        private GameObject[] lavaTiles;
        public GameObject[] LavaTiles
        {
            get { return lavaTiles; }
            set { lavaTiles = value; }
        }

        private GameObject boat;

        private GameObject destination;
        public GameObject Destination
        {
            get { return destination; }
            set { destination = value; }
        }

        private BoatDetectStimulus boatDetectStimulus;

        private int boatPositionIncrementor;
        
        private void InjectBoatController([EntityScope] BoatDetectStimulus boatDetectStimulus,
                                          [TopParentScope] GameObject boat)
        {
            this.boatDetectStimulus = boatDetectStimulus;
            this.boat = boat;
        }
        
        private void Awake()
        {
            InjectDependencies("InjectBoatController");
            lavaTiles = GameObject.FindGameObjectsWithTag(R.S.Tag.LavaTile);

            for(int i = 0; i < lavaTiles.Length; i++)
            {
                lavaTiles[i] = GameObject.Find("LavaTile (" + i.ToString() + ")");
            }
            
            boatPositionIncrementor = startTileID;
        }
        
        private void Update()
        {
            IncrementerManager();
            MoveBoat();
        }

        private void IncrementerManager()
        {
            destination = lavaTiles[boatPositionIncrementor];
            gameObject.GetTopParent().transform.rotation = lavaTiles[boatPositionIncrementor].transform.rotation;
        }

        private void MoveBoat()
        {
            //Si le bateau n'a pas encore atteint sa position.
            if (boat.transform.position != destination.transform.position)
            {
                boat.transform.position = Vector3.MoveTowards(boat.transform.position, destination.transform.position, Speed * Time.deltaTime);
            }
            else if (boat.transform.position == destination.transform.position) //S'il l'a atteint.
            {
                boatPositionIncrementor++;

                if (boatPositionIncrementor == lavaTiles.Length)
                {
                    boatPositionIncrementor = 0;
                }
            }
        }

        private void EnterCollision(GameObject passenger)
        {
                passenger.GetTopParent().GetComponentInChildren<CharacterStatistics>().AddLavaImmunity();
                passenger.transform.parent = GetTopParent().transform;
        }

        private void ExitCollision(GameObject passenger)
        {
                passenger.transform.parent = null;
                passenger.GetTopParent().GetComponentInChildren<CharacterStatistics>().RemoveLavaImmunity();
        }

        private void OnEnable()
        {
            boatDetectStimulus.OnBoatDetectEnter += EnterCollision;
            boatDetectStimulus.OnBoatDetectExit += ExitCollision;
        }

        private void OnDisable()
        {
            boatDetectStimulus.OnBoatDetectEnter -= EnterCollision;
            boatDetectStimulus.OnBoatDetectExit -= ExitCollision;
        }
    }
}
