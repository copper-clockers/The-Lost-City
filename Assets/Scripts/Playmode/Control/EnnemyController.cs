﻿using System.Collections;
using System.Collections.Generic;
using Harmony;
using UnityEngine;

namespace ProjetSynthese
{
    [AddComponentMenu("Game/Control/EnnemyController")]
    public class EnnemyController : GameScript
    {
        public enum StartingStates
        {
            Chase,
            Patrol,
            None
        }

        [SerializeField]
        [Tooltip("Seulement pour un ennemi qui patrol.\n" +
            "Tableau de game objects avec collider 2D trigger qui vont donner les points à atteindre en boucle pour le patrol de l'ennemi s'il le peut")]
        protected GameObject[] patrolPoints;

        [SerializeField]
        [Tooltip("Seulement pour un ennemi qui ne patrol pas.\n" +
            "Tableau de game objects avec collider 2D trigger qui vont donner les points à atteindre en boucle pour le patrol de l'ennemi s'il le peut")]
        protected GameObject mainTarget;

        [SerializeField]
        [Tooltip("L'état que l'ennemi doit commencer avec.\nCet état sera aussi l'état que l'ennemi va retourner par défaut lors de la fin d'un autre état.")]
        protected StartingStates startState;

        [SerializeField]
        [Tooltip("Prefab de la barre de vie et d'armure de l'ennemi.")]
        protected GameObject healthArmorBarPrefab;

        protected EnnemyState state;

        public EnnemyState State
        {
            get
            {
                return state;
            }

            set
            {
                state = value;
            }
        }
        protected AiPath pathFinder;
        protected GameObject topParentGameObject;
        protected PlayerDetectSensor playerDetectSensor;
        protected PlayerInRangeSensor playerInRangeSensor;
        protected EnemyKilledEventChannel enemyKilledEventChannel;

        protected KillableObject health;
        protected EnnemyMover mover;
        protected CharacterStatistics stats;

        private GameObject healthArmorBar;
        private GameObject healthBar;
        private GameObject armorBar;
        private Vector3 maxScale;


        public GameObject MainTarget
        {
            get
            {
                return mainTarget;
            }

            private set
            {
                mainTarget = value;
            }
        }

        public GameObject[] PatrolPoints
        {
            get
            {
                return patrolPoints;
            }

            private set
            {
                patrolPoints = value;
            }
        }

        public StartingStates StartState
        {
            get
            {
                return startState;
            }

            private set
            {
                startState = value;
            }
        }

        protected void InjectEnnemiState([EventChannelScope] EnemyKilledEventChannel enemyKilledEventChannel,
                                         [TopParentScope] GameObject topParentGameObject,
                                         [EntityScope] PlayerDetectSensor playerDetectSensor,
                                         [EntityScope] PlayerInRangeSensor playerInRangeSensor,
                                         [EntityScope] KillableObject health,
                                         [EntityScope] AiPath pathFinder,
                                         [EntityScope] EnnemyMover mover,
                                         [EntityScope] CharacterStatistics stats)
        {
            this.topParentGameObject = topParentGameObject;
            this.playerDetectSensor = playerDetectSensor;
            this.playerInRangeSensor = playerInRangeSensor;
            this.health = health;
            this.pathFinder = pathFinder;
            this.mover = mover;
            this.stats = stats;
            this.enemyKilledEventChannel = enemyKilledEventChannel;
        }

        protected virtual void Awake()
        {
            Inject();
        }

        protected void Inject()
        {
            InjectDependencies("InjectEnnemiState");
            healthArmorBar = null;
        }

        protected virtual void Start()
        {
            if (StartState == StartingStates.Patrol)
            {
                State = new Patrol(PatrolPoints, pathFinder, this, new List<GameObject>());
            }
            else if (StartState == StartingStates.Chase)
            {
                State = new Chase(mainTarget, pathFinder, this, new List<GameObject>(), false);
            }
        }

        // Update is called once per frame
        protected virtual void Update()
        {
            State.Update();
        }

        protected virtual void OnEnable()
        {
            playerDetectSensor.OnDetectPlayerEnter += OnPlayerEnterDetected;
            playerDetectSensor.OnDetectPlayerExit += OnPlayerExitDetected;
            playerInRangeSensor.OnPlayerInRangeEnter += OnPlayerInRangeDetected;
            playerInRangeSensor.OnPlayerInRangeExit += OnPlayerOutOfRangeDetected;
            health.OnHealthChanged += OnHealthChanged;
            health.OnArmorChanged += OnArmorChanged;
            health.OnDeath += OnDeath;

            if (healthArmorBarPrefab != null)
            {
                healthArmorBar = Instantiate(healthArmorBarPrefab);
                ApplyPositionOfBar();
                GetArmorAndHealthBar();
                maxScale = armorBar.transform.localScale;
            }
        }

        protected virtual void OnDisable()
        {
            playerDetectSensor.OnDetectPlayerEnter -= OnPlayerEnterDetected;
            playerDetectSensor.OnDetectPlayerExit -= OnPlayerExitDetected;
            playerInRangeSensor.OnPlayerInRangeEnter -= OnPlayerInRangeDetected;
            playerInRangeSensor.OnPlayerInRangeExit -= OnPlayerOutOfRangeDetected;
            health.OnHealthChanged -= OnHealthChanged;
            health.OnArmorChanged -= OnArmorChanged;
            health.OnDeath -= OnDeath;

            if (healthArmorBar != null)
            {
                Destroy(healthArmorBar);
            }
        }

        public virtual void IsMoving(bool isMoving)
        {

        }

        protected virtual void OnDeath()
        {
            enemyKilledEventChannel.Publish(new EnemyKilledEvent());
        }

        protected virtual void OnHealthChanged(int currentHealthPoints, int maxHealthPoints)
        {
            Vector3 newScale = new Vector3(currentHealthPoints * maxScale.x / maxHealthPoints, maxScale.y);
            healthBar.transform.localScale = newScale;
        }

        protected virtual void OnArmorChanged(int currentArmor, int maxArmor)
        {
            Vector3 newScale = new Vector3(currentArmor * maxScale.x / maxArmor, maxScale.y);
            armorBar.transform.localScale = newScale;
        }

        protected virtual void OnPlayerEnterDetected(GameObject player)
        {
            State.OnPlayerEnterDetected(player);
        }

        protected virtual void OnPlayerExitDetected(GameObject player)
        {
            State.OnPlayerExitDetected(player);
        }

        protected virtual void OnPlayerInRangeDetected(GameObject player)
        {
            State.OnPlayerInRangeDetected(player);
        }

        protected virtual void OnPlayerOutOfRangeDetected(GameObject player)
        {
            State.OnPlayerOutOfRangeDetected(player);
        }

        public virtual void Move(Vector2 destination)
        {
            mover.Move(destination);
            ApplyPositionOfBar();
        }

        public virtual void Rotate(Vector2 destination)
        {
            mover.Turn(destination);
            ApplyPositionOfBar();
        }

        public virtual void Attack()
        {
            if (!stats.IsPacified())
            {
                ExecuteAttack();
            }
        }

        protected virtual void ExecuteAttack()
        {

        }

        protected virtual void ApplyPositionOfBar()
        {
            if (healthArmorBarPrefab != null)
            {
                healthArmorBar.transform.position = new Vector3(GetTopParent().transform.position.x, GetTopParent().transform.position.y + 0.5f, GetTopParent().transform.position.z);
            }
        }

        private void GetArmorAndHealthBar()
        {
            IList<GameObject> bars = healthArmorBar.GetAllChildrens();
            foreach (GameObject bar in bars)
            {
                if (bar.name.Equals("HealthBar"))
                {
                    healthBar = bar;
                }
                else if (bar.name.Equals("ArmorBar"))
                {
                    armorBar = bar;
                }
            }
        }
    }
}
