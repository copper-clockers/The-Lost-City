﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Harmony;

namespace ProjetSynthese
{
    [AddComponentMenu("Game/Control/LeviathanController")]
    public class LeviathanController : EnnemyController
    {
        [SerializeField]
        [Range(0, 5)]
        [Tooltip("The delay between each attack of the Leviathan")]
        private float attackCooldown;

        [SerializeField]
        [Tooltip("a game object with the collider of the whole roaming area")]
        private GameObject roamingArea;

        private List<GameObject> targetsInRoamingArea;

        private Animator animator;
        private MeleeWeaponController weapon;
        private Patrol patrolState;
        private bool attackOnCooldown;

        private void InjectLeviathanController([EntityScope] Animator animator,
                                                [EntityScope] MeleeWeaponController weapon)
        {
            this.animator = animator;
            this.weapon = weapon;
        }

        public void SetRoamingArea(GameObject newRoamingAreaGameObject)
        {
            roamingArea = newRoamingAreaGameObject;
            roamingArea.GetComponent<PlayerDetectSensor>().OnDetectPlayerEnter += OnPlayerInRoamingArea;
            roamingArea.GetComponent<PlayerDetectSensor>().OnDetectPlayerExit += OnPlayerOutOfRoamingArea;
        }

        protected override void Awake()
        {
            InjectDependencies("InjectLeviathanController");
            targetsInRoamingArea = new List<GameObject>();
            base.Awake();
        }

        protected override void OnEnable()
        {
            if (roamingArea != null)
            {
                roamingArea.GetComponent<PlayerDetectSensor>().OnDetectPlayerEnter += OnPlayerInRoamingArea;
                roamingArea.GetComponent<PlayerDetectSensor>().OnDetectPlayerExit += OnPlayerOutOfRoamingArea;
            }
            base.OnEnable();
        }

        protected override void OnDisable()
        {
            if (roamingArea != null)
            {
                roamingArea.GetComponent<PlayerDetectSensor>().OnDetectPlayerEnter -= OnPlayerInRoamingArea;
                roamingArea.GetComponent<PlayerDetectSensor>().OnDetectPlayerExit -= OnPlayerOutOfRoamingArea;
            }
            base.OnDisable();
        }

        private bool IsTargetInRomaingArea(GameObject target)
        {
            RemoveNullTargets();
            for (int i = 0; i < targetsInRoamingArea.Count; i++)
            {
                if (targetsInRoamingArea[i].GetTopParent() == target.GetTopParent())
                {
                    return true;
                }
            }
            return false;
        }

        private void RemoveNullTargets()
        {
            for (int i = 0; i < targetsInRoamingArea.Count; i++)
            {
                if (targetsInRoamingArea[i] == null || !targetsInRoamingArea[i].activeInHierarchy)
                {
                    targetsInRoamingArea.RemoveAt(i);
                    i--;
                }
            }
        }

        protected void OnPlayerInRoamingArea(GameObject player)
        {
            if (!IsTargetInRomaingArea(player))
            {
                targetsInRoamingArea.Add(player);
            }
        }

        protected void OnPlayerOutOfRoamingArea(GameObject player)
        {
            targetsInRoamingArea.Remove(player);
            state.RemoveTarget(player);
            OnPlayerExitDetected(player);
        }

        protected override void OnPlayerEnterDetected(GameObject player)
        {
            if (IsTargetInRomaingArea(player))
            {
                base.OnPlayerEnterDetected(player);
            }
        }

        protected override void OnPlayerInRangeDetected(GameObject player)
        {
            if (IsTargetInRomaingArea(player))
            {
                base.OnPlayerInRangeDetected(player);
            }
        }

        public void SetPatrolPoints(GameObject[] newPatrolPoints)
        {
            patrolPoints = newPatrolPoints;
            State = new Patrol(newPatrolPoints,pathFinder,this,new List<GameObject>());            
        }

        public override void Attack()
        {
            if (!attackOnCooldown)
            {
                StartCoroutine(AttackCooldownCoroutine());
                animator.SetTrigger("isAttacking");
                ExecuteAttack();
            }
            
        }

        public override void IsMoving(bool isMoving)
        {
            animator.SetBool("isWalking", isMoving);
        }

        protected override void ExecuteAttack()
        {
            weapon.Attack();
            animator.SetTrigger("isAttacking");
        }

        private IEnumerator AttackCooldownCoroutine()
        {
            attackOnCooldown = true;
            yield return new WaitForSeconds(attackCooldown);
            attackOnCooldown = false;
        }
    }

}


