﻿using UnityEngine;

namespace ProjetSynthese
{
    public class PlayerContainer : GameScript
    {
        private GameObject player;
        public GameObject Player
        {
            get { return player; }
            set
            {
                player = value;
               
                GetComponentInChildren<ShowHealth>().AssignedPlayer = value;
                GetComponentInChildren<ShowArmor>().AssignedPlayer = value; 
                GetComponentInChildren<ChangeButtonAssetForPlayer>().AssignedPlayer = value;
                GetComponentInChildren<ShowAmmoQuantity>().AssignedPlayer = value;
            }
        }
    }
}
