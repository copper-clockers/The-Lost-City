﻿using System.Collections;
using System.Collections.Generic;
using Harmony;
using UnityEngine;

namespace ProjetSynthese
{
    [AddComponentMenu("Game/control/GetAchievementOnAllChildDepositsInteracted")]
    public class GetAchievementOnAllChildDepositsInteracted : GameScript
    {
        [SerializeField]
        [Tooltip("All the deposits that need to be interacted with to get the fifth acheivement")]
        private GameObject[] deposits;

        private bool[] interactedChilds;

        private AchievementRepository achievementRepository;

        private void InjectGetAchievementOnAllChildDepositsInteracted([ApplicationScope] AchievementRepository achievementRepository)
        {
            this.achievementRepository = achievementRepository;
        }

        private void Awake()
        {
            InjectDependencies("InjectGetAchievementOnAllChildDepositsInteracted");
            interactedChilds = new bool[deposits.Length];
            for (int i = 0; i < interactedChilds.Length; i++)
            {
                interactedChilds[i] = false;
            }
        }

        private void OnEnable()
        {
            for (int i = 0; i < deposits.Length; i++)
            {
                deposits[i].GetComponentInChildren<DepositDetectStimulus>().OnDepositDetectEnter += OnDepositDetected;
            }
        }

        private void OnDisable()
        {
            for (int i = 0; i < deposits.Length; i++)
            {
                deposits[i].GetComponentInChildren<DepositDetectStimulus>().OnDepositDetectEnter -= OnDepositDetected;
            }
        }

        private void OnDepositDetected(GameObject deposit)
        {
            for (int i = 0; i < deposits.Length; i++)
            {
                if (deposits[i].GetComponentInChildren<DepositDetectStimulus>().gameObject.Equals(deposit))
                {
                    interactedChilds[i] = true;
                    break;
                }
            }
            int nbInteractedChilds = 0;
            for (int i = 0; i < interactedChilds.Length; i++)
            {
                if (interactedChilds[i])
                {
                    nbInteractedChilds++;
                }
            }
            if (nbInteractedChilds == interactedChilds.Length)
            {
                achievementRepository.UnlockAchievement(5);
                enabled = false;
            }
        }

    }
}
