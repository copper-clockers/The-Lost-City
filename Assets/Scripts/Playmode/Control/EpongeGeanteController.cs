﻿using System.Collections;
using System.Collections.Generic;
using Harmony;
using UnityEngine;

namespace ProjetSynthese
{
    [AddComponentMenu("Game/Control/EpongeGeanteController")]
    public class EpongeGeanteController : MonsterController
    {

        [SerializeField]
        [Tooltip("Songe part that will be spawned around the ennemi")]
        private GameObject spongePart;

        [SerializeField]
        [Tooltip("Lenght and height of the square songe parts that will be spawned")]
        private double spongeDimention;

        [SerializeField]
        [Tooltip("radious of the sponge parts that will be spawned around ennemi at first")]
        private double spongeRadiousBeginning;

        [SerializeField]
        [Tooltip("The sponge's weapon")]
        private GameObject weapon;

        [SerializeField]
        [Tooltip("The lenght to add to the radious each time it gets angrier")]
        private float rangeToAdd;

        private float currentSpongeRadious;

        private int lvlOfAnger = 0;

        private bool playerSpotted;

        private bool playerInRange;

        private GameObject target;

        private List<Vector2> SpongesPlaced;

        private RangedWeapon weaponControl;

        protected List<GameObject> objectsDetected;

        protected List<GameObject> objectsInRange;

        private Animator animator;

        protected void InjectEpongeGeanteState([EventChannelScope] EnemyKilledEventChannel enemyKilledEventChannel,
                                       [TopParentScope] GameObject topParentGameObject,
                                       [EntityScope] PlayerDetectSensor playerSensor,
                                       [EntityScope] KillableObject health,
                                       [EntityScope] EnnemyMover mover,
                                       [EntityScope] PlayerInRangeSensor playerInRangeSensor,
                                       [EntityScope] CharacterStatistics stats,
                                       [EntityScope] Animator animator)
        {
            this.topParentGameObject = topParentGameObject;
            this.playerDetectSensor = playerSensor;
            this.playerInRangeSensor = playerInRangeSensor;
            this.health = health;
            this.mover = mover;
            this.stats = stats;
            this.enemyKilledEventChannel = enemyKilledEventChannel;
            this.animator = animator;
        }

        protected override void Awake()
        {
            spongePart.transform.localScale = new Vector2((float)spongeDimention, (float)spongeDimention);
            currentSpongeRadious = (float)spongeRadiousBeginning;
            InjectDependencies("InjectEpongeGeanteState");
            GameObject createdWeapon = Instantiate(weapon, transform.position + Vector3.right, transform.rotation, transform);
            createdWeapon.transform.Rotate(0, 0, -90);
            weaponControl = createdWeapon.GetComponent<RangedWeapon>();
            objectsDetected = new List<GameObject>();
            objectsInRange = new List<GameObject>();
        }

        //implémenté pour que le start de la super classe ne soit pas appelé
        protected override void Start()
        {
            SpawnSponges();
            state = null;
        }

        protected override void Update()
        {
            if (target != null)
            {
                if (playerSpotted)
                {
                    Rotate(target.transform.position);
                }
                if (playerInRange && lvlOfAnger > 0)
                {
                    Rotate(target.transform.position);
                    Attack();
                }
            }
            else
            {
                if (objectsDetected.Count > 0 || objectsInRange.Count > 0)
                {
                    FindTargetWithMostPriority();
                }
            }
        }

        /// <summary>
        /// fait apparître des éponges autour de l'énnemi dans un rayon déterminé par currentSpongeRadious.
        /// Lors de la création des éponges, tous les joueurs seront poussés à l'extérieur de la zone d'éponges,
        /// tout ce qui n'est pas un joueur et qui a de la vie sera détruit
        /// </summary>
        private void SpawnSponges()
        {
            //on s'occupe de tous les objets qui sont où seront les éponges
            foreach (RaycastHit2D hit in Physics2D.CircleCastAll(gameObject.transform.position,
                                                                  (int)currentSpongeRadious,
                                                                  Vector2.zero))
            {
                GameObject collidedObject = hit.collider.gameObject;
                if (collidedObject != topParentGameObject)
                {
                    //on déplace les joueurs
                    if (collidedObject.layer == LayerMask.NameToLayer(R.S.Layer.Player))
                    {
                        collidedObject.transform.position = getTargetNewPosition(collidedObject.transform.position);
                    }
                    //on tue tout ce qui n'est pas un joueur ou et qui a de la vie
                    else if (collidedObject.GetComponentInChildren<KillableObject>() != null)
                    {
                        collidedObject.GetComponentInChildren<KillableObject>().GetComponentInChildren<KillableObject>().ReceiveDamage(int.MaxValue);
                    }
                }
            }
            SpongesPlaced = new List<Vector2>();
            SpawnSpongesAround(gameObject.transform.position);
        }

        private void SpawnSpongesAround(Vector2 currentPosition)
        {
            Instantiate(spongePart, currentPosition, new Quaternion(0, 0, 0, 0));
            SpongesPlaced.Add(currentPosition);
            float dimention = (float)spongeDimention;
            Vector2 nextSponge = new Vector2(currentPosition.x, currentPosition.y + dimention/2);
            if (!IsSpongeThere(nextSponge) && !IsWallThere(nextSponge) && GetDisatanceToCenter(nextSponge) <= currentSpongeRadious)
            {
                SpawnSpongesAround(nextSponge);
            }
            nextSponge = new Vector2(currentPosition.x, currentPosition.y - dimention/2);
            if (!IsSpongeThere(nextSponge) && !IsWallThere(nextSponge) && GetDisatanceToCenter(nextSponge) <= currentSpongeRadious)
            {
                SpawnSpongesAround(nextSponge);
            }
            nextSponge = new Vector2(currentPosition.x + dimention/2, currentPosition.y);
            if (!IsSpongeThere(nextSponge) && !IsWallThere(nextSponge) && GetDisatanceToCenter(nextSponge) <= currentSpongeRadious)
            {
                SpawnSpongesAround(nextSponge);
            }
            nextSponge = new Vector2(currentPosition.x - dimention/2, currentPosition.y);
            if (!IsSpongeThere(nextSponge) && !IsWallThere(nextSponge) && GetDisatanceToCenter(nextSponge) <= currentSpongeRadious)
            {
                SpawnSpongesAround(nextSponge);
            }

        }

        /// <summary>
        /// détermine si un mur toucherais une éponge placée à la position donnée
        /// </summary>
        /// <param name="position">position où serait placé l'éponge</param>
        /// <returns>vrai s'il y a un mur, faux s'il n'y a pas de mur</returns>
        private bool IsWallThere(Vector2 position)
        {
            foreach (RaycastHit2D hit in Physics2D.BoxCastAll(position,
                                                              Vector2.one * (float)spongeDimention,
                                                              0,
                                                              Vector2.zero))
            {
                if (hit.transform.gameObject.layer == LayerMask.NameToLayer(R.S.Layer.WallDetection))
                {
                    return true;
                }
            }
            return false;
        }
        /// <summary>
        /// détermine si'il y a déjà une éponge à la position donnée
        /// </summary>
        /// <param name="position">position à regarder</param>
        /// <returns>vrai s'il y a une éponge, faux s'il n'y en a pas</returns>
        private bool IsSpongeThere(Vector2 position)
        {
            for (int i = 0; i < SpongesPlaced.Count; i++)
            {
                if (SpongesPlaced[i] == position)
                {
                    return true;
                }
            }
            return false;
        }
        /// <summary>
        /// détermine la distance entre le centre de l'ennemi et un point donné avec pythagore
        /// </summary>
        /// <param name="position">position dont nous devons trouver la distance du centre</param>
        /// <returns>la distance entre le centre de l'ennemi et le point donné</returns>
        private float GetDisatanceToCenter(Vector2 position)
        {
            return Mathf.Sqrt(Mathf.Pow(position.x - gameObject.transform.position.x, 2) + Mathf.Pow(position.y - gameObject.transform.position.y, 2));
        }

        /// <summary>
        /// Trouve la nouvelle position d'un joueur qui serais pris dans les éponges
        /// </summary>
        /// <param name="targetPostion">position du joueur pris dans les éponges</param>
        /// <returns>la nouvelle position du joueur</returns>
        private Vector2 getTargetNewPosition(Vector2 targetPostion)
        {
            Vector2 myPosition = topParentGameObject.transform.position;
            float hypotenuse = Mathf.Sqrt(Mathf.Pow(targetPostion.x - myPosition.x, 2) + Mathf.Pow(targetPostion.y - myPosition.y, 2));
            float multiplicateur = currentSpongeRadious / hypotenuse;
            float newX;
            float newY;
            if (targetPostion.x > myPosition.x)
            {
                newX = myPosition.x + ((targetPostion.x - myPosition.x) * multiplicateur) + (float)spongeDimention;
            }
            else
            {
                newX = myPosition.x - ((myPosition.x - targetPostion.x) * multiplicateur) + (float)spongeDimention;
            }
            if (targetPostion.y > myPosition.y)
            {
                newY = myPosition.y + ((targetPostion.y - myPosition.y) * multiplicateur) + (float)spongeDimention;
            }
            else
            {
                newY = myPosition.y - ((myPosition.y - targetPostion.y) * multiplicateur) + (float)spongeDimention;
            }
            return new Vector2(newX, newY);
        }

        protected override void OnHealthChanged(int currentHealthPoints, int maxHealthPoints)
        {
            if (lvlOfAnger == 0)
            {
                lvlOfAnger = 1;
            }
            else if (lvlOfAnger == 1 && currentHealthPoints <= maxHealthPoints * 3 / 4)
            {
                currentSpongeRadious = (float)(spongeRadiousBeginning * 2);
                lvlOfAnger = 2;
                playerDetectSensor.gameObject.GetComponent<CircleCollider2D>().radius += rangeToAdd;
                playerInRangeSensor.gameObject.GetComponent<CircleCollider2D>().radius += rangeToAdd;
                stats.IncreaseDamageMultiplier(100);
                SpawnSponges();
            }
            else if (lvlOfAnger == 2 && currentHealthPoints <= maxHealthPoints * 2 / 4)
            {
                currentSpongeRadious = (float)(spongeRadiousBeginning * 3);
                lvlOfAnger = 3;
                playerDetectSensor.gameObject.GetComponent<CircleCollider2D>().radius += rangeToAdd;
                playerInRangeSensor.gameObject.GetComponent<CircleCollider2D>().radius += rangeToAdd;
                stats.IncreaseDamageMultiplier(100);
                SpawnSponges();
            }
            else if (lvlOfAnger == 3 && currentHealthPoints <= maxHealthPoints * 1 / 4)
            {
                currentSpongeRadious = (float)(spongeRadiousBeginning * 4);
                lvlOfAnger = 4;
                playerDetectSensor.gameObject.GetComponent<CircleCollider2D>().radius += rangeToAdd;
                playerInRangeSensor.gameObject.GetComponent<CircleCollider2D>().radius += rangeToAdd;
                stats.IncreaseDamageMultiplier(100);
                SpawnSponges();
            }
            base.OnHealthChanged(currentHealthPoints, maxHealthPoints);

        }

        protected override void OnPlayerEnterDetected(GameObject player)
        {
            if (!isObjectAlreadyThere(player, true))
            {
                objectsDetected.Add(player);
            }
            FindTargetWithMostPriority();
        }

        protected override void OnPlayerExitDetected(GameObject player)
        {
            objectsDetected.Remove(player);
            FindTargetWithMostPriority();
        }

        protected override void OnPlayerInRangeDetected(GameObject player)
        {
            if (!isObjectAlreadyThere(player, false))
            {
                objectsInRange.Add(player);
            }
            FindTargetWithMostPriority();
        }

        protected override void OnPlayerOutOfRangeDetected(GameObject player)
        {
            objectsInRange.Remove(player);
            FindTargetWithMostPriority();
        }

        //implemented so that it won't move like the super class does
        public override void Move(Vector2 destination)
        {

        }

        protected override void ExecuteAttack()
        {
            weaponControl.Fire();
            animator.SetTrigger("isAttacking");
        }

        private void RemoveNullTargets()
        {
            for (int i = 0; i < objectsDetected.Count; i++)
            {
                if (objectsDetected[i] == null || !objectsDetected[i].activeInHierarchy)
                {
                    objectsDetected.RemoveAt(i);
                    i--;
                }
            }
            for (int i = 0; i < objectsInRange.Count; i++)
            {
                if (objectsInRange[i] == null || !objectsInRange[i].activeInHierarchy)
                {
                    objectsInRange.RemoveAt(i);
                    i--;
                }
            }
        }

        protected bool isObjectAlreadyThere(GameObject target, bool checkDetected)
        {
            List<GameObject> listToCheck;
            if (checkDetected)
            {
                listToCheck = objectsDetected;
            }
            else
            {
                listToCheck = objectsInRange;
            }
            for (int i = 0; i < listToCheck.Count; i++)
            {
                if (listToCheck[i].GetTopParent() == target.GetTopParent())
                {
                    return true;
                }
            }
            return false;
        }

        protected void FindTargetWithMostPriority()
        {
            RemoveNullTargets();
            GameObject currentObjectInPriority = null;
            targetTypes currentObjectType = targetTypes.None;
            List<GameObject> listToCheck = new List<GameObject>();
            if (objectsInRange.Count == 0)
            {
                playerInRange = false;
                if (objectsDetected.Count == 0)
                {
                    playerSpotted = false;

                }
                else
                {
                    listToCheck = objectsDetected;
                    playerSpotted = true;
                }
            }
            else
            {
                listToCheck = objectsInRange;
                playerInRange = true;
            }
            for (int i = 0; i < listToCheck.Count; i++)
            {
                //si c'est le sous-marin
                if (listToCheck[i].GetTopParent().tag == R.E.Tag.Submarine.ToString())
                {
                    if (currentObjectType > targetTypes.Sub)
                    {
                        currentObjectType = targetTypes.Sub;
                        currentObjectInPriority = listToCheck[i];
                    }
                }
                else if (listToCheck[i].GetTopParent().tag == R.E.Tag.Construct.ToString())
                {
                    //si c'est un extracteur
                    if (listToCheck[i].GetTopParent().layer == LayerMask.NameToLayer(R.S.Layer.MetalExtractor))
                    {
                        if (currentObjectType > targetTypes.Extractor)
                        {
                            currentObjectType = targetTypes.Extractor;
                            currentObjectInPriority = listToCheck[i];
                        }
                    }
                    //si c'est une tourelle
                    else
                    {
                        if (currentObjectType > targetTypes.Turret)
                        {
                            currentObjectType = targetTypes.Turret;
                            currentObjectInPriority = listToCheck[i];
                        }
                    }
                }
                //si c'est un player
                else if (listToCheck[i].GetTopParent().tag == R.E.Tag.Player.ToString())
                {
                    if (currentObjectType > targetTypes.Player)
                    {
                        currentObjectType = targetTypes.Player;
                        currentObjectInPriority = listToCheck[i];
                    }
                }
            }
            target = currentObjectInPriority;
        }

    }
}
