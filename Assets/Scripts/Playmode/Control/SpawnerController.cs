﻿using System;
using Harmony;
using UnityEngine;
using System.Collections;

namespace ProjetSynthese
{

    [AddComponentMenu("Game/Control/SpawnerController")]
    public class SpawnerController : GameScript
    {

        [SerializeField]
        [Tooltip("All the spawners in the level, in the order they'll be enabled. Spawners need to be disabled at start.")]
        private GameObject[] spawnersInLevel;

        [SerializeField]
        [Tooltip("Amount of spawners enabled at the same time")]
        private int numberOfSpawnersCalculatingAtTheSameTime;

        private bool[] spawnersDone;

        private bool[] spawnersRunning;

        // Use this for initialization
        void Start()
        {
            spawnersDone = new bool[spawnersInLevel.Length];
            spawnersRunning = new bool[spawnersInLevel.Length];
            for (int i = 0; i < spawnersInLevel.Length; i++)
            {
                spawnersDone[i] = false;
                spawnersRunning[i] = false;
            }
            for (int i = 0; i < numberOfSpawnersCalculatingAtTheSameTime; i++)
            {
                spawnersInLevel[i].SetActive(true);
                spawnersRunning[i] = true;

            }
        }

        private void OnEnable()
        {
            for (int i = 0; i < spawnersInLevel.Length; i++)
            {
                spawnersInLevel[i].GetComponent<SpawnEnnemiAtInterval>().OnSpawnerReady += SpawnerReady;
            }
        }

        private void OnDisable()
        {
            for (int i = 0; i < spawnersInLevel.Length; i++)
            {
                spawnersInLevel[i].GetComponent<SpawnEnnemiAtInterval>().OnSpawnerReady -= SpawnerReady;
            }
        }

        private bool CheckIfAllSpawnersEnabled()
        {
            int nbSpawners = 0;
            for (int i = 0; i < spawnersRunning.Length; i++)
            {
                if (spawnersInLevel[i].activeSelf)
                {
                    nbSpawners++;
                }
            }
            if (nbSpawners >= spawnersInLevel.Length)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private int FindPositionOfSpawner(GameObject spawner)
        {
            for (int i = 0; i < spawnersInLevel.Length; i++)
            {
                if (spawnersInLevel[i].transform.position == spawner.transform.position)
                {
                    return i;
                }
            }
            return -1;
        }

        private void SpawnerReady(GameObject spawner)
        {
            int spawnerPosition = FindPositionOfSpawner(spawner);
            spawnersRunning[spawnerPosition] = false;
            spawnersDone[spawnerPosition] = true;
            for (int i = 0; i < spawnersDone.Length; i++)
            {
                if (!spawnersRunning[i] && !spawnersDone[i])
                {
                    spawnersInLevel[i].SetActive(true);
                    spawnersRunning[i] = true;
                    break;
                }
            }
            if (CheckIfAllSpawnersEnabled())
            {
                Destroy(this.gameObject);

            }
        }
    }
}
