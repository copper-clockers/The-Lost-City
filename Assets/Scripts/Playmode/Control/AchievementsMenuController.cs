﻿using Harmony;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace ProjetSynthese
{
    [AddComponentMenu("Game/Control/AchievementsMenuController")]
    public class AchievementsMenuController : GameScript, IMenuController
    {
        [SerializeField]
        [Tooltip("The gameObject that will contain all the achievements")]
        private GameObject achievementsScroll;

        [SerializeField]
        [Tooltip("A prefab of an interface made to show an achievement")]
        private GameObject achievementPrefab;

        [SerializeField]
        [Tooltip("array of all the images of the achievements when unlocked")]
        private Sprite[] achievementImages;

        [SerializeField]
        [Tooltip("array of all the images of the achievements when locked")]
        private Sprite[] achievementLockedImages;

        private GameObject[] acheivementsObjects;

        private float maxScrollHeight;

        private float minScrollHeight;

        private float achievementHeight;

        private float timeToGo;

        private const float timeDelay = 0.25f;

        private PlayerInputSensor playerInputSensor;

        private ActivityStack activityStack;

        private GameActivityParameters gameActivityParameters;

        private AchievementRepository achievementRepository;

        private void InjectAchievementsMenuController([ApplicationScope] ActivityStack activityStack,
                                                      [ApplicationScope] GameActivityParameters gameActivityParameters,
                                                      [ApplicationScope] PlayerInputSensor playerInputSensor,
                                                      [ApplicationScope] AchievementRepository achievementRepository)
        {
            this.activityStack = activityStack;
            this.gameActivityParameters = gameActivityParameters;
            this.playerInputSensor = playerInputSensor;
            this.achievementRepository = achievementRepository;
        }

        private void Awake()
        {
            InjectDependencies("InjectAchievementsMenuController");
            timeToGo = Time.time;
        }

        private void Start()
        {
            achievementsScroll.GetComponent<RectTransform>().offsetMax = new Vector2(0, 0);
            achievementsScroll.GetComponent<RectTransform>().offsetMin = new Vector2(0, 0);
        }

        private void OnEnable()
        {
            playerInputSensor.Players[0].OnMoveY += OnMove;
            playerInputSensor.Players[0].OnBuild += OnExit;
        }

        private void OnDisable()
        {
            playerInputSensor.Players[0].OnMoveY -= OnMove;
            playerInputSensor.Players[0].OnBuild -= OnExit;
        }

        public void OnCreate(params object[] parameters)
        {
            achievementHeight = achievementPrefab.GetComponent<RectTransform>().anchorMax.y - achievementPrefab.GetComponent<RectTransform>().anchorMin.y;
            //on load tous les achievements
            List<Achievement> achievements = (List<Achievement>)achievementRepository.GetAllAchievements();
            acheivementsObjects = new GameObject[achievements.Count];
            for (int i = 0; i < achievements.Count; i++)
            {
                GameObject achievement = Instantiate(achievementPrefab, achievementsScroll.transform);
                acheivementsObjects[i] = achievement;

                //on set le bottom de l'achievement
                Vector2 newAnchorMin = new Vector2(achievement.GetComponent<RectTransform>().anchorMin.x,
                    achievement.GetComponent<RectTransform>().anchorMin.y - achievementHeight * i);
                achievement.GetComponent<RectTransform>().anchorMin = newAnchorMin;

                //on set le top de l'achievement
                Vector2 newAnchorMax = new Vector2(achievement.GetComponent<RectTransform>().anchorMax.x,
                    achievement.GetComponent<RectTransform>().anchorMax.y - achievementHeight * i);
                achievement.GetComponent<RectTransform>().anchorMax = newAnchorMax;

                //on entre toutes les données de l'achievement dans le gameObject
                List<GameObject> children = (List<GameObject>)achievement.GetAllChildrens();
                for (int j = 0; j < children.Count; j++)
                {
                    if (children[j].name == "LockedBlanket")
                    {
                        if (achievements[i].IsUnlocked)
                        {
                            children[j].SetActive(false);
                        }
                        else
                        {
                            children[j].SetActive(true);
                        }
                    }
                    else if (children[j].name == "Image") //c'est l'image
                    {
                        if (achievements[i].IsUnlocked)
                        {
                            children[j].GetComponent<Image>().sprite = achievementImages[achievements[i].Id - 1];
                        }
                        else
                        {
                            children[j].GetComponent<Image>().sprite = achievementLockedImages[achievements[i].Id - 1];
                        }
                    }
                    else if (children[j].name == "Nom") //c'est le titre
                    {
                        children[j].GetComponent<Text>().text = achievements[i].Name;
                    }
                    else //c'est la description
                    {
                        if (achievements[i].IsUnlocked)
                        {
                            children[j].GetComponent<Text>().text = achievements[i].Description;
                        }
                    }

                }
            }
            maxScrollHeight = (float)((float)achievementPrefab.GetComponent<RectTransform>().anchorMax.y + (float)(achievements.Count-4) * (float)achievementHeight);
            minScrollHeight = (float)achievementPrefab.GetComponent<RectTransform>().anchorMax.y;
        }

        public void OnResume()
        {
            //Nothing to do
        }

        public void OnPause()
        {
            //Nothing to do
        }

        public void OnStop()
        {
            //Nothing to do
        }

        private void OnMove(float yAxe)
        {
            if (Time.time > timeToGo)
            {
                if (yAxe > 0)
                {
                    OnUp();
                }
                else if (yAxe < 0)
                {
                    OnDown();
                }
                timeToGo = Time.time + timeDelay;
            }
        }

        private void OnUp()
        {
            ScrollAchievements(achievementHeight * -1);
        }

        private void OnDown()
        {
            ScrollAchievements(achievementHeight);
        }

        private void OnExit()
        {
            activityStack.StopCurrentMenu();
        }

        private void ScrollAchievements(float height)
        {
            if (height < 0 && acheivementsObjects[0].GetComponent<RectTransform>().anchorMax.y > minScrollHeight ||
                height > 0 && acheivementsObjects[0].GetComponent<RectTransform>().anchorMax.y < maxScrollHeight)
            {
                for (int i = 0; i < acheivementsObjects.Length; i++)
                {
                    GameObject achievement = acheivementsObjects[i];
                    //on set le bottom de l'achievement
                    Vector2 newAnchorMin = new Vector2(achievement.GetComponent<RectTransform>().anchorMin.x,
                        achievement.GetComponent<RectTransform>().anchorMin.y + height);
                    achievement.GetComponent<RectTransform>().anchorMin = newAnchorMin;

                    //on set le top de l'achievement
                    Vector2 newAnchorMax = new Vector2(achievement.GetComponent<RectTransform>().anchorMax.x,
                        achievement.GetComponent<RectTransform>().anchorMax.y + height);
                    achievement.GetComponent<RectTransform>().anchorMax = newAnchorMax;
                }
            }
        }
    }
}
