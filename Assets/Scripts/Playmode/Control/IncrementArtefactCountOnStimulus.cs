﻿using UnityEngine;
using Harmony;

namespace ProjetSynthese
{
    public class IncrementArtefactCountOnStimulus : GameScript
    {
        private ArtefactDetectStimulus artefactDetectStimulus;

        private ArtefactCount artefactCount;

        private void InjectIncrementArtefactCountOnStimulus([GameObjectScope] ArtefactDetectStimulus artefactDetectStimulus,
                                                            [SceneScope] ArtefactCount artefactCount)
        {
            this.artefactDetectStimulus = artefactDetectStimulus;
            this.artefactCount = artefactCount;
        }

        private void Awake()
        {
            InjectDependencies("InjectIncrementArtefactCountOnStimulus");
        }

        private void OnEnable()
        {
            artefactDetectStimulus.OnArtefactDetectEnter += ArtefactCountChanged;
        }
        
        private void ArtefactCountChanged(GameObject uselessParameter)
        {
            artefactCount.CollectArtefact();
            artefactDetectStimulus.OnArtefactDetectEnter -= ArtefactCountChanged;
        }
    }
}
