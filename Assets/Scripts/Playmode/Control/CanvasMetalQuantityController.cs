﻿using UnityEngine.UI;

namespace ProjetSynthese
{
    public class CanvasMetalQuantityController : GameScript
    {
        private Metal metal;
        public Metal Metal
        {
            get { return metal; }
            set {
                    metal = value;
                    metal.OnMetalQuantityChanged += MetalQuantityChanged;
                }
        }
        
        private void OnDestroy()
        {
            Metal.OnMetalQuantityChanged -= MetalQuantityChanged;
        }

        public void MetalQuantityChanged(int oldMetalQuantity, int newMetalQuantity)
        {
            GetComponentInChildren<Text>().text = newMetalQuantity.ToString() + "/" + Metal.MaximalMetalQuantity.ToString();
        }
    }
}