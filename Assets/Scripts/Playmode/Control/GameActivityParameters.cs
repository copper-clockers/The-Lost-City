﻿using UnityEngine;
using Harmony;

namespace ProjetSynthese
{
    public class GameActivityParameters : GameScript
    {
        private string[] playersNames = new string[4];
        public string[] PlayersNames
        {
            get { return playersNames; }
            set { playersNames = value; }
        }

        [SerializeField]
        [Tooltip("Number of players we want to begin the game.")]
        private int numberOfPlayers;
        public int NumberOfPlayers
        {
            get { return numberOfPlayers; }
            set { numberOfPlayers = value; }
        }
        
        private Activity selectedLevel;
        public Activity SelectedLevel
        {
            get { return selectedLevel; }
            set { selectedLevel = value; }
        }
    }
}