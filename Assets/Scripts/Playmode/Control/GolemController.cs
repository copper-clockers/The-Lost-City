﻿using Harmony;
using UnityEngine;

namespace ProjetSynthese
{
    public class GolemController : MonsterController
    {
        private MeleeWeaponController meleeWeaponController;
        private Animator animator;
        private int attackRate = 3;
        private float timeActionEnd;
        private GetAchievementForKillingEnemies getAchievementForKillingEnemies;

        private void InjectGolemController([EntityScope] MeleeWeaponController meleeWeaponController,
                                           [EntityScope] Animator animator,
                                           [SceneScope] GetAchievementForKillingEnemies getAchievementForKillingEnemies)
        {
            this.meleeWeaponController = meleeWeaponController;
            this.animator = animator;
            this.getAchievementForKillingEnemies = getAchievementForKillingEnemies;
        }

        private void Awake()
        {
            InjectDependencies("InjectGolemController");
            timeActionEnd = Time.time;
            base.Awake();
            stats.AddLavaAbsorbtion();
        }

        protected override void ExecuteAttack()
        {
            if (timeActionEnd < Time.time)
            {
                meleeWeaponController.Attack();
                animator.SetTrigger("isAttacking");
                timeActionEnd = Time.time + attackRate;
            }
        }

        public override void IsMoving(bool isMoving)
        {
            animator.SetBool("isWalking", isMoving);
        }

        public void OnDestroy()
        {
            getAchievementForKillingEnemies.EnemiesKilled++;
        }
    }
}
