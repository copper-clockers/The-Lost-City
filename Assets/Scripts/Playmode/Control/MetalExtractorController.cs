﻿using System.Collections;
using System.Collections.Generic;
using Harmony;
using UnityEngine;

namespace ProjetSynthese
{
    [AddComponentMenu("Game/Control/MetalExtractorController")]
    public class MetalExtractorController : GameScript
    {
        [SerializeField]
        [Tooltip("Rate at which the extractors mine metal.")]
        private float miningRate;

        [SerializeField]
        [Tooltip("Amount of metal extracted each time.")]
        private int amountOfMetalExtracted;

        [SerializeField]
        [Tooltip("Health and Armor bar.")]
        private GameObject healthArmorBarPrefab;

        public GameObject MineralDeposit { get; set; }
        public Metal Metal { get; set; }

        private DestroyedExtractorEventChannel destroyedExtractorEventChannel;
        private DepositExtractionEventChannel eventChannel;
        private KillableObject killableObject;
        private GameObject healthArmorBar;
        private GameObject healthBar;
        private GameObject armorBar;
        private Vector3 maxScale;
        private EntityDestroyer destroyer;

        private void InjectMetalExtractorController([EventChannelScope] DestroyedExtractorEventChannel destroyedExtractorEventChannel,
                                                    [EventChannelScope] DepositExtractionEventChannel eventChannel,
                                                    [EntityScope] KillableObject killableObject,
                                                    [EntityScope] EntityDestroyer destroyer)
        {
            this.destroyedExtractorEventChannel = destroyedExtractorEventChannel;
            this.eventChannel = eventChannel;
            this.killableObject = killableObject;
            this.destroyer = destroyer;
        }

        private void Awake()
        {
            InjectDependencies("InjectMetalExtractorController");
            healthArmorBar = null;
        }

        private void Start()
        {
            transform.parent.localScale = MineralDeposit.transform.localScale;
            StartCoroutine(Mine());

            if (healthArmorBarPrefab != null)
            {
                healthArmorBar = Instantiate(healthArmorBarPrefab);
                ApplyPositionOfBar();
                GetArmorAndHealthBar();
                maxScale = armorBar.transform.localScale;
            }
        }

        private void OnEnable()
        {
            killableObject.OnHealthChanged += OnHealthChanged;
            killableObject.OnArmorChanged += OnArmorChanged;
            killableObject.OnDeath += OnDeath;
        }

        private void OnDisable()
        {
            killableObject.OnHealthChanged -= OnHealthChanged;
            killableObject.OnArmorChanged -= OnArmorChanged;
            killableObject.OnDeath -= OnDeath;

            if (healthArmorBar != null)
            {
                Destroy(healthArmorBar);
            }
        }

        private IEnumerator Mine()
        {
            while (MineralDeposit != null && !MineralDeposit.gameObject.GetComponentInChildren<MetalDepositController>()
                       .IsDepleted)
            {
                Metal.ReduceMetalQuantity(amountOfMetalExtracted);
                eventChannel.Publish(new DepositExtractionEvent(amountOfMetalExtracted));
                yield return new WaitForSeconds(miningRate);
            }
            destroyer.Destroy();
            MineralDeposit.Destroy();
        }

        private void OnHealthChanged(int currentHealthPoints, int maxHealthPoints)
        {
            Vector3 newScale = new Vector3(currentHealthPoints * maxScale.x / maxHealthPoints, maxScale.y);
            healthBar.transform.localScale = newScale;
        }

        private void OnArmorChanged(int currentArmor, int maxArmor)
        {
            Vector3 newScale = new Vector3(currentArmor * maxScale.x / maxArmor, maxScale.y);
            armorBar.transform.localScale = newScale;
        }

        private void OnDeath()
        {
            MineralDeposit.gameObject.GetComponentInChildren<MetalDepositController>().IsOccupied = false;
            destroyedExtractorEventChannel.Publish(new DestroyedExtractorEvent());
        }

        public void ShowVisualHint(bool showHint)
        {
            GameObject hint = GetVisualHint();
            if (hint != null && killableObject.GetCurrentHealth()<killableObject.GetCurrentMaxHealth())
            {
                hint.SetActive(showHint);
            }
            else if (hint != null && killableObject.GetCurrentHealth() == killableObject.GetCurrentMaxHealth())
            {
                hint.SetActive(false);
            }
        }

        private GameObject GetVisualHint()
        {
            GameObject extractor = gameObject.GetTopParent();
            IList<GameObject> liste = extractor.GetAllChildrens();
            foreach (GameObject possibleHint in liste)
            {
                if (possibleHint.name.Equals("Hint"))
                {
                    return possibleHint;
                }
            }
            return null;
        }

        private void ApplyPositionOfBar()
        {
            if (healthArmorBarPrefab != null)
            {
                healthArmorBar.transform.position = new Vector3(GetTopParent().transform.position.x, 
                    GetTopParent().transform.position.y - 0.5f, GetTopParent().transform.position.z);
            }
        }

        private void GetArmorAndHealthBar()
        {
            IList<GameObject> bars = healthArmorBar.GetAllChildrens();
            foreach (GameObject bar in bars)
            {
                if (bar.name.Equals("HealthBar"))
                {
                    healthBar = bar;
                }
                else if (bar.name.Equals("ArmorBar"))
                {
                    armorBar = bar;
                }
            }
        }
    }
}

