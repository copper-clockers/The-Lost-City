﻿using System;
using Harmony;
using UnityEngine;
using System.Collections;

namespace ProjetSynthese
{
    [AddComponentMenu("Game/Control/GetAchievementOnJoinBeforeTime")]
    public class GetAchievementOnJoinBeforeTime : GameScript
    {
        [SerializeField]
        [Tooltip("time the 4 players have to get the acheivement fom the start of the level")]
        private float nbSecondsToGetAchievment;

        private int nbPlayers;

        private int playersAtSub;

        private PlayerDetectSensor playerDetect;

        private AchievementRepository achievementRepository;

        private void InjectGetAchievementOnJoinBeforeTime([ApplicationScope] GameActivityParameters gameActivityParameters,
                                                          [EntityScope] PlayerDetectSensor playerDetect,
                                                          [ApplicationScope] AchievementRepository achievementRepository)
        {
            this.playerDetect = playerDetect;
            this.achievementRepository = achievementRepository;
            nbPlayers = gameActivityParameters.NumberOfPlayers;
        }

        private void Awake()
        {
            InjectDependencies("InjectGetAchievementOnJoinBeforeTime");
            if (nbPlayers != 4)
            {
                enabled = false;
            }
        }

        private void Start()
        {
            StartCoroutine(DisableAfterSeconds(nbSecondsToGetAchievment));
        }

        private void OnEnable()
        {
            playerDetect.OnDetectPlayerEnter += OnPlayerEnter;
            playerDetect.OnDetectPlayerExit += OnPlayerExit;
        }

        private void OnDisable()
        {
            playerDetect.OnDetectPlayerEnter -= OnPlayerEnter;
            playerDetect.OnDetectPlayerExit -= OnPlayerExit;
        }

        private IEnumerator DisableAfterSeconds(float seconds)
        {
            yield return new WaitForSeconds(seconds);
            enabled = false;
        }

        private void OnPlayerEnter(GameObject player)
        {
            if (player.GetTopParent().tag != R.E.Tag.Player.ToString())
            {
                playersAtSub++;
            }
            if (nbPlayers == 4 && playersAtSub >= nbPlayers)
            {
                achievementRepository.UnlockAchievement(6);
                enabled = false;
            }
        }

        private void OnPlayerExit(GameObject player)
        {
            if (player.GetTopParent().tag != R.E.Tag.Player.ToString())
            {
                playersAtSub--;
            }
        }

    }
}
