﻿using System;
using System.Collections.Generic;
using Harmony;
using UnityEngine;
using UnityEngine.UI;

namespace ProjetSynthese
{
    public class PlayerSelectController : GameScript, IMenuController
    {
        [SerializeField]
        [Tooltip("Character Selection for Player One")]
        private GameObject playerOneSelector;

        [SerializeField]
        [Tooltip("Character Selection for Player Two")]
        private GameObject playerTwoSelector;

        [SerializeField]
        [Tooltip("Character Selection for Player Three")]
        private GameObject playerThreeSelector;

        [SerializeField]
        [Tooltip("Character Selection for Player Four")]
        private GameObject playerFourSelector;

        private ActivityStack activityStack;
        private GameActivityParameters gameActivityParameters;
        private PlayerInputSensor playerInputSensor;
        private AdventurerClassRepository adventurerClassRepository;
        
        private bool playerOneInfoOn;
        private bool playerTwoInfoOn;
        private bool playerThreeInfoOn;
        private bool playerFourInfoOn;

        private float timeForSelectionOne;
        private float timeForSelectionTwo;
        private float timeForSelectionThree;
        private float timeForSelectionFour;

        private const float timeDelay=0.25f;
        private float characterTileHeight;
        private int nbPlayerReady;

        private void InjectPlayerSelectController([ApplicationScope] ActivityStack activityStack,
                                                  [ApplicationScope] GameActivityParameters gameActivityParameters,
                                                  [ApplicationScope] PlayerInputSensor playerInputSensor,
                                                  [ApplicationScope] AdventurerClassRepository adventurerClassRepository)
        {
            this.activityStack = activityStack;
            this.gameActivityParameters = gameActivityParameters;
            this.playerInputSensor = playerInputSensor;
            this.adventurerClassRepository = adventurerClassRepository;
        }

        private void Awake()
        {
            InjectDependencies("InjectPlayerSelectController");

            timeForSelectionOne = Time.time;
            timeForSelectionTwo = Time.time;
            timeForSelectionThree = Time.time;
            timeForSelectionFour = Time.time;

            playerOneInfoOn = false;
            playerTwoInfoOn = false;
            playerThreeInfoOn = false;
            playerFourInfoOn = false;
        }

        private void OnEnable()
        {
            ResetCharacterSelection();

            SwitchSelection(playerOneSelector, null);
            SwitchSelection(playerTwoSelector, null);
            SwitchSelection(playerThreeSelector, null);
            SwitchSelection(playerFourSelector, null);

            nbPlayerReady = 0;
            DisableSelections();
            characterTileHeight = 1;
            playerInputSensor.Players[0].OnMoveY += OnPlayerOneMove;
            playerInputSensor.Players[0].OnSkill2 += OnPlayerOneInfo;
            playerInputSensor.Players[0].OnSkill1 += OnPlayerOneSelect;
            playerInputSensor.Players[0].OnBuild += OnPlayerOneReturn;
            if (gameActivityParameters.NumberOfPlayers >= 2)
            {
                playerInputSensor.Players[1].OnMoveY += OnPlayerTwoMove;
                playerInputSensor.Players[1].OnSkill2 += OnPlayerTwoInfo;
                playerInputSensor.Players[1].OnSkill1 += OnPlayerTwoSelect;
            }
            if (gameActivityParameters.NumberOfPlayers >= 3)
            {
                playerInputSensor.Players[2].OnMoveY += OnPlayerThreeMove;
                playerInputSensor.Players[2].OnSkill2 += OnPlayerThreeInfo;
                playerInputSensor.Players[2].OnSkill1 += OnPlayerThreeSelect;
            }
            if (gameActivityParameters.NumberOfPlayers == 4)
            {
                playerInputSensor.Players[3].OnMoveY += OnPlayerFourMove;
                playerInputSensor.Players[3].OnSkill2 += OnPlayerFourInfo;
                playerInputSensor.Players[3].OnSkill1 += OnPlayerFourSelect;
            }
        }

        private void OnDisable()
        {
            EnableSelections();
            playerInputSensor.Players[0].OnMoveY -= OnPlayerOneMove;
            playerInputSensor.Players[0].OnSkill2 -= OnPlayerOneInfo;
            playerInputSensor.Players[0].OnSkill1 -= OnPlayerOneSelect;
            playerInputSensor.Players[0].OnBuild -= OnPlayerOneReturn;
            if (gameActivityParameters.NumberOfPlayers >= 2)
            {
                playerInputSensor.Players[1].OnMoveY -= OnPlayerTwoMove;
                playerInputSensor.Players[1].OnSkill2 -= OnPlayerTwoInfo;
                playerInputSensor.Players[1].OnSkill1 -= OnPlayerTwoSelect;
            }
            if (gameActivityParameters.NumberOfPlayers >= 3)
            {
                playerInputSensor.Players[2].OnMoveY -= OnPlayerThreeMove;
                playerInputSensor.Players[2].OnSkill2 -= OnPlayerThreeInfo;
                playerInputSensor.Players[2].OnSkill1 -= OnPlayerThreeSelect;
            }
            if (gameActivityParameters.NumberOfPlayers == 4)
            {
                playerInputSensor.Players[3].OnMoveY -= OnPlayerFourMove;
                playerInputSensor.Players[3].OnSkill2 -= OnPlayerFourInfo;
                playerInputSensor.Players[3].OnSkill1 -= OnPlayerFourSelect;
            }
            
        }

        public void OnCreate(params object[] parameters)
        {

        }

        public void OnResume()
        {

        }

        public void OnPause()
        {
            //Nothing to do
        }

        public void OnStop()
        {
            //Nothing to do
        }

        #region Player One
        private void OnPlayerOneMove(float yAxe)
        {
            if (Time.time > timeForSelectionOne && gameActivityParameters.PlayersNames[0]==null)
            {
                if (yAxe>0)
                {
                    PlayerUp(playerOneSelector);
                }
                else if (yAxe<0)
                {
                    PlayerDown(playerOneSelector);
                }
                timeForSelectionOne = Time.time+timeDelay;
            }
        }

        public void OnPlayerOneInfo()
        {
            playerOneInfoOn = SwitchInfo(playerOneSelector,playerOneInfoOn);
        }

        public void OnPlayerOneSelect()
        {
            if (gameActivityParameters.PlayersNames[0] == null)
            {
                bool characterIsFree = true;
                string visibleCharacter= FindVisibleCharacter(playerOneSelector);
                for (int i = 0; i < gameActivityParameters.PlayersNames.Length; i++)
                {
                    if (gameActivityParameters.PlayersNames[i]!=null && gameActivityParameters.PlayersNames[i].Equals(visibleCharacter))
                    {
                        characterIsFree = false;
                    }
                }
                if (characterIsFree)
                {
                    gameActivityParameters.PlayersNames[0] = visibleCharacter;
                    nbPlayerReady++;
                }
            }
            else
            {
                gameActivityParameters.PlayersNames[0] = null;
                nbPlayerReady--;
            }
            SwitchSelection(playerOneSelector, gameActivityParameters.PlayersNames[0]);

            if (nbPlayerReady == gameActivityParameters.NumberOfPlayers)
            {
                activityStack.StartActivity(gameActivityParameters.SelectedLevel);
            }
        }

        public void OnPlayerOneReturn()
        {
            activityStack.StopCurrentMenu();
        }
        #endregion

        #region Player Two
        private void OnPlayerTwoMove(float yAxe)
        {
            if (Time.time > timeForSelectionTwo && gameActivityParameters.PlayersNames[1] == null)
            {
                if (yAxe > 0)
                {
                    PlayerUp(playerTwoSelector);
                }
                else if (yAxe < 0)
                {
                    PlayerDown(playerTwoSelector);
                }
                timeForSelectionTwo = Time.time + timeDelay;
            }
        }

        public void OnPlayerTwoInfo()
        {
            playerTwoInfoOn = SwitchInfo(playerTwoSelector, playerTwoInfoOn);
        }

        public void OnPlayerTwoSelect()
        {
            if (gameActivityParameters.PlayersNames[1] == null)
            {
                bool characterIsFree = true;
                string visibleCharacter = FindVisibleCharacter(playerTwoSelector);
                for (int i = 0; i < gameActivityParameters.PlayersNames.Length; i++)
                {
                    if (gameActivityParameters.PlayersNames[i] != null && gameActivityParameters.PlayersNames[i].Equals(visibleCharacter))
                    {
                        characterIsFree = false;
                    }
                }
                if (characterIsFree)
                {
                    gameActivityParameters.PlayersNames[1] = FindVisibleCharacter(playerTwoSelector);
                    nbPlayerReady++;
                }
            }
            else
            {
                gameActivityParameters.PlayersNames[1] = null;
                nbPlayerReady--;
            }
            SwitchSelection(playerTwoSelector, gameActivityParameters.PlayersNames[1]);

            if (nbPlayerReady == gameActivityParameters.NumberOfPlayers)
            {
                activityStack.StartActivity(gameActivityParameters.SelectedLevel);
            }
        }
        #endregion

        #region Player Three
        private void OnPlayerThreeMove(float yAxe)
        {
            if (Time.time > timeForSelectionThree && gameActivityParameters.PlayersNames[2] == null)
            {
                if (yAxe > 0)
                {
                    PlayerUp(playerThreeSelector);
                }
                else if (yAxe < 0)
                {
                    PlayerDown(playerThreeSelector);
                }
                timeForSelectionThree = Time.time + timeDelay;
            }
        }

        public void OnPlayerThreeInfo()
        {
            playerThreeInfoOn = SwitchInfo(playerThreeSelector, playerThreeInfoOn);
        }

        public void OnPlayerThreeSelect()
        {
            if (gameActivityParameters.PlayersNames[2] == null)
            {
                bool characterIsFree = true;
                string visibleCharacter = FindVisibleCharacter(playerThreeSelector);
                for (int i = 0; i < gameActivityParameters.PlayersNames.Length; i++)
                {
                    if (gameActivityParameters.PlayersNames[i] != null && gameActivityParameters.PlayersNames[i].Equals(visibleCharacter))
                    {
                        characterIsFree = false;
                    }
                }
                if (characterIsFree)
                {
                    gameActivityParameters.PlayersNames[2] = FindVisibleCharacter(playerThreeSelector);
                    nbPlayerReady++;
                }
            }
            else
            {
                gameActivityParameters.PlayersNames[2] = null;
                nbPlayerReady--;
            }
            SwitchSelection(playerThreeSelector, gameActivityParameters.PlayersNames[2]);

            if (nbPlayerReady == gameActivityParameters.NumberOfPlayers)
            {
                activityStack.StartActivity(gameActivityParameters.SelectedLevel);
            }
        }
        #endregion

        #region Player Four
        private void OnPlayerFourMove(float yAxe)
        {
            if (Time.time > timeForSelectionFour && gameActivityParameters.PlayersNames[3] == null)
            {
                if (yAxe > 0)
                {
                    PlayerUp(playerFourSelector);
                }
                else if (yAxe < 0)
                {
                    PlayerDown(playerFourSelector);
                }
                timeForSelectionFour = Time.time + timeDelay;
            }
        }

        public void OnPlayerFourInfo()
        {
            playerFourInfoOn = SwitchInfo(playerFourSelector, playerFourInfoOn);
        }

        public void OnPlayerFourSelect()
        {
            if (gameActivityParameters.PlayersNames[3] == null)
            {
                bool characterIsFree = true;
                string visibleCharacter = FindVisibleCharacter(playerFourSelector);
                for (int i = 0; i < gameActivityParameters.PlayersNames.Length; i++)
                {
                    if (gameActivityParameters.PlayersNames[i] != null && gameActivityParameters.PlayersNames[i].Equals(visibleCharacter))
                    {
                        characterIsFree = false;
                    }
                }
                if (characterIsFree)
                {
                    gameActivityParameters.PlayersNames[3] = FindVisibleCharacter(playerFourSelector);
                    nbPlayerReady++;
                }
            }
            else
            {
                gameActivityParameters.PlayersNames[3] = null;
                nbPlayerReady--;
            }
            SwitchSelection(playerFourSelector, gameActivityParameters.PlayersNames[3]);

            if (nbPlayerReady == gameActivityParameters.NumberOfPlayers)
            {
                activityStack.StartActivity(gameActivityParameters.SelectedLevel);
            }
        }
        #endregion

        private void DisableSelections()
        {
            if (gameActivityParameters.NumberOfPlayers < 2)
            {
                playerTwoSelector.SetActive(false);
                playerThreeSelector.SetActive(false);
                playerFourSelector.SetActive(false);
                FillInformationFromDb(playerOneSelector);
            }
            else if (gameActivityParameters.NumberOfPlayers < 3)
            {
                playerThreeSelector.SetActive(false);
                playerFourSelector.SetActive(false);
                FillInformationFromDb(playerOneSelector);
                FillInformationFromDb(playerTwoSelector);
            }
            else if (gameActivityParameters.NumberOfPlayers < 4)
            {
                playerFourSelector.SetActive(false);
                FillInformationFromDb(playerOneSelector);
                FillInformationFromDb(playerTwoSelector);
                FillInformationFromDb(playerThreeSelector);
            }
            else
            {
                FillInformationFromDb(playerOneSelector);
                FillInformationFromDb(playerTwoSelector);
                FillInformationFromDb(playerThreeSelector);
                FillInformationFromDb(playerFourSelector);
            }
        }

        private void EnableSelections()
        {
            playerOneSelector.SetActive(true);
            playerTwoSelector.SetActive(true);
            playerThreeSelector.SetActive(true);
            playerFourSelector.SetActive(true);
        }

        private void FillInformationFromDb(GameObject selector)
        {
            IList<GameObject> gameObjectSelect = selector.GetAllChildrens();
            AdventurerClass enforcer = adventurerClassRepository.GetAnAdventurerClass("Enforcer");
            AdventurerClass machinist = adventurerClassRepository.GetAnAdventurerClass("Machinist");
            AdventurerClass scientist = adventurerClassRepository.GetAnAdventurerClass("Scientist");
            AdventurerClass specter = adventurerClassRepository.GetAnAdventurerClass("Specter");
            foreach (GameObject itemToFill in gameObjectSelect)
            {
                #region Enforcer
                if (itemToFill.name.Equals("EnforcerNameClassText"))
                {
                    itemToFill.GetComponent<Text>().text = enforcer.NameClass;
                }
                if (itemToFill.name.Equals("EnforcerNameText"))
                {
                    itemToFill.GetComponent<Text>().text = enforcer.NameAdventurer;
                }
                if (itemToFill.name.Equals("EnforcerInfo"))
                {
                    itemToFill.GetComponent<Text>().text = GetInfoText(enforcer);
                }
                if (itemToFill.name.Equals("EnforcerInfoStory"))
                {
                    itemToFill.GetComponent<Text>().text = "Story: \n" + enforcer.StoryAdventurer;
                }
                #endregion

                #region Machinist
                if (itemToFill.name.Equals("MachinistNameClassText"))
                {
                    itemToFill.GetComponent<Text>().text = machinist.NameClass;
                }
                if (itemToFill.name.Equals("MachinistNameText"))
                {
                    itemToFill.GetComponent<Text>().text = machinist.NameAdventurer;
                }
                if (itemToFill.name.Equals("MachinistInfo"))
                {
                    itemToFill.GetComponent<Text>().text = GetInfoText(machinist);
                }
                if (itemToFill.name.Equals("MachinistInfoStory"))
                {
                    itemToFill.GetComponent<Text>().text = "Story: \n" + machinist.StoryAdventurer;
                }
                #endregion

                #region Scientist
                if (itemToFill.name.Equals("ScientistNameClassText"))
                {
                    itemToFill.GetComponent<Text>().text = scientist.NameClass;
                }
                if (itemToFill.name.Equals("ScientistNameText"))
                {
                    itemToFill.GetComponent<Text>().text = scientist.NameAdventurer;
                }
                if (itemToFill.name.Equals("ScientistInfo"))
                {
                    itemToFill.GetComponent<Text>().text = GetInfoText(scientist);
                }
                if (itemToFill.name.Equals("ScientistInfoStory"))
                {
                    itemToFill.GetComponent<Text>().text = "Story: \n" + scientist.StoryAdventurer;
                }
                #endregion

                #region Specter
                if (itemToFill.name.Equals("SpecterNameClassText"))
                {
                    itemToFill.GetComponent<Text>().text = specter.NameClass;
                }
                if (itemToFill.name.Equals("SpecterNameText"))
                {
                    itemToFill.GetComponent<Text>().text = specter.NameAdventurer;
                }
                if (itemToFill.name.Equals("SpecterInfo"))
                {
                    itemToFill.GetComponent<Text>().text = GetInfoText(specter);
                }
                if (itemToFill.name.Equals("SpecterInfoStory"))
                {
                    itemToFill.GetComponent<Text>().text = specter.StoryAdventurer;
                }
                #endregion
            }
        }

        private string GetInfoText(AdventurerClass adventurer)
        {
            string infoRangedWeapon = "Ranged Weapons: " + adventurer.RangedWeaponOne + ", " + adventurer.RangedWeaponTwo + "\n";
            string infoMeleeWeapon = "Melee Weapon: " + adventurer.MeleeWeapon + "\n";
            string infoSkills = "Skills: " + adventurer.SkillOne + ", " + adventurer.SkillTwo;
            return infoRangedWeapon + infoMeleeWeapon + infoSkills;
        }

        private void PlayerUp(GameObject selector)
        {
            if (selector.GetComponent<RectTransform>().anchorMax.y == 1f)
            {
                Vector2 newAnchorMin = new Vector2(selector.GetComponent<RectTransform>().anchorMin.x,
                    selector.GetComponent<RectTransform>().anchorMin.y + characterTileHeight * 3);
                selector.GetComponent<RectTransform>().anchorMin = newAnchorMin;

                Vector2 newAnchorMax = new Vector2(selector.GetComponent<RectTransform>().anchorMax.x,
                    selector.GetComponent<RectTransform>().anchorMax.y + characterTileHeight * 3);
                selector.GetComponent<RectTransform>().anchorMax = newAnchorMax;
            }
            else
            {
                Vector2 newAnchorMin = new Vector2(selector.GetComponent<RectTransform>().anchorMin.x,
                    selector.GetComponent<RectTransform>().anchorMin.y - characterTileHeight);
                selector.GetComponent<RectTransform>().anchorMin = newAnchorMin;

                Vector2 newAnchorMax = new Vector2(selector.GetComponent<RectTransform>().anchorMax.x,
                    selector.GetComponent<RectTransform>().anchorMax.y - characterTileHeight);
                selector.GetComponent<RectTransform>().anchorMax = newAnchorMax;
            }
        }

        private void PlayerDown(GameObject selector)
        {
            if (selector.GetComponent<RectTransform>().anchorMin.y == 0f)
            {

                Vector2 newAnchorMin = new Vector2(selector.GetComponent<RectTransform>().anchorMin.x,
                    selector.GetComponent<RectTransform>().anchorMin.y - characterTileHeight * 3);
                selector.GetComponent<RectTransform>().anchorMin = newAnchorMin;

                Vector2 newAnchorMax = new Vector2(selector.GetComponent<RectTransform>().anchorMax.x,
                    selector.GetComponent<RectTransform>().anchorMax.y - characterTileHeight * 3);
                selector.GetComponent<RectTransform>().anchorMax = newAnchorMax;
            }
            else
            {
                Vector2 newAnchorMin = new Vector2(selector.GetComponent<RectTransform>().anchorMin.x,
                    selector.GetComponent<RectTransform>().anchorMin.y + characterTileHeight);
                selector.GetComponent<RectTransform>().anchorMin = newAnchorMin;

                Vector2 newAnchorMax = new Vector2(selector.GetComponent<RectTransform>().anchorMax.x,
                    selector.GetComponent<RectTransform>().anchorMax.y + characterTileHeight);
                selector.GetComponent<RectTransform>().anchorMax = newAnchorMax;
            }
        }

        private void SwitchSelection(GameObject selector, string character)
        {
            IList<GameObject> listSelector = selector.GetAllChildrens();
            foreach (GameObject itemSelection in listSelector)
            {
                if (itemSelection.name.Equals("SelectedCharacter"))
                {
                    if (character == null)
                    {
                        itemSelection.SetActive(false);
                    }
                    else
                    {
                        itemSelection.SetActive(true);
                    }
                }
            }
        }

        private bool SwitchInfo(GameObject selector, bool playerInfoOn)
        {
            if (playerInfoOn)
            {
                IList<GameObject> listSelector = selector.GetAllChildrens();
                foreach (GameObject objectSelect in listSelector)
                {
                    if (objectSelect.name == "EnforcerShortInfo")
                    {
                        objectSelect.SetActive(true);
                    }
                    if (objectSelect.name == "EnforcerLongInfo")
                    {
                        objectSelect.SetActive(false);
                    }
                    if (objectSelect.name == "MachinistShortInfo")
                    {
                        objectSelect.SetActive(true);
                    }
                    if (objectSelect.name == "MachinistLongInfo")
                    {
                        objectSelect.SetActive(false);
                    }
                    if (objectSelect.name == "ScientistShortInfo")
                    {
                        objectSelect.SetActive(true);
                    }
                    if (objectSelect.name == "ScientistLongInfo")
                    {
                        objectSelect.SetActive(false);
                    }
                    if (objectSelect.name == "SpecterShortInfo")
                    {
                        objectSelect.SetActive(true);
                    }
                    if (objectSelect.name == "SpecterLongInfo")
                    {
                        objectSelect.SetActive(false);
                    }
                }
                return false;
            }
            else
            {
                IList<GameObject> listSelector = selector.GetAllChildrens();
                foreach (GameObject objectSelect in listSelector)
                {
                    if (objectSelect.name == "EnforcerShortInfo")
                    {
                        objectSelect.SetActive(false);
                    }
                    if (objectSelect.name == "EnforcerLongInfo")
                    {
                        objectSelect.SetActive(true);
                    }
                    if (objectSelect.name == "MachinistShortInfo")
                    {
                        objectSelect.SetActive(false);
                    }
                    if (objectSelect.name == "MachinistLongInfo")
                    {
                        objectSelect.SetActive(true);
                    }
                    if (objectSelect.name == "ScientistShortInfo")
                    {
                        objectSelect.SetActive(false);
                    }
                    if (objectSelect.name == "ScientistLongInfo")
                    {
                        objectSelect.SetActive(true);
                    }
                    if (objectSelect.name == "SpecterShortInfo")
                    {
                        objectSelect.SetActive(false);
                    }
                    if (objectSelect.name == "SpecterLongInfo")
                    {
                        objectSelect.SetActive(true);
                    }
                }
                return true;
            }
        }

        private string FindVisibleCharacter(GameObject selector)
        {
            if (selector.GetComponent<RectTransform>().anchorMin.y == -3f)
            {
                return "Enforcer";
            }
            if (selector.GetComponent<RectTransform>().anchorMin.y == -2f)
            {
                return "Machinist";
            }
            if (selector.GetComponent<RectTransform>().anchorMin.y == -1f)
            {
                return "Scientist";
            }
            if (selector.GetComponent<RectTransform>().anchorMin.y == 0f)
            {
                return "Specter";
            }
            return "";
        }

        private void ResetCharacterSelection()
        {
            for (int i = 0; i < gameActivityParameters.PlayersNames.Length; i++)
            {
                gameActivityParameters.PlayersNames[i] = null;
            }
        }
    }
}