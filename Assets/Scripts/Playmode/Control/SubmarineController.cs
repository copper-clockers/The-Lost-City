﻿using System.Collections;
using System.Collections.Generic;
using Harmony;
using UnityEngine;

namespace ProjetSynthese
{
    public delegate void SubmarineStateEventHandler();

    [AddComponentMenu("Game/Control/SubmarineController")]
    public class SubmarineController : GameScript
    {
        private Metal metal;
        private DepositExtractionEventChannel eventChannel;
        private PausingEventChannel pausingEventChannel;
        private HitSensor hitSensor;
        private CanvasMetalQuantityController canvasMetalQuantityController;
        private bool gameIsPaused;

        public event SubmarineStateEventHandler OnSubDestroyed;
        public event SubmarineStateEventHandler OnSubRepaired;
        public event SubmarineStateEventHandler OnSubNotRepairedAnymore;

        private bool isRepaired;

        public Metal Metal
        {
            get { return metal; }
        }

        private void InjectSubmarineController([GameObjectScope] Metal metal,
                                               [EventChannelScope] DepositExtractionEventChannel eventChannel,
                                               [EventChannelScope] PausingEventChannel pausingEventChannel,
                                               [EntityScope] HitSensor hitSensor,
                                               [SceneScope] CanvasMetalQuantityController canvasMetalQuantityController)
        {
            this.metal = metal;
            this.eventChannel = eventChannel;
            this.pausingEventChannel = pausingEventChannel;
            this.hitSensor = hitSensor;
            this.canvasMetalQuantityController = canvasMetalQuantityController;
        }

        private void Awake()
        {
            InjectDependencies("InjectSubmarineController");
            canvasMetalQuantityController.Metal = metal;
            gameIsPaused = false;
        }

        private void OnEnable()
        {
            metal.OnMetalQuantityDepleted += OnDestroyed;
            eventChannel.OnEventPublished += OnRepair;
            metal.OnMetalQuantityFilled += OnRepaired;
            metal.OnMetalQuantityChanged += OnMetalQuantityChanged;
            hitSensor.OnHit += OnHit;

            pausingEventChannel.OnEventPublished += OnPause;

            FindVisualState();
        }

        private void OnDisable()
        {
            metal.OnMetalQuantityDepleted -= OnDestroyed;
            eventChannel.OnEventPublished -= OnRepair;
            metal.OnMetalQuantityFilled -= OnRepaired;
            metal.OnMetalQuantityChanged -= OnMetalQuantityChanged;

            pausingEventChannel.OnEventPublished -= OnPause;

            hitSensor.OnHit -= OnHit;
        }

        private void OnRepair(DepositExtractionEvent extractionEvent)
        {
            metal.IncreaseMetalQuantity(extractionEvent.MetalAmountRecolted);
            FindVisualState();
        }

        private void OnPause(PausingEvent pausingEvent)
        {
            gameIsPaused = pausingEvent.isPaused;
        }

        private void OnRepaired()
        {
            isRepaired = true;
            Debug.Log("Submarine was repaired");
            if (OnSubRepaired != null) OnSubRepaired();
        }

        private void OnMetalQuantityChanged(int oldMetalQuantity, int newMetalQuantity)
        {
            if (isRepaired && oldMetalQuantity > newMetalQuantity)
            {
                isRepaired = false;
                if (OnSubNotRepairedAnymore != null) OnSubNotRepairedAnymore();
            }
        }

        private void OnDestroyed()
        {
            Debug.Log("Submarine was destroyed");
            metal.OnMetalQuantityDepleted -= OnDestroyed;
            if (OnSubDestroyed != null) OnSubDestroyed();
        }

        public void OnHit(int damage)
        {
            if (!gameIsPaused)
            {
                Debug.Log("Submarine took damage: " + damage);
                metal.ReduceMetalQuantity(damage);
                FindVisualState();
            }
        }

        private void FindVisualState()
        {
            if (!gameIsPaused)
            {
                GameObject visuals = GetSubmarineVisual();
                IList<GameObject> visualsToArrange = visuals.GetAllChildrens();

                foreach (GameObject visual in visualsToArrange)
                {
                    if (metal.MetalQuantity < metal.MaximalMetalQuantity / 6)
                    {
                        if (visual.name.Equals("submarine_0"))
                        {
                            visual.SetActive(true);
                        }
                        else
                        {
                            visual.SetActive(false);
                        }
                    }
                    else if (metal.MetalQuantity < (metal.MaximalMetalQuantity / 6) * 2)
                    {
                        if (visual.name.Equals("submarine_1"))
                        {
                            visual.SetActive(true);
                        }
                        else
                        {
                            visual.SetActive(false);
                        }
                    }
                    else if (metal.MetalQuantity < (metal.MaximalMetalQuantity / 6) * 3)
                    {
                        if (visual.name.Equals("submarine_2"))
                        {
                            visual.SetActive(true);
                        }
                        else
                        {
                            visual.SetActive(false);
                        }
                    }
                    else if (metal.MetalQuantity < (metal.MaximalMetalQuantity / 6) * 4)
                    {
                        if (visual.name.Equals("submarine_3"))
                        {
                            visual.SetActive(true);
                        }
                        else
                        {
                            visual.SetActive(false);
                        }
                    }
                    else if (metal.MetalQuantity < (metal.MaximalMetalQuantity / 6) * 5)
                    {
                        if (visual.name.Equals("submarine_4"))
                        {
                            visual.SetActive(true);
                        }
                        else
                        {
                            visual.SetActive(false);
                        }
                    }
                    else if (metal.MetalQuantity < metal.MaximalMetalQuantity)
                    {
                        if (visual.name.Equals("submarine_5"))
                        {
                            visual.SetActive(true);
                        }
                        else
                        {
                            visual.SetActive(false);
                        }
                    }
                }
            }
        }

        private GameObject GetSubmarineVisual()
        {
            GameObject visualToReturn = null;
            IList<GameObject> objectToSearch = GetTopParent().GetAllChildrens();
            foreach (GameObject possibleVisual in objectToSearch)
            {
                if (possibleVisual.name.Equals("SubmarineVisual"))
                {
                    visualToReturn = possibleVisual;
                    break;
                }
            }
            return visualToReturn;
        }
    }
}