﻿using System;
using Harmony;
using UnityEngine;
using UnityEngine.UI;

namespace ProjetSynthese
{
    [AddComponentMenu("Game/Control/GameOverMenuController")]
    public class GameOverMenuController : GameScript, IMenuController
    {
        private Selectable retryButton;
        private ActivityStack activityStack;
        private PlayerInputSensor playerInputSensor;
        private Selectable currentSelected;
        private PausingEventChannel pausingEventChannel;

        private void InjectGameOverController([Named(R.S.GameObject.RetryButton)] [EntityScope] Selectable retryButton,
                                              [ApplicationScope] ActivityStack activityStack,
                                              [ApplicationScope] PlayerInputSensor playerInputSensor,
                                              [EventChannelScope] PausingEventChannel pausingEventChannel)
        {
            this.retryButton = retryButton;
            this.activityStack = activityStack;
            this.playerInputSensor = playerInputSensor;
            this.pausingEventChannel = pausingEventChannel;
        }

        private void Awake()
        {
            InjectDependencies("InjectGameOverController");
            currentSelected = null;
        }

        public void OnCreate(params object[] parameters)
        {
            pausingEventChannel.Publish(new PausingEvent(true));
            TimeExtensions.Pause();
        }

        public void OnResume()
        {
            retryButton.Select();
            currentSelected = retryButton;
        }

        public void OnPause()
        {
            //Nothing to do
        }

        public void OnStop()
        {
            //Nothing to do
        }

        [CalledOutsideOfCode]
        public void RetryGame()
        {
            pausingEventChannel.Publish(new PausingEvent(false));
            TimeExtensions.Resume();
            activityStack.RestartCurrentActivity();
            activityStack.StopCurrentMenu();
        }

        [CalledOutsideOfCode]
        public void QuitGame()
        {
            pausingEventChannel.Publish(new PausingEvent(false));
            TimeExtensions.Resume();
            activityStack.StopCurrentActivity();
        }

    }
}