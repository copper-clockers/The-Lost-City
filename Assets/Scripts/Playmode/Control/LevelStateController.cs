﻿using System;
using System.Collections;
using System.Collections.Generic;
using Harmony;
using UnityEngine;

namespace ProjetSynthese
{
    [AddComponentMenu("Game/Control/LevelStateController")]
    public class LevelStateController : GameScript
    {
        public string DateGameStarted { get; private set; }
        public string TimeGameTook { get; private set; }
        public bool GameWasWon { get; private set; }
        public int AmountOfMetalGathered { get; private set; }
        public int AmountOfMetalSpent { get; private set; }
        public int AmountOfExtractorsBuilt { get; private set; }
        public int AmountOfExtractorsDestroyed { get; private set; }
        public int AmountOfTurretsBuilt { get; private set; }
        public int AmountOfTurretsDestroyed { get; private set; }
        public int AmountOfEnemiesKilled { get; private set; }
        public string LevelName { get; private set; }

        private DateTime timeGameStarted;
        private bool[] tableOfDeath;
        private string[] tableOfLivingDuration;

        private GameActivityParameters gameActivityParameters;
        private LevelRepository levelRepository;
        private AdventurerClassRepository adventurerClassRepository;
        private TeamMemberRepository teamMemberRepository;
        private GameScoreRepository gameScoreRepository;
        private AchievementRepository achievementRepository;
        private DepositExtractionEventChannel depositExtractionEventChannel;
        private MetalSpentEventChannel metalSpentEventChannel;
        private CreatedExtractorEventChannel createdExtractorEventChannel;
        private DestroyedExtractorEventChannel destroyedExtractorEventChannel;
        private CreatedTurretEventChannel createdTurretEventChannel;
        private DestroyedTurretEventChannel destroyedTurretEventChannel;
        private EnemyKilledEventChannel enemyKilledEventChannel;
        private PlayerDeathEventChannel playerDeathEventChannel;

        private void InjectLevelStateController([ApplicationScope] GameActivityParameters gameActivityParameters,
                                                [ApplicationScope] LevelRepository levelRepository,
                                                [ApplicationScope] AdventurerClassRepository adventurerClassRepository,
                                                [ApplicationScope] TeamMemberRepository teamMemberRepository,
                                                [ApplicationScope] GameScoreRepository gameScoreRepository,
                                                [ApplicationScope] AchievementRepository achievementRepository,
                                                [EventChannelScope] DepositExtractionEventChannel depositExtractionEventChannel,
                                                [EventChannelScope] MetalSpentEventChannel metalSpentEventChannel,
                                                [EventChannelScope] CreatedExtractorEventChannel createdExtractorEventChannel,
                                                [EventChannelScope] DestroyedExtractorEventChannel destroyedExtractorEventChannel,
                                                [EventChannelScope] CreatedTurretEventChannel createdTurretEventChannel,
                                                [EventChannelScope] DestroyedTurretEventChannel destroyedTurretEventChannel,
                                                [EventChannelScope] EnemyKilledEventChannel enemyKilledEventChannel,
                                                [EventChannelScope] PlayerDeathEventChannel playerDeathEventChannel)
        {
            this.gameActivityParameters = gameActivityParameters;
            this.levelRepository = levelRepository;
            this.adventurerClassRepository = adventurerClassRepository;
            this.teamMemberRepository = teamMemberRepository;
            this.gameScoreRepository = gameScoreRepository;
            this.achievementRepository = achievementRepository;
            this.depositExtractionEventChannel = depositExtractionEventChannel;
            this.metalSpentEventChannel = metalSpentEventChannel;
            this.createdExtractorEventChannel = createdExtractorEventChannel;
            this.destroyedExtractorEventChannel = destroyedExtractorEventChannel;
            this.createdTurretEventChannel = createdTurretEventChannel;
            this.destroyedTurretEventChannel = destroyedTurretEventChannel;
            this.enemyKilledEventChannel = enemyKilledEventChannel;
            this.playerDeathEventChannel = playerDeathEventChannel;
        }

        private void Awake()
        {
            InjectDependencies("InjectLevelStateController");
            DateGameStarted = "";
            TimeGameTook = "";
            timeGameStarted = DateTime.Now;
            GameWasWon = false;
            AmountOfMetalGathered = 0;
            AmountOfMetalSpent = 0;
            AmountOfExtractorsBuilt = 0;
            AmountOfExtractorsDestroyed = 0;
            AmountOfTurretsBuilt = 0;
            AmountOfTurretsDestroyed = 0;
            AmountOfEnemiesKilled = 0;

            tableOfLivingDuration = new string[4]{"","","",""};
            tableOfDeath = new bool[4];
            for (int i = 0; i < tableOfDeath.Length; i++)
            {
                tableOfDeath[i] = true;
            }
        }

        private void OnEnable()
        {
            DateGameStarted = DateTime.Now.ToShortDateString() + " - " + DateTime.Now.ToLongTimeString();
            timeGameStarted = DateTime.Now;
            depositExtractionEventChannel.OnEventPublished += OnMetalGathered;
            metalSpentEventChannel.OnEventPublished += OnMetalSpent;
            createdExtractorEventChannel.OnEventPublished += OnExtractorBuilt;
            destroyedExtractorEventChannel.OnEventPublished += OnExtractorDestroyed;
            createdTurretEventChannel.OnEventPublished += OnTurretBuilt;
            destroyedTurretEventChannel.OnEventPublished += OnTurretDestroyed;
            enemyKilledEventChannel.OnEventPublished += OnEnemyKilled;
            playerDeathEventChannel.OnEventPublished += OnPlayerKilled;
            LevelName = GetCorrectLevelName();

            for (int i = 0; i < gameActivityParameters.PlayersNames.Length; i++)
            {
                if (gameActivityParameters.PlayersNames[i] != null)
                {
                    tableOfDeath[i] = false;
                }
            }
        }

        private void OnDisable()
        {
            depositExtractionEventChannel.OnEventPublished -= OnMetalGathered;
            metalSpentEventChannel.OnEventPublished -= OnMetalSpent;
            createdExtractorEventChannel.OnEventPublished -= OnExtractorBuilt;
            destroyedExtractorEventChannel.OnEventPublished -= OnExtractorDestroyed;
            createdTurretEventChannel.OnEventPublished -= OnTurretBuilt;
            destroyedTurretEventChannel.OnEventPublished -= OnTurretDestroyed;
            enemyKilledEventChannel.OnEventPublished -= OnEnemyKilled;
            playerDeathEventChannel.OnEventPublished -= OnPlayerKilled;
            for (int i = 0; i < tableOfDeath.Length; i++)
            {
                tableOfDeath[i] = true;
            }
        }

        public void GameIsOver(bool gameWasWon)
        {
            GameWasWon = gameWasWon;
            TimeSpan timeLapse = DateTime.Now.Subtract(timeGameStarted);
            string time = "";
            if (timeLapse.Hours < 10)
            {
                time += "0";
            }
            time += timeLapse.Hours.ToString() + ":";
            if (timeLapse.Minutes < 10)
            {
                time += "0";
            }
            time += timeLapse.Minutes.ToString() + ":";
            if (timeLapse.Seconds < 10)
            {
                time += "0";
            }
            time += timeLapse.Seconds.ToString();

            TimeGameTook = time;

            if (gameActivityParameters.SelectedLevel.name.Equals("CaveOfTheLostActivity") && gameWasWon)
            {
                if (AmountOfTurretsBuilt == 0)
                {
                    UnlockAchievement(2);
                }
                if (timeLapse.Hours <= 0 && timeLapse.Minutes <= 5)
                {
                    UnlockAchievement(1);
                }
            }

            HandleEndGameDatabase();
        }

        private void OnMetalGathered(DepositExtractionEvent depositExtractionEvent)
        {
            AmountOfMetalGathered += depositExtractionEvent.MetalAmountRecolted;
        }

        private void OnMetalSpent(MetalSpentEvent metalSpentEvent)
        {
            AmountOfMetalSpent += metalSpentEvent.MetalAmountSpent;
        }

        private void OnExtractorBuilt(CreatedExtractorEvent createdExtractorEvent)
        {
            AmountOfExtractorsBuilt += createdExtractorEvent.ExtractorAmountBuilded;
        }

        private void OnExtractorDestroyed(DestroyedExtractorEvent destroyedExtractorEvent)
        {
            AmountOfExtractorsDestroyed += destroyedExtractorEvent.ExtractorAmountDestroyed;
        }

        private void OnTurretBuilt(CreatedTurretEvent createdTurretEvent)
        {
            AmountOfTurretsBuilt += createdTurretEvent.TurretAmountCreated;
        }

        private void OnTurretDestroyed(DestroyedTurretEvent destroyedTurretEvent)
        {
            AmountOfTurretsDestroyed += destroyedTurretEvent.TurretAmountDestroyed;
        }

        private void OnEnemyKilled(EnemyKilledEvent enemyKilledEvent)
        {
            AmountOfEnemiesKilled += enemyKilledEvent.EnemyAmountKilled;
        }

        private void OnPlayerKilled(PlayerDeathEvent playerDeathEvent)
        {
            tableOfDeath[playerDeathEvent.PlayerNumber] = true;

            TimeSpan timeLapse = DateTime.Now.Subtract(timeGameStarted);
            string time = "";
            if (timeLapse.Hours < 10)
            {
                time += "0";
            }
            time += timeLapse.Hours + ":";
            if (timeLapse.Minutes < 10)
            {
                time += "0";
            }
            time += timeLapse.Minutes + ":";
            if (timeLapse.Seconds < 10)
            {
                time += "0";
            }
            time += timeLapse.Seconds;
            tableOfLivingDuration[playerDeathEvent.PlayerNumber] = time;
        }

        private string GetCorrectLevelName()
        {
            if (gameActivityParameters.SelectedLevel.name.Equals("CaveOfTheLostActivity"))
            {
                return "Cave of the Lost";
            }
            if (gameActivityParameters.SelectedLevel.name.Equals("CaveOfTrialsActivity"))
            {
                return "Cave of Trials";
            }
            if (gameActivityParameters.SelectedLevel.name.Equals("CaveOfInfernoActivity"))
            {
                return "Cave of Inferno";
            }
            if (gameActivityParameters.SelectedLevel.name.Equals("LairOfLeviathan"))
            {
                return "Lair of the Leviathan";
            }
            return "";
        }

        private void HandleEndGameDatabase()
        {
            HandleGameScore();
            CheckAchievementIfAllLevelDone();
        }

        private void HandleGameScore()
        {
            GameScore gameScore = new GameScore();
            gameScore.TimeGame = DateGameStarted;
            gameScore.GameDuration = TimeGameTook;
            gameScore.GameWon = GameWasWon;
            gameScore.MetalQuantityGathered = AmountOfMetalGathered;
            gameScore.MetalQuantitySpent = AmountOfMetalSpent;
            gameScore.NbConstructedTurret = AmountOfTurretsBuilt;
            gameScore.NbDestructedTurret = AmountOfTurretsDestroyed;
            gameScore.NbConstructedExtractor = AmountOfExtractorsBuilt;
            gameScore.NbDestructedExtractor = AmountOfExtractorsDestroyed;
            gameScore.NbEnemiesKilled = AmountOfEnemiesKilled;
            gameScore.LevelName = LevelName;

            gameScoreRepository.AddGameScore(gameScore);

            HandleTeamMembers((int)gameScore.Id);
        }
        
        private void HandleTeamMembers(int gameScoreId)
        {
            for (int i = 0; i < gameActivityParameters.PlayersNames.Length; i++)
            {
                if (gameActivityParameters.PlayersNames[i] != null)
                {
                    TeamMember teamMember = new TeamMember();
                    teamMember.IdAdventurerClass = adventurerClassRepository
                        .GetAnAdventurerClass(gameActivityParameters.PlayersNames[i]).Id;
                    teamMember.IdGameScore = gameScoreId;
                    teamMember.WasKilled = tableOfDeath[i];
                    teamMember.LivingDuration = tableOfLivingDuration[i];

                    teamMemberRepository.AddTeamMember(teamMember);
                }
            }
        }

        private void CheckAchievementIfAllLevelDone()
        {
            IList<GameScore> gameScores = gameScoreRepository.GetAllGameScores();
            bool caveLostDone = false;
            bool caveTrialsDone = false;
            bool caveInfernoDone = false;
            bool caveLevyDone = false;
            foreach (GameScore gameScore in gameScores)
            {
                if (gameScore.GameWon)
                {
                    if (gameScore.LevelName.Equals("Cave of the Lost"))
                    {
                        caveLostDone = true;
                    }
                    else if (gameScore.LevelName.Equals("Cave of Trials"))
                    {
                        caveTrialsDone = true;
                    }
                    else if (gameScore.LevelName.Equals("Cave of Inferno"))
                    {
                        caveInfernoDone = true;
                    }
                    else if (gameScore.LevelName.Equals("Lair of the Leviathan"))
                    {
                        caveLevyDone = true;
                    }
                }
            }
            if (caveLostDone && caveTrialsDone && caveInfernoDone && caveLevyDone)
            {
                UnlockAchievement(10);
            }
        }

        private void UnlockAchievement(int indexAchievement)
        {
            achievementRepository.UnlockAchievement(indexAchievement);
        }
    }
}