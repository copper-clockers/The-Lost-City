﻿using System.Collections;
using System.Collections.Generic;
using Harmony;
using UnityEngine;

namespace ProjetSynthese
{
    [AddComponentMenu("Game/Control/ScoutController")]
    public class ScoutController : GardianController
    {
        [SerializeField]
        [Tooltip("The scout's weapon")]
        private GameObject weapon;

        [SerializeField]
        [Tooltip("Distance ennemies have to be to this scout to be alerted")]
        private float alertRadius;

        private RangedWeapon weaponControl;
        private Animator animator;

        private void InjectScoutController([EntityScope] Animator animator)
        {
            this.animator = animator;
        }

        protected override void Awake()
        {
            GameObject createdWeapon = Instantiate(weapon, transform.position + Vector3.right, transform.rotation, transform);
            createdWeapon.transform.Rotate(0, 0, -90);
            weaponControl = createdWeapon.GetComponent<RangedWeapon>();
            InjectDependencies("InjectScoutController");
            base.Awake();
        }

        public override void IsMoving(bool isMoving)
        {
            animator.SetBool("isWalking",isMoving);
        }

        protected override void OnHealthChanged(int currentHealthPoints, int maxHealthPoints)
        {
            //lors de la réception de coup, tous les ennemis autour du scout se dirigeront vers lui
            foreach (RaycastHit2D hit in Physics2D.CircleCastAll(gameObject.transform.position,
                                                                  alertRadius,
                                                                  Vector2.zero))
            {
                if (hit.collider.gameObject.GetComponentInChildren<AiPath>() != null && hit.collider.gameObject != topParentGameObject)
                {
                    hit.collider.gameObject.GetComponentInChildren<AiPath>().PathfindingMethod = 
                        hit.collider.gameObject.GetComponentInChildren<AiPath>().StartPathfindingMethod;
                    hit.collider.gameObject.GetComponentInChildren<AiPath>().SetNewPath(topParentGameObject.transform.position);
                }
            }
            base.OnHealthChanged(currentHealthPoints, maxHealthPoints);
        }

        protected override void ExecuteAttack()
        {
            weaponControl.Fire();
            animator.SetTrigger("isAttacking");
        }
    }
}