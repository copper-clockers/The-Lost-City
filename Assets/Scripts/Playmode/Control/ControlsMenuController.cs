﻿using Harmony;
using UnityEngine;

namespace ProjetSynthese
{
    [AddComponentMenu("Game/Control/ControlsMenuController")]
    public class ControlsMenuController : GameScript
    {

        private PlayerInputSensor playerInputSensor;

        private ActivityStack activityStack;

        private void InjectControlsMenuController([ApplicationScope] ActivityStack activityStack,
                                                  [ApplicationScope] PlayerInputSensor playerInputSensor)
        {
            this.activityStack = activityStack;
            this.playerInputSensor = playerInputSensor;
        }

        private void Awake()
        {
            InjectDependencies("InjectControlsMenuController");
        }

        public void OnEnable()
        {
            playerInputSensor.Players[0].OnBuild += OnReturnToMainMenu;
        }

        public void OnDisable()
        {
            playerInputSensor.Players[0].OnBuild -= OnReturnToMainMenu;
        }

        private void OnReturnToMainMenu()
        {
            activityStack.StopCurrentMenu();
        }
        


    }

}


