﻿using UnityEngine;
using Harmony;

namespace ProjetSynthese
{
    public class GameActivityParametersReception : GameScript, IActivityController
    {

        private GameActivityParameters gameActivityParameters;
        public GameActivityParameters GameActivityParameters
        {
            get { return gameActivityParameters;  } 
            set { gameActivityParameters = value; } 
        }
        
        [SerializeField]
        private GameObject scientist;

        [SerializeField]
        private GameObject specter;

        [SerializeField]
        private GameObject machinist;

        [SerializeField]
        private GameObject enforcer;

        [SerializeField]
        private Vector3[] playersStartPosition;


        private FogOfWar fogOfWar;


        private void InjectParametersFromMenu([ApplicationScope] GameActivityParameters gameActivityParameters,
                                              [SceneScope] FogOfWar fogOfWar)
        {
            this.gameActivityParameters = gameActivityParameters;
            this.fogOfWar = fogOfWar;
        }

        private void Awake()
        {
            InjectDependencies("InjectParametersFromMenu");
        }

        private void InstanciatePlayers()
        {
            string[] playersNames = gameActivityParameters.PlayersNames;

            for (int i = 0; i < playersNames.Length; i++)
            {
                int playerNumber = i + 1;
                if (playersNames[i] != null)
                {
                    GameObject newCharacter = null;
                    if (playersNames[i] == "Scientist")
                    {
                        newCharacter = Instantiate(scientist, playersStartPosition[i], Quaternion.identity) as GameObject;
                    }
                    else if (playersNames[i] == "Specter")
                    {
                        newCharacter = Instantiate(specter, playersStartPosition[i], Quaternion.identity) as GameObject;
                    }
                    else if (playersNames[i] == "Machinist")
                    {
                        newCharacter = Instantiate(machinist, playersStartPosition[i], Quaternion.identity) as GameObject;
                    }
                    else if (playersNames[i] == "Enforcer")
                    {
                        newCharacter = Instantiate(enforcer, playersStartPosition[i], Quaternion.identity) as GameObject;
                    }
                    GameObject uiManager = GameObject.Find("UIManagerP" + playerNumber.ToString());
                    uiManager.GetComponent<PlayerContainer>().Player = newCharacter;

                    newCharacter.GetComponentInChildren<GetFollowedByCamera>().PlayerNumber = playerNumber;
                    newCharacter.GetComponentInChildren<FogOfWarRevealer>().Radius = 50;
                    fogOfWar.RegisterRevealer(newCharacter.GetComponentInChildren<FogOfWarRevealer>());
                    newCharacter.GetComponentInChildren<PlayerController>().PlayerControllerIndex = i;
                }
                //else
                //{
                //    GameObject uiManager = GameObject.Find("UIManagerP" + playerNumber.ToString());
                //    Destroy(uiManager);
                //}
            }
        }

        public void OnCreate()
        {
            InstanciatePlayers();
        }

        public void OnStop()
        {

        }
    }
}