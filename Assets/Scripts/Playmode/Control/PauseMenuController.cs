﻿using Harmony;
using UnityEngine;
using UnityEngine.UI;

namespace ProjetSynthese
{
    [AddComponentMenu("Game/World/Ui/Control/PauseMenuController")]
    public class PauseMenuController : GameScript, IMenuController
    {
        [SerializeField]
        private Menu menuControls;

        private Selectable resumeButton;
        private ActivityStack activityStack;
        private PlayerInputSensor playerInputSensor;
        private Selectable currentSelected;

        private PausingEventChannel pausingEventChannel;

        private void InjectPauseController([Named(R.S.GameObject.ResumeButton)] [EntityScope] Selectable resumeButton,
                                           [EventChannelScope] PausingEventChannel pausingEventChannel,
                                           [ApplicationScope] ActivityStack activityStack,
                                           [ApplicationScope] PlayerInputSensor playerInputSensor)
        {
            this.resumeButton = resumeButton;
            this.activityStack = activityStack;
            this.playerInputSensor = playerInputSensor;
            this.pausingEventChannel = pausingEventChannel;
        }

        private void Awake()
        {
            InjectDependencies("InjectPauseController");
            currentSelected = null;
        }

        public void OnCreate(params object[] parameters)
        {
            pausingEventChannel.Publish(new PausingEvent(true));
            TimeExtensions.Pause();
        }

        public void OnResume()
        {
            resumeButton.Select();
            currentSelected = resumeButton;
        }

        public void OnPause()
        {
            //Nothing to do
        }

        public void OnStop()
        {
            TimeExtensions.Resume();
        }

        [CalledOutsideOfCode]
        public void ResumeGame()
        {
            activityStack.StopCurrentMenu();
            pausingEventChannel.Publish(new PausingEvent(false));
        }

        [CalledOutsideOfCode]
        public void StartControls()
        {
            activityStack.StartMenu(menuControls);
        }

        [CalledOutsideOfCode]
        public void RetryGame()
        {
            pausingEventChannel.Publish(new PausingEvent(false));
            TimeExtensions.Resume();
            activityStack.RestartCurrentActivity();
            activityStack.StopCurrentMenu();
        }

        [CalledOutsideOfCode]
        public void QuitGame()
        {
            activityStack.StopCurrentActivity();
            activityStack.StopCurrentMenu();
        }

    }
}