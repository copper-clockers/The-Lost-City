﻿using UnityEngine;
using Harmony;

namespace ProjetSynthese
{
    public delegate void ArtefactCountChangedEventHandler(int artefactCount);

    public class ArtefactCount : GameScript
    {
        private int artefactCount;

        private int artefactTotal;

        public event ArtefactCountChangedEventHandler OnArtefactCountChanged;

        public void CollectArtefact()
        {
            artefactCount++;
            if (OnArtefactCountChanged != null) OnArtefactCountChanged(artefactCount);
        }
    }
}


