﻿using System.Collections;
using Harmony;
using UnityEngine;
using System.Collections.Generic;

namespace ProjetSynthese
{
    [AddComponentMenu("Game/Control/PlayerController")]
    public class PlayerController : GameScript
    {
        [Tooltip("Le numéro du joueur selon sa manette.")]
        private int playerControllerIndex;
        public int PlayerControllerIndex
        {
            get { return playerControllerIndex; }
            set
            {
                playerControllerIndex = value;

                playerInputSensor.Players[playerControllerIndex].OnMoveX += OnMoveX;
                playerInputSensor.Players[playerControllerIndex].OnMoveY += OnMoveY;
                playerInputSensor.Players[playerControllerIndex].OnAimX += OnAimX;
                playerInputSensor.Players[playerControllerIndex].OnAimY += OnAimY;

                playerInputSensor.Players[playerControllerIndex].OnSkill1 += OnSkill1;
                playerInputSensor.Players[playerControllerIndex].OnBuild += OnBuild;
                playerInputSensor.Players[playerControllerIndex].OnSkill2 += OnSkill2;
                playerInputSensor.Players[playerControllerIndex].OnRepair += OnRepair;

                playerInputSensor.Players[playerControllerIndex].OnSwitchWeapon += OnSwitchWeapon;
                playerInputSensor.Players[playerControllerIndex].OnMeleeAttack += OnMeleeAttack;
                playerInputSensor.Players[playerControllerIndex].OnFire += OnFire;
                playerInputSensor.Players[playerControllerIndex].OnTogglePause += OnTogglePause;
            }
        }

        [SerializeField]
        [Tooltip("The player's main weapon")]
        private GameObject firstWeapon;

        [SerializeField]
        [Tooltip("The player's secondary weapon")]
        private GameObject secondWeapon;

        [SerializeField]
        [Tooltip("Repair effect")]
        private Effect repair;

        private RangedWeapon mainWeaponControl;
        private RangedWeapon subWeaponControl;
        private bool isUsingMainWeapon = true;

        private bool isNearSubmarine = false;

        private KillableObject health;
        private PlayerInputSensor playerInputSensor;
        private PlayerMovement playerMovement;
        private RangedWeapon rangedWeapon;
        private Metal metalSubmarineQuantity;
        private TurretSpawner turretSpawner;
        private MeleeWeaponController meleeWeaponController;
        private DepositDetectSensor depositDetectSensor;
        private TurretDetectSensor turretDetectSensor;
        private ExtractorDetectSensor extractorDetectSensor;
        private TurretController turretController;
        private MetalExtractorController metalExtractorController;
        private AbilityController abilityCaster;
        private CreatedTurretEventChannel createdTurretEventChannel;
        private CreatedExtractorEventChannel createdExtractorEventChannel;
        private MetalSpentEventChannel metalSpentEventChannel;
        private PlayerDeathEventChannel eventChannel;
        private CharacterStatistics stats;
        private Animator animator;
        private PutGameOnPause putGameOnPause;
        private PausingEventChannel pausingEventChannel;
        private bool gameIsPaused;

        private List<MetalDepositController> depositsNear;
        private List<MetalExtractorController> extractorsNear;
        private List<TurretController> turretsNear;

        public RangedWeapon CurrentWeapon { get { return isUsingMainWeapon ? mainWeaponControl : subWeaponControl; } }
        public RangedWeapon GetMainWeapon { get { return mainWeaponControl; } }
        public RangedWeapon GetSubWeapon { get { return subWeaponControl; } }

        private void InjectPlayerController([EventChannelScope] CreatedTurretEventChannel createdTurretEventChannel,
                                            [EventChannelScope] CreatedExtractorEventChannel createdExtractorEventChannel,
                                            [EventChannelScope] MetalSpentEventChannel metalSpentEventChannel,
                                            [ApplicationScope] PlayerInputSensor playerInputSensor,
                                            [TagScope(R.S.Tag.Submarine)] Metal submarineMetal,
                                            [GameObjectScope] KillableObject health,
                                            [GameObjectScope] PlayerMovement playerMovement,
                                            [EntityScope] MeleeWeaponController meleeWeaponController,
                                            [EntityScope] TurretSpawner turretSpawner,
                                            [EntityScope] AbilityController abilityCaster,
                                            [EntityScope] CharacterStatistics stats,
                                            [EntityScope] Animator animator,
                                            [EventChannelScope] PlayerDeathEventChannel eventChannel,
                                            [EventChannelScope] PausingEventChannel pausingEventChannel,
                                            [SceneScope] PutGameOnPause putGameOnPause)
        {
            metalSubmarineQuantity = submarineMetal;
            this.createdTurretEventChannel = createdTurretEventChannel;
            this.createdExtractorEventChannel = createdExtractorEventChannel;
            this.metalSpentEventChannel = metalSpentEventChannel;
            this.eventChannel = eventChannel;
            this.health = health;
            this.playerInputSensor = playerInputSensor;
            this.playerMovement = playerMovement;
            this.turretSpawner = turretSpawner;
            this.meleeWeaponController = meleeWeaponController;
            this.abilityCaster = abilityCaster;
            this.stats = stats;
            this.animator = animator;
            this.putGameOnPause = putGameOnPause;
            this.pausingEventChannel = pausingEventChannel;
        }

        private void Awake()
        {
            InjectDependencies("InjectPlayerController");

            metalExtractorController = null;
            turretController = null;

            depositDetectSensor = transform.parent.gameObject.GetComponentInChildren<DepositDetectSensor>();
            extractorDetectSensor = transform.parent.gameObject.GetComponentInChildren<ExtractorDetectSensor>();
            turretDetectSensor= transform.parent.gameObject.GetComponentInChildren<TurretDetectSensor>();

            GameObject createdWeapon;
            createdWeapon = Instantiate(secondWeapon, transform.position + Vector3.up, transform.rotation, transform);
            subWeaponControl = createdWeapon.GetComponent<RangedWeapon>();
            createdWeapon = Instantiate(firstWeapon, transform.position + Vector3.up, transform.rotation, transform);
            mainWeaponControl = createdWeapon.GetComponent<RangedWeapon>();
            
            gameIsPaused = false;

            depositsNear = new List<MetalDepositController>();
            extractorsNear = new List<MetalExtractorController>();
            turretsNear = new List<TurretController>();
        }
        
        private void OnEnable()
        {
            depositDetectSensor.OnDetectDepositEnter += DepositInRange;
            depositDetectSensor.OnDetectDepositExit += DepositNotInRange;
            extractorDetectSensor.OnDetectExtractorEnter += ExtractorInRange;
            extractorDetectSensor.OnDetectExtractorExit += ExtractorNotInRange;
            turretDetectSensor.OnDetectTurretEnter += TurretInRange;
            turretDetectSensor.OnDetectTurretExit += TurretNotInRange;

            mainWeaponControl.OnWeaponFire += OnWeaponFire;
            subWeaponControl.OnWeaponFire += OnWeaponFire;
            mainWeaponControl.OnReloading += OnReload;
            subWeaponControl.OnReloading += OnReload;

            health.OnDeath += PlayerDead;

            pausingEventChannel.OnEventPublished += OnPause;
            
        }

        private void OnDisable()
        {
            depositDetectSensor.OnDetectDepositEnter -= DepositInRange;
            depositDetectSensor.OnDetectDepositExit -= DepositNotInRange;
            extractorDetectSensor.OnDetectExtractorEnter -= ExtractorInRange;
            extractorDetectSensor.OnDetectExtractorExit -= ExtractorNotInRange;
            turretDetectSensor.OnDetectTurretEnter -= TurretInRange;
            turretDetectSensor.OnDetectTurretExit -= TurretNotInRange;

            mainWeaponControl.OnWeaponFire -= OnWeaponFire;
            subWeaponControl.OnWeaponFire -= OnWeaponFire;
            mainWeaponControl.OnReloading -= OnReload;
            subWeaponControl.OnReloading -= OnReload;

            health.OnDeath -= PlayerDead;

            pausingEventChannel.OnEventPublished -= OnPause;

            playerInputSensor.Players[playerControllerIndex].OnMoveX -= OnMoveX;
            playerInputSensor.Players[playerControllerIndex].OnMoveY -= OnMoveY;
            playerInputSensor.Players[playerControllerIndex].OnAimX -= OnAimX;
            playerInputSensor.Players[playerControllerIndex].OnAimY -= OnAimY;

            playerInputSensor.Players[playerControllerIndex].OnSkill1 -= OnSkill1;
            playerInputSensor.Players[playerControllerIndex].OnBuild -= OnBuild;
            playerInputSensor.Players[playerControllerIndex].OnSkill2 -= OnSkill2;
            playerInputSensor.Players[playerControllerIndex].OnRepair -= OnRepair;

            playerInputSensor.Players[playerControllerIndex].OnSwitchWeapon -= OnSwitchWeapon;
            playerInputSensor.Players[playerControllerIndex].OnMeleeAttack -= OnMeleeAttack;
            playerInputSensor.Players[playerControllerIndex].OnFire -= OnFire;
            playerInputSensor.Players[playerControllerIndex].OnTogglePause -= OnTogglePause;
        }

        public void Configure()
        {
            if(!gameIsPaused)
            {
                health.ResetLife();
                health.ResetArmor();
            }
        }

        public void EnterSubmarineRange()
        {
            isNearSubmarine = true;
        }

        public void LeaveSubmarineRange()
        {
            isNearSubmarine = false;
        }

        private void RefillWeaponAmmunitions()
        {
            if (!gameIsPaused)
            {
                if (GetMainWeapon.GetRefillAmmoCost() + GetSubWeapon.GetRefillAmmoCost() <
                metalSubmarineQuantity.MetalQuantity)
                {
                    metalSubmarineQuantity.ReduceMetalQuantity(GetMainWeapon.RefillAmmunition());
                    metalSubmarineQuantity.ReduceMetalQuantity(GetSubWeapon.RefillAmmunition());
                    CurrentWeapon.UpdateAmmunitionQuantity();
                }
            }
        }

        private void OnWeaponFire()
        {
            if (!gameIsPaused)
            {
                animator.SetTrigger("isAttacking");
            }
        }

        private void OnReload()
        {
            if (!gameIsPaused)
            {
                if (!(mainWeaponControl is Bow))
                {
                    animator.SetTrigger("isReloading");
                }
                else
                {
                    animator.SetTrigger("timeToReload");
                }
            }
            
        }

        private void PlayerDead()
        {
            if (!gameIsPaused)
            {
                eventChannel.Publish(new PlayerDeathEvent(playerControllerIndex));
            }
        }

        private void OnMoveX(float impulse)
        {
            if (!gameIsPaused)
            {
                playerMovement.MoveX(impulse);
            }
        }

        private void OnMoveY(float impulse)
        {
            if (!gameIsPaused)
            {
                playerMovement.MoveY(impulse);
            }
        }

        private void OnAimX(float impulse)
        {
            if (!gameIsPaused)
            {
                playerMovement.AimX(impulse);
            }
        }

        private void OnAimY(float impulse)
        {
            if (!gameIsPaused)
            {
                playerMovement.AimY(impulse);
            }
        }

        private void OnSkill1()
        {
            if (!gameIsPaused)
            {
                if (!stats.IsPacified())
                {
                    abilityCaster.CastFirstAbility();
                }

            }
        }
        
        private void OnBuild()
        {
            if (!gameIsPaused)
            {
                if (!stats.IsPacified())
                {
                    if (isNearSubmarine)
                    {
                        RefillWeaponAmmunitions();
                    }
                    else
                    {
                        int depositPosition = DetermineClosestDepositPosition();
                        if (depositPosition != -1)
                        {
                            MetalDepositController deposit = depositsNear[depositPosition];
                            if (deposit.GetExtractorPrice() < metalSubmarineQuantity.MetalQuantity)
                            {
                                int price = deposit.GetExtractorPrice();
                                metalSubmarineQuantity.ReduceMetalQuantity(price);
                                metalSpentEventChannel.Publish(new MetalSpentEvent(price));
                            }
                            else
                            {
                                int price = metalSubmarineQuantity.MetalQuantity - 1;
                                metalSubmarineQuantity.ReduceMetalQuantity((metalSubmarineQuantity.MetalQuantity - 1));
                                metalSpentEventChannel.Publish(new MetalSpentEvent(price));
                            }
                            deposit.CreateExtractor();
                            deposit.ShowVisualHint(false);
                            createdExtractorEventChannel.Publish(new CreatedExtractorEvent());
                        }
                        else if (turretSpawner.GetTurretPrice() < metalSubmarineQuantity.MetalQuantity)
                        {
                            turretSpawner.Spawn();
                            metalSubmarineQuantity.ReduceMetalQuantity(turretSpawner.GetTurretPrice());
                            metalSpentEventChannel.Publish(new MetalSpentEvent(turretSpawner.GetTurretPrice()));
                            createdTurretEventChannel.Publish(new CreatedTurretEvent());
                        }
                    }

                }
            }

        }

        private void OnSkill2()
        {
            if (!gameIsPaused)
            {
                if (!stats.IsPacified())
                {
                    abilityCaster.CastSecondAbility();
                }
            }

        }

        private void OnRepair()
        {
            if (!gameIsPaused)
            {
                if (!stats.IsPacified())
                {
                    GameObject machineToRepair = DetermineClosestMachinePosition();
                    if (machineToRepair != null)
                    {
                        repair.Apply(this.gameObject,machineToRepair);
                    }
                }
            }
        }

        private void OnSwitchWeapon()
        {
            if (!gameIsPaused)
            {
                if (!stats.IsPacified())
                {
                    isUsingMainWeapon = !isUsingMainWeapon;
                    animator.SetBool("isSubWeapon", !isUsingMainWeapon);
                }
                CurrentWeapon.UpdateAmmunitionQuantity();
            }

        }

        private void OnMeleeAttack()
        {
            if (!gameIsPaused)
            {
                if (!stats.IsPacified())
                {
                    meleeWeaponController.Attack();
                    animator.SetTrigger("isMeleeAttacking");
                }
            }

        }

        private void OnFire()
        {
            if (!gameIsPaused)
            {
                if (!stats.IsPacified())
                {
                    if (isUsingMainWeapon)
                    {
                        mainWeaponControl.Fire();
                    }
                    else
                    {
                        subWeaponControl.Fire();
                    }
                }
            }
        }

        private void OnPause(PausingEvent pausingEvent)
        {
            gameIsPaused = pausingEvent.isPaused;
        }

        private void OnTogglePause()
        {
            if (!gameIsPaused)
            {
                putGameOnPause.PauseGame();
                
            }
            

        }

        private void DepositInRange(GameObject deposit)
        {
            if (!gameIsPaused)
            {
                MetalDepositController currentController = deposit.transform.parent.gameObject.GetComponentInChildren<MetalDepositController>();
                depositsNear.Add(currentController);
                currentController.ShowVisualHint(true);
            }
        }

        private void DepositNotInRange(GameObject deposit)
        {
            if (!gameIsPaused)
            {
                MetalDepositController currentController = deposit.transform.parent.gameObject.GetComponentInChildren<MetalDepositController>();
                currentController.ShowVisualHint(false);
                depositsNear.Remove(currentController);
            }
        }

        private int DetermineClosestDepositPosition()
        {
            int closestDepositPosition = -1;
            double closestDepositDistance = double.MaxValue;
            for (int i = 0; i < depositsNear.Count; i++)
            {
                if (!depositsNear[i].IsOccupied)
                {
                    double depositDistance = Mathf.Sqrt(Mathf.Pow(depositsNear[i].transform.position.x - GetTopParent().transform.position.x, 2) +
                        Mathf.Pow(depositsNear[i].transform.position.y - GetTopParent().transform.position.y, 2));
                    if (depositDistance < closestDepositDistance)
                    {
                        closestDepositPosition = i;
                        closestDepositDistance = depositDistance;
                    }
                }
            }
            return closestDepositPosition;
        }

        private void ExtractorInRange(GameObject extractor)
        {
            if (!gameIsPaused)
            {
                metalExtractorController = extractor.transform.parent.gameObject
                .GetComponentInChildren<MetalExtractorController>();
                extractorsNear.Add(metalExtractorController);
                metalExtractorController.ShowVisualHint(true);
            }
        }

        private void ExtractorNotInRange(GameObject extractor)
        {
            if (!gameIsPaused)
            {
                metalExtractorController.ShowVisualHint(false);
                extractorsNear.Remove(metalExtractorController);
                metalExtractorController = null;
            }
        }

        private void TurretInRange(GameObject turret)
        {
            if (!gameIsPaused)
            {
                turretController = turret.transform.parent.gameObject
                .GetComponentInChildren<TurretController>();
                turretsNear.Add(turretController);
                turretController.ShowVisualHint(true);
            }
        }

        private void TurretNotInRange(GameObject turret)
        {
            if (!gameIsPaused)
            {
                turretController.ShowVisualHint(false);
                turretsNear.Remove(turretController);
                turretController = null;
            }
        }

        private GameObject DetermineClosestMachinePosition()
        {
            int closestTurretPosition = -1;
            int closestExtractorPosition = -1;
            double closestTurretDistance = double.MaxValue;
            double closestExtractorDistance = double.MaxValue;

            GameObject machineToReturn = null;
            for (int i = 0; i < turretsNear.Count; i++)
            {
                double turretDistance = Mathf.Sqrt(Mathf.Pow(turretsNear[i].transform.position.x - GetTopParent().transform.position.x, 2) +
                                                   Mathf.Pow(turretsNear[i].transform.position.y - GetTopParent().transform.position.y, 2));
                if (turretDistance < closestTurretDistance)
                {
                    closestTurretPosition = i;
                    closestTurretDistance = turretDistance;
                }
            }
            
            for (int i = 0; i < extractorsNear.Count; i++)
            {
                double extractorDistance = Mathf.Sqrt(Mathf.Pow(extractorsNear[i].transform.position.x - GetTopParent().transform.position.x, 2) +
                                                      Mathf.Pow(extractorsNear[i].transform.position.y - GetTopParent().transform.position.y, 2));
                if (extractorDistance < closestExtractorDistance)
                {
                    closestExtractorPosition = i;
                    closestExtractorDistance = extractorDistance;
                }
            }

            if (closestTurretDistance < closestExtractorDistance)
            {
                machineToReturn = turretsNear[closestTurretPosition].GetTopParent();
            }
            else
            {
                machineToReturn = extractorsNear[closestExtractorPosition].GetTopParent();
            }
            return machineToReturn;
        }
    }
}