﻿using Harmony;
using UnityEngine;

namespace ProjetSynthese
{
    [AddComponentMenu("Game/Control/GetAchievementForStayingOnLava")]
    public class GetAchievementForStayingOnLava : GameScript
    {
        private AchievementRepository achievementRepository;

        private void InjectGetAchievementForStayingOnLava([ApplicationScope] AchievementRepository achievementRepository)
        {
            this.achievementRepository = achievementRepository;
        }

        private void Awake()
        {
            InjectDependencies("InjectGetAchievementForStayingOnLava");
        }

        public void StayingTenSecondsOnLava()
        {
            achievementRepository.UnlockAchievement(4);
        }

    }
}
