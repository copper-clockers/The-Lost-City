﻿using UnityEngine;
using UnityEngine.UI;
using Harmony;

namespace ProjetSynthese
{
    public class CanvasArtefactController : GameScript
    {
        private GameObject[] artefacts;

        private int artefactTotal;

        private int artefactsCollected;

        private ArtefactCount artefactCount;

        private void InjectCanvasArtefactController([GameObjectScope] ArtefactCount artefactCount)
        {
            this.artefactCount = artefactCount;
        }

        private void Awake()
        {
            InjectDependencies("InjectCanvasArtefactController");
            artefacts = GameObject.FindGameObjectsWithTag(R.S.Tag.Artefact);
            artefactTotal = artefacts.Length;
        }

        private void Update()
        {
            GetComponentInChildren<Text>().text = artefactsCollected.ToString() + "/" + artefactTotal.ToString();

            if (artefactsCollected == artefactTotal)
            {
                GetComponentInChildren<Text>().color = Color.green;
                GetComponentInChildren<Text>().fontStyle = FontStyle.Bold;
            }
        }

        private void OnEnable()
        {
            artefactCount.OnArtefactCountChanged += ArtefactCountChanged;
        }

        private void OnDisable()
        {
            artefactCount.OnArtefactCountChanged -= ArtefactCountChanged;
        }

        public void ArtefactCountChanged(int artefactsCollected)
        {
            this.artefactsCollected = artefactsCollected;
        }
    }
}