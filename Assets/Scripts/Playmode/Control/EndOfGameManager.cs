﻿using Harmony;
using UnityEngine;

namespace ProjetSynthese
{
    public class EndOfGameManager : GameScript
    {
        [SerializeField]
        [Tooltip("the menu shown when the game is won")]
        private Menu winGameMenu;

        [SerializeField]
        [Tooltip("the menu shown when the game is lost")]
        private Menu lostGameMenu;

        [SerializeField]
        [Tooltip("the GameObject that determines if all players have joined")]
        private GameObject playerJoinArea;

        private PlayerDeathEventChannel playerDeathEventChannel;

        private ArtefactPickedUpEventChannel artefactPickedUpEventChannel;

        private GameActivityParameters gameActivityParameters;

        private LevelStateController levelStateController;

        private ActivityStack activityStack;

        private SubmarineController submarineController;

        private int remainingPlayers;

        private GameObject[] artefacts;

        private int artefactTotal;

        private int amountOfArtefactPickedUp;

        private bool playersAreAtSub;

        private bool isSubRepaired;

        public void InjectEndOfGameManager([ApplicationScope] GameActivityParameters gameActivityParameters,
                                           [EntityScope] LevelStateController levelStateController,
                                           [EventChannelScope] PlayerDeathEventChannel playerDeathEventChannel,
                                           [EventChannelScope] ArtefactPickedUpEventChannel artefactPickedUpEventChannel,
                                           [ApplicationScope] ActivityStack activityStack,
                                           [TagScope(R.S.Tag.Submarine)] SubmarineController submarineController)
        {
            this.playerDeathEventChannel = playerDeathEventChannel;
            this.artefactPickedUpEventChannel = artefactPickedUpEventChannel;
            this.gameActivityParameters = gameActivityParameters;
            this.levelStateController = levelStateController;
            this.activityStack = activityStack;
            this.submarineController = submarineController;
        }
        
        private void Awake()
        {
            InjectDependencies("InjectEndOfGameManager");
            amountOfArtefactPickedUp = 0;
            remainingPlayers = gameActivityParameters.NumberOfPlayers;

            artefacts = GameObject.FindGameObjectsWithTag(R.S.Tag.Artefact);
            artefactTotal = artefacts.Length;

            playersAreAtSub = false;
            isSubRepaired = false;
        }

        private void OnEnable()
        {
            playerJoinArea.GetComponent<PlayerJoinedController>().OnPlayersAtSub += playersAtSub;
            playerJoinArea.GetComponent<PlayerJoinedController>().OnPlayersNotAtSubAnymore += playersNotAtSubAnymore;
            playerDeathEventChannel.OnEventPublished += OnDeath;
            submarineController.OnSubDestroyed += OnSubMarineDeath;
            submarineController.OnSubRepaired += OnSubMarineRepaired;
            submarineController.OnSubNotRepairedAnymore += OnSubNotRepairedAnymore;
            artefactPickedUpEventChannel.OnEventPublished += OnArtefactPickedUp;
        }

        private void OnDisable()
        {
            playerJoinArea.GetComponent<PlayerJoinedController>().OnPlayersAtSub -= playersAtSub;
            playerJoinArea.GetComponent<PlayerJoinedController>().OnPlayersNotAtSubAnymore -= playersNotAtSubAnymore;
            playerDeathEventChannel.OnEventPublished -= OnDeath;
            submarineController.OnSubDestroyed -= OnSubMarineDeath;
            submarineController.OnSubRepaired -= OnSubMarineRepaired;
            submarineController.OnSubNotRepairedAnymore += OnSubNotRepairedAnymore;
            artefactPickedUpEventChannel.OnEventPublished -= OnArtefactPickedUp;
        }

        private void playersAtSub()
        {
            playersAreAtSub = true;
            CheckIfGameWon();
        }

        private void playersNotAtSubAnymore()
        {
            playersAreAtSub = false;
        }

        private void OnDeath(PlayerDeathEvent playerDeathEvent)
        {
            remainingPlayers--;
            Debug.Log("A player has been killed...");
            if(remainingPlayers == 0)
            {
                LoseGame();
                Debug.Log("All players were killed :'(...");
            }
        }

        private void OnSubMarineDeath()
        {
            LoseGame();
        }

        private void OnSubMarineRepaired()
        {
            isSubRepaired = true;
            CheckIfGameWon();
        }

        private void OnSubNotRepairedAnymore()
        {
            isSubRepaired = false;
        }

        private void OnArtefactPickedUp(ArtefactPickedUpEvent artefactPickedUpEvent)
        {
            amountOfArtefactPickedUp++;
            CheckIfGameWon();
        }

        private void CheckIfGameWon()
        {
            if (amountOfArtefactPickedUp >= artefactTotal && isSubRepaired && playersAreAtSub)
            {
                levelStateController.GameIsOver(true);
                activityStack.StartMenu(winGameMenu);
                enabled = false;
            }
        }

        private void LoseGame()
        {
            levelStateController.GameIsOver(false);
            activityStack.StartMenu(lostGameMenu);
            enabled = false;
        }
    }
}


