﻿using System.Collections;
using System.Collections.Generic;
using Harmony;
using UnityEngine;

namespace ProjetSynthese
{
    [AddComponentMenu("Game/Control/MonsterController")]
    public class MonsterController : EnnemyController
    {

        [SerializeField]
        [Tooltip("Will determine if the ennemy was spawned by an ennemy spawner or if it was placed in the editor")]
        private bool IAmNotASpawnedMonster;

        protected void InjectMonsterController([TagScope(R.S.Tag.Submarine)] GameObject submarine)
        {
            this.mainTarget = submarine;
        }

        protected override void Awake()
        {
            InjectDependencies("InjectMonsterController");
            startState = StartingStates.Chase;
            base.Awake();
        }

        protected override void Start()
        {
            if (IAmNotASpawnedMonster)
            {
                State = new Chase(mainTarget, pathFinder, this, new List<GameObject>(), false);
            }
            else
            {
                State = new Chase(mainTarget, pathFinder, this, new List<GameObject>(), true);
            }
        }
    }
}