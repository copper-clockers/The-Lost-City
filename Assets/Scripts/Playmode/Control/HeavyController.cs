﻿using System.Collections;
using System.Collections.Generic;
using Harmony;
using UnityEngine;

namespace ProjetSynthese
{
    [AddComponentMenu("Game/Control/HeavyController")]
    public class HeavyController : GardianController
    {
        [SerializeField]
        [Tooltip("The heavy's weapon")]
        private GameObject weapon;

        [SerializeField]
        [Tooltip("Amount of seconds between self-repair.")]
        private float timeBetweenRepair;

        [SerializeField]
        [Tooltip("Amount of repair per each self-repair.")]
        private int amountOfRepair;

        private float timeToGo;
        private RangedWeapon weaponControl;
        private Animator animator;

        private void InjectHeavyController([EntityScope] Animator animator)
        {
            this.animator = animator;
        }

        protected override void Awake()
        {
            GameObject createdWeapon = Instantiate(weapon, transform.position + Vector3.right, transform.rotation, transform);
            createdWeapon.transform.Rotate(0, 0, -90);
            weaponControl = createdWeapon.GetComponent<RangedWeapon>();
            timeToGo = 0;
            InjectDependencies("InjectHeavyController");
            base.Awake();
        }

        protected override void Update()
        {
            if (Time.time >= timeToGo)
            {
                health.Heal(amountOfRepair);
                timeToGo = Time.time + timeBetweenRepair;
            }
            base.Update();
        }

        public override void IsMoving(bool isMoving)
        {
            animator.SetBool("isWalking", isMoving);
        }

        protected override void ExecuteAttack()
        {
            weaponControl.Fire();
            animator.SetTrigger("isAttacking");
        }
    }
}