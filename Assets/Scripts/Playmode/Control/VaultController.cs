﻿using System.Collections;
using System.Collections.Generic;
using ProjetSynthese;
using UnityEngine;
using Harmony;

namespace ProjetSynthese
{
    public class VaultController : GameScript
    {
        [SerializeField]
        [Range(0, 600)]
        [Tooltip("Duration in seconds to open the vault")]
        private float vaultOpenningTimeInSeconds;

        [SerializeField]
        [Tooltip("The visuals of the capture zone when the vault is being openned.")]
        private GameObject captureZoneActiveVisuals;
        [SerializeField]
        [Tooltip("The visuals of the capture zone when the vault is not being openned.")]
        private GameObject captureZoneInactiveVisuals;
        [SerializeField]
        [Tooltip("The collider to disable when the vault unlocks")]
        private GameObject lockedCollider;


        private PlayerDetectSensor nearbyPlayerDetection;
        private int nearbyPlayers = 0;
        private float vaultOpenningProgress = 0;
        private bool vaultIsOpen = false;
        private bool isOpenInOneTry = true;
        private Animator animator;
        private AchievementRepository achivements;
        private float vaultXInitialScale;
        private float vaultYInitialScale;

        private const int achivementId = 7;

        private const float VaultOpenningPointsPerSecond = 1;
        private const float VaultClosingPointsPerSecond = 0.5f;

        private void InjectVaultController([EntityScope] PlayerDetectSensor playerSensor,
                                [EntityScope] Animator animator,
                                [ApplicationScope] AchievementRepository achivementsRepository)
        {
            nearbyPlayerDetection = playerSensor;
            this.animator = animator;
            this.achivements = achivementsRepository;
        }

        private void Awake()
        {
            InjectDependencies("InjectVaultController");
            StartCoroutine(VaultOpenningProgress());
            vaultXInitialScale = captureZoneActiveVisuals.transform.localScale.x;
            vaultYInitialScale = captureZoneActiveVisuals.transform.localScale.y;
            captureZoneActiveVisuals.transform.localScale = new Vector3(0, 0, 1);
        }

        private void OnEnable()
        {
            nearbyPlayerDetection.OnDetectPlayerEnter += IncreasePlayerCount;
            nearbyPlayerDetection.OnDetectPlayerExit += DecreasePlayerCount;
        }

        private void OnDisable()
        {
            nearbyPlayerDetection.OnDetectPlayerEnter -= IncreasePlayerCount;
            nearbyPlayerDetection.OnDetectPlayerExit -= DecreasePlayerCount;
        }

        private void IncreasePlayerCount(GameObject player)
        {
            if(player.GetTopParent().GetComponentInChildren<PlayerController>() != null)
            {
                nearbyPlayers++;
            }
            
            
        }

        private void DecreasePlayerCount(GameObject player)
        {

            if (player.GetTopParent().GetComponentInChildren<PlayerController>() != null)
            {
                nearbyPlayers--;
                if (nearbyPlayers == 0)
                {
                    isOpenInOneTry = false;
                }
            }

        }

        private IEnumerator VaultOpenningProgress()
        {
            while (!vaultIsOpen)
            {
                yield return new WaitForSeconds(1);
                if (nearbyPlayers > 0)
                {
                    vaultOpenningProgress += VaultOpenningPointsPerSecond;
                    captureZoneActiveVisuals.transform.localScale = 
                        new Vector3(vaultOpenningProgress/vaultOpenningTimeInSeconds*vaultXInitialScale, 
                        vaultOpenningProgress / vaultOpenningTimeInSeconds * vaultYInitialScale, 1);
                }
                else if (vaultOpenningProgress > 0)
                {
                    vaultOpenningProgress -= VaultClosingPointsPerSecond;
                    captureZoneActiveVisuals.transform.localScale =
                        new Vector3(vaultOpenningProgress / vaultOpenningTimeInSeconds * vaultXInitialScale,
                            vaultOpenningProgress / vaultOpenningTimeInSeconds * vaultYInitialScale, 1);
                }
                if (vaultOpenningTimeInSeconds <= vaultOpenningProgress)
                {
                    vaultIsOpen = true;
                }
                
            }
            OpenVault();
        }

        private void OpenVault()
        {
            nearbyPlayerDetection.enabled = false;
            captureZoneActiveVisuals.SetActive(false);
            captureZoneInactiveVisuals.SetActive(false);
            animator.SetTrigger("openVault");
            lockedCollider.SetActive(false);

            if (!achivements.GetAnAchievement(achivementId).IsUnlocked && isOpenInOneTry)
            {
                achivements.UnlockAchievement(achivementId);
            }
        }
    }

}


