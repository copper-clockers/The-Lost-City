﻿using System;
using System.Collections;
using System.Collections.Generic;
using Harmony;
using UnityEngine;
using UnityEngine.UI;

namespace ProjetSynthese
{
    public class LevelSelectController : GameScript, IMenuController
    {
        [SerializeField]
        [Tooltip("Text item containing the level description.")]
        private Text levelDescription;

        [SerializeField]
        [Tooltip("Text item containing the level title.")]
        private Text levelTitle;

        [SerializeField]
        [Tooltip("Level activites that can be selected")]
        private Menu characterSelection;

        [SerializeField]
        [Tooltip("Level that was selected")]
        private Activity[] activityLevels;

        [SerializeField]
        [Tooltip("Icones of the levels")]
        private Image[] levelpicture;

        [SerializeField]
        [Tooltip("Cursor object that hovers over the selected level")]
        private Image cursor;

        private PlayerInputSensor playerInputSensor;

        private ActivityStack activityStack;

        private GameActivityParameters gameActivityParameters;

        private LevelRepository levelRepository;

        private List<Level> levels;

        private int selectedLevel = 1;
        private int selectedPlayerQuantity = 1;
        private const int maxPlayerQuantity = 4;

        private const float safetyDelay = 0.1f;

        private void InjectLevelSelectController([ApplicationScope] ActivityStack activityStack,
            [ApplicationScope] GameActivityParameters gameActivityParameters,
            [ApplicationScope] PlayerInputSensor playerInputSensor,
            [EntityScope] LevelRepository levelRepository)
        {
            this.activityStack = activityStack;
            this.gameActivityParameters = gameActivityParameters;
            this.playerInputSensor = playerInputSensor;
            this.levelRepository = levelRepository;
        }

        private void Awake()
        {
            InjectDependencies("InjectLevelSelectController");
        }

        public void OnCreate(params object[] parameters)
        {
            levels = (List<Level>)levelRepository.GetAllLevels();

        }

        public void OnResume()
        {
            EnterLevelSelection();
        }

        public void OnPause()
        {
            ExitLevelSelection();
        }

        public void OnStop()
        {
            
        }

        private void UpdateSelectedLevel()
        {
            for (int i = 0; i < levelpicture.Length; i++)
            {
                if (i == selectedLevel - 1)
                {
                    cursor.rectTransform.SetParent(levelpicture[i].rectTransform);
                    cursor.rectTransform.position = cursor.rectTransform.parent.position;
                    levelTitle.text = levels[i].NameLevel;
                    levelDescription.text = levels[i].DescriptionLevel;
                }
                else
                {

                }
            }
        }

        private void UpdateSelectedPlayerAmount()
        {
            for (int i = 1; i < cursor.rectTransform.childCount; i++)
            {
                if (i == selectedPlayerQuantity)
                {
                    cursor.rectTransform.GetChild(i).GetComponent<Image>().color = Color.green;
                }
                else
                {
                    cursor.rectTransform.GetChild(i).GetComponent<Image>().color = Color.white;
                }
            }
        }

        private void OnSelectLevel()
        {
            ExitLevelSelection();
            EnterPlayerSelection();
        }

        private void OnCancelLevelSelect()
        {
            ExitPlayerSelection();
            EnterLevelSelection();
        }

        private void OnStartLevel()
        {
            ExitPlayerSelection();
            gameActivityParameters.NumberOfPlayers = selectedPlayerQuantity;
            gameActivityParameters.SelectedLevel = activityLevels[selectedLevel - 1];
            activityStack.StartMenu(characterSelection);

        }

        private void OnReturnToMainMenu()
        {
            activityStack.StopCurrentMenu();
        }

        private void OnChangeSelectedLevelHorizontal(float intensity)
        {

            selectedLevel = UpdateSelectNumberHorizontal(intensity, selectedLevel);
            UpdateSelectedLevel();
        }

        private void OnChangeSelectedLevelVertical(float intensity)
        {
            selectedLevel = UpdateSelectNumberVertical(intensity, selectedLevel);
            UpdateSelectedLevel();
        }

        private void OnChangeSelectedPlayerAmountHorizontal(float intensity)
        {
            selectedPlayerQuantity = UpdateSelectNumberHorizontal(intensity, selectedPlayerQuantity);
            UpdateSelectedPlayerAmount();
        }

        private void OnChangeSelectedPlayerAmountVertical(float intensity)
        {
            selectedPlayerQuantity = UpdateSelectNumberVertical(intensity, selectedPlayerQuantity);
            UpdateSelectedPlayerAmount();
        }

        private int UpdateSelectNumberHorizontal(float intensity, int cursorValue)
        {
            if (intensity > 0 && !IsSelectedItemOnRightSide(cursorValue))
            {
                return cursorValue+1;
            }
            else if (intensity < 0 && IsSelectedItemOnRightSide(cursorValue))
            {
                return cursorValue-1;
            }
            return cursorValue;
        }
        private int UpdateSelectNumberVertical(float intensity, int cursorValue)
        {
            if (intensity < 0 && !IsSelectedItemLowerHalf(cursorValue))
            {
                return cursorValue + 2;
            }
            if (intensity > 0 && IsSelectedItemLowerHalf(cursorValue))
            {
                return cursorValue - 2;
            }
            return cursorValue;
        }

        private void EnterLevelSelection()
        {
            UpdateSelectedLevel();
            playerInputSensor.Players[0].OnMoveX += OnChangeSelectedLevelHorizontal;
            playerInputSensor.Players[0].OnMoveY += OnChangeSelectedLevelVertical;
            StartCoroutine(SafetyDelayBeforeSelectPlayers());
        }

        private void EnterPlayerSelection()
        {
            playerInputSensor.Players[0].OnMoveX += OnChangeSelectedPlayerAmountHorizontal;
            playerInputSensor.Players[0].OnMoveY += OnChangeSelectedPlayerAmountVertical;
            foreach (RectTransform child in cursor.rectTransform)
            {
                child.gameObject.SetActive(true);
            }
            StartCoroutine(SafetyDelayBeforeStartGame());
            UpdateSelectedPlayerAmount();
        }

        private void ExitLevelSelection()
        {
            playerInputSensor.Players[0].OnMoveX -= OnChangeSelectedLevelHorizontal;
            playerInputSensor.Players[0].OnMoveY -= OnChangeSelectedLevelVertical;
            playerInputSensor.Players[0].OnSkill1 -= OnSelectLevel;
            playerInputSensor.Players[0].OnBuild -= OnReturnToMainMenu;
        }

        private void ExitPlayerSelection()
        {
            playerInputSensor.Players[0].OnMoveX -= OnChangeSelectedPlayerAmountHorizontal;
            playerInputSensor.Players[0].OnMoveY -= OnChangeSelectedPlayerAmountVertical;
            playerInputSensor.Players[0].OnSkill1 -= OnStartLevel;
            playerInputSensor.Players[0].OnBuild -= OnCancelLevelSelect;
            foreach (RectTransform child in cursor.rectTransform)
            {
                child.gameObject.SetActive(false);
            }
        }
        
        private bool IsSelectedItemOnRightSide(int cursorValue)
        {
            return cursorValue % 2 == 0;
        }

        private bool IsSelectedItemLowerHalf(int cursorValue)
        {
            return cursorValue > 2;
        }

        private IEnumerator SafetyDelayBeforeSelectPlayers()
        {
            yield return new WaitForSeconds(safetyDelay);
            playerInputSensor.Players[0].OnSkill1 += OnSelectLevel;
            playerInputSensor.Players[0].OnBuild += OnReturnToMainMenu;
        }
        private IEnumerator SafetyDelayBeforeStartGame()
        {
            yield return new WaitForSeconds(safetyDelay);
            playerInputSensor.Players[0].OnSkill1 += OnStartLevel;
            playerInputSensor.Players[0].OnBuild += OnCancelLevelSelect;
        }


    }

}


