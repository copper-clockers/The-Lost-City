﻿using Harmony;
using UnityEngine;

namespace ProjetSynthese
{
    [AddComponentMenu("Game/Control/GetAchievementForKillingEnemies")]
    public class GetAchievementForKillingEnemies : GameScript
    {
        private int enemiesKilled;
        public int EnemiesKilled
        {
            get { return enemiesKilled; }
            set { enemiesKilled = value; }
        }

        private bool achievementObtained;

        private AchievementRepository achievementRepository;

        private void InjectGetAchievementForKillingEnemies([ApplicationScope] AchievementRepository achievementRepository)
        {
            this.achievementRepository = achievementRepository;
        }

        private void Awake()
        {
            InjectDependencies("InjectGetAchievementForKillingEnemies");
            achievementObtained = false;
        }

        private void Update()
        {
            if(enemiesKilled >= 50 && achievementObtained == false)
            {
                achievementRepository.UnlockAchievement(3);
                achievementObtained = true;
            }
        }

    }
}
