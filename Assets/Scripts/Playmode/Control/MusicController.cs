﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ProjetSynthese
{
    public class MusicController : GameScript
    {
        [SerializeField]
        [Tooltip("The music played during the average of the level")]
        private AudioSource levelMusic;

        [SerializeField]
        [Tooltip("The music played during the leviathanEncounter")]
        private AudioSource leviathanMusic;

        private Coroutine loopLeviathanThemeCoroutine;

        private const float LeviathanLoopTime = 34f;

        public void EncounterLeviathan()
        {
            levelMusic.Pause();
            leviathanMusic.Play();
            loopLeviathanThemeCoroutine = StartCoroutine(LoopLeviathanTheme());
        }

        public void LeviathanDeath()
        {
            StopCoroutine(loopLeviathanThemeCoroutine);
            leviathanMusic.Stop();
            levelMusic.UnPause();
            
        }

        private IEnumerator LoopLeviathanTheme()
        {
            while (true)
            {
                yield return new WaitForSeconds(leviathanMusic.clip.length - leviathanMusic.time);
                leviathanMusic.time = LeviathanLoopTime;
            }
            

        }
    }

}


