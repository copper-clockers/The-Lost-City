﻿using Harmony;
using UnityEngine;
using UnityEngine.UI;

namespace ProjetSynthese
{
    [AddComponentMenu("Game/Control/MainMenuController")]
    public class MainMenuController : GameScript, IMenuController
    {
        [SerializeField]
        private Menu menuLevel;

        [SerializeField]
        private Menu menuStats;
        
        [SerializeField]
        private Menu menuAchievements;

        [SerializeField]
        private Menu menuOptions;

        [SerializeField]
        private Activity activityCredits;

        [SerializeField]
        private Selectable buttonStart;

        [SerializeField]
        private Selectable buttonStats;

        [SerializeField]
        private Selectable buttonAchievements;

        [SerializeField]
        private Selectable buttonOptions;

        [SerializeField]
        private Selectable buttonCredits;

        [SerializeField]
        private Selectable buttonExit;

        private Selectable currentSelected;

        private ActivityStack activityStack;

        private GameActivityParameters gameActivityParameters;

        private PlayerInputSensor playerInputSensor;

        private void InjectMainMenuController([ApplicationScope] ActivityStack activityStack, 
                                              [ApplicationScope] GameActivityParameters gameActivityParameters,
                                              [ApplicationScope] PlayerInputSensor playerInputSensor)
        {
            this.activityStack = activityStack;
            this.gameActivityParameters = gameActivityParameters;
            this.playerInputSensor = playerInputSensor;
        }

        private void Awake()
        {
            InjectDependencies("InjectMainMenuController");
            currentSelected = null;
        }

        public void OnCreate(params object[] parameters)
        {
            //Nothing to do
        }

        public void OnResume()
        {
            buttonStart.Select();
            currentSelected = buttonStart;
        }

        public void OnPause()
        {
            //Nothing to do
        }

        public void OnStop()
        {
            //Nothing to do
        }

        [CalledOutsideOfCode]
        public void StartGame()
        {
            activityStack.StartMenu(menuLevel);
        }

        [CalledOutsideOfCode]
        public void StartStats()
        {
            activityStack.StartMenu(menuStats);
        }

        [CalledOutsideOfCode]
        public void StartAchievements()
        {
            activityStack.StartMenu(menuAchievements);
        }

        [CalledOutsideOfCode]
        public void StartOptions()
        {
            activityStack.StartMenu(menuOptions);
        }

        [CalledOutsideOfCode]
        public void StartCredits()
        {
            activityStack.StartActivity(activityCredits);
        }

        [CalledOutsideOfCode]
        public void QuitGame()
        {
            activityStack.StopCurrentActivity();
        }
    }
}