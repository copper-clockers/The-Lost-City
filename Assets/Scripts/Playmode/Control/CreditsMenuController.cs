﻿using Harmony;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace ProjetSynthese
{
    [AddComponentMenu("Game/Control/CreditsMenuController")]
    public class CreditsMenuController : GameScript, IMenuController
    {

        [SerializeField]
        private GameObject CreditsText;

        [SerializeField]
        private int SecondsBeforeLeavingCredits;

        [SerializeField]
        [Range(0,60)]
        private float ScrollSpeed;

        private float secondsSinceStart;

        private ActivityStack activityStack;

        private GameActivityParameters gameActivityParameters;

        private AchievementRepository achievementRepository;

        private PlayerInputSensor playerInputSensor;

        private void InjectCreditsMenuController([ApplicationScope] ActivityStack activityStack,
                                                 [ApplicationScope] GameActivityParameters gameActivityParameters,
                                                 [ApplicationScope] PlayerInputSensor playerInputSensor,
                                                 [ApplicationScope] AchievementRepository achievementRepository)
        {
            this.activityStack = activityStack;
            this.gameActivityParameters = gameActivityParameters;
            this.playerInputSensor = playerInputSensor;
            this.achievementRepository = achievementRepository;
        }

        private void Awake()
        {
            InjectDependencies("InjectCreditsMenuController");
        }

        private void OnEnable()
        {
            playerInputSensor.Players[0].OnBuild += OnExit;
        }

        private void OnDisable()
        {
            playerInputSensor.Players[0].OnBuild -= OnExit;
        }

        private void Update()
        {
            CreditsText.transform.Translate(Vector3.up*ScrollSpeed*60*Time.deltaTime);

        }

        private IEnumerator WaitForEndOfScroll()
        {
            yield return new WaitForSeconds(SecondsBeforeLeavingCredits);
            CreditsDoneScrolling();
        }

        public void OnCreate(params object[] parameters)
        {
            StartCoroutine(WaitForEndOfScroll());
            //Nothing to do
        }

        public void OnResume()
        {
            //Nothing to do
        }

        public void OnPause()
        {
            //Nothing to do
        }

        public void OnStop()
        {
            //Nothing to do
        }

        public void CreditsDoneScrolling()
        {
            achievementRepository.UnlockAchievement(9);
            activityStack.StopCurrentActivity();
        }

        private void OnExit()
        {
            activityStack.StopCurrentActivity();
        }
    }
}
