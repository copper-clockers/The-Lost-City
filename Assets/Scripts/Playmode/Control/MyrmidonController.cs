﻿using System.Collections;
using System.Collections.Generic;
using Harmony;
using UnityEngine;

namespace ProjetSynthese
{
    public class MyrmidonController : MonsterController
    {
        private MeleeWeaponController meleeWeaponController;
        private Animator animator;
        private int attackRate = 3;
        private float timeActionEnd;

        private void InjectMyrmidonController([EntityScope] MeleeWeaponController meleeWeaponController,
                                              [EntityScope] Animator animator)
        {
            this.meleeWeaponController = meleeWeaponController;
            this.animator = animator;
        }

        

        protected override void Awake()
        {
            InjectDependencies("InjectMyrmidonController");
            timeActionEnd = Time.time;
            base.Awake();
        }

        protected override void ExecuteAttack()
        {
            if (timeActionEnd < Time.time)
            {
                meleeWeaponController.Attack();
                animator.SetTrigger("isAttacking");
                timeActionEnd = Time.time + attackRate;
            }
        }

        public override void IsMoving(bool isMoving)
        {
            animator.SetBool("isWalking",isMoving);
        }
    }
}


