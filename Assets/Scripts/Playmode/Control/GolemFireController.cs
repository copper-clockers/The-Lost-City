﻿using Harmony;
using UnityEngine;

namespace ProjetSynthese
{
    public class GolemFireController : MonsterController
    {
        [SerializeField]
        [Tooltip("Weapon used by the golem.")]
        private GameObject weapon;
        
        private ProjectileShooter projectileShooter;
        private Animator animator;
        private int attackRate = 3;
        private float timeActionEnd;
        private GetAchievementForKillingEnemies getAchievementForKillingEnemies;

        private void InjectGolemController([EntityScope] Animator animator,
                                           [SceneScope] GetAchievementForKillingEnemies getAchievementForKillingEnemies)
        {
            this.animator = animator;
            this.getAchievementForKillingEnemies = getAchievementForKillingEnemies;
        }

        protected override void Awake()
        {
            GameObject createdWeapon = Instantiate(weapon, transform.position + Vector3.right, transform.rotation, transform);
            createdWeapon.transform.Rotate(0, 0, -90);
            projectileShooter = createdWeapon.GetComponent<ProjectileShooter>();

            InjectDependencies("InjectGolemController");
            timeActionEnd = Time.time;
            base.Awake();
            stats.AddLavaAbsorbtion();
        }

        protected override void ExecuteAttack()
        {
            if (timeActionEnd < Time.time)
            {
                projectileShooter.Fire();
                animator.SetTrigger("isAttacking");
                timeActionEnd = Time.time + attackRate;
            }
        }

        public override void IsMoving(bool isMoving)
        {
            animator.SetBool("isWalking", isMoving);
        }

        public void OnDestroy()
        {
            getAchievementForKillingEnemies.EnemiesKilled++;
        }
    }
}
