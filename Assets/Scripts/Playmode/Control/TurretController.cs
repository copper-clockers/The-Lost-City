﻿using System;
using System.Collections.Generic;
using Harmony;
using UnityEngine;

namespace ProjetSynthese
{
    [AddComponentMenu("Game/Control/TurretController")]
    public class TurretController : GameScript
    {
        [SerializeField]
        [Tooltip("Health and Armor Bar.")]
        private GameObject healthArmorBarPrefab;

        private DestroyedTurretEventChannel destroyedTurretEventChannel;
        private KillableObject healthControl;
        private RangedWeapon rangedWeapon;
        private FaceNearbyEnemy enemyTargetFinder;
        private GameObject healthArmorBar;
        private GameObject healthBar;
        private GameObject armorBar;
        private Vector3 maxScale;

        private void InjectTurretController([EventChannelScope] DestroyedTurretEventChannel destroyedTurretEventChannel,
                                            [GameObjectScope] KillableObject health,
                                            [EntityScope] FaceNearbyEnemy targetFinder,
                                            [EntityScope] RangedWeapon rangedWeapon)
        {
            healthControl = health;
            enemyTargetFinder = targetFinder;
            this.rangedWeapon = rangedWeapon;
            this.destroyedTurretEventChannel = destroyedTurretEventChannel;
        }

        private void Awake()
        {
            InjectDependencies("InjectTurretController");
            healthArmorBar = null;
        }

        private void Start()
        {
            if (healthArmorBarPrefab != null)
            {
                healthArmorBar = Instantiate(healthArmorBarPrefab);
                ApplyPositionOfBar();
                GetArmorAndHealthBar();
                maxScale = armorBar.transform.localScale;
            }
        }

        private void OnEnable()
        {
            healthControl.OnHealthChanged += OnHealthChanged;
            healthControl.OnArmorChanged += OnArmorChanged;
            healthControl.OnDeath += TurretWasDestroyed;
        }

        private void OnDisable()
        {
            healthControl.OnHealthChanged -= OnHealthChanged;
            healthControl.OnArmorChanged -= OnArmorChanged;
            healthControl.OnDeath -= TurretWasDestroyed;

            if (healthArmorBar != null)
            {
                Destroy(healthArmorBar);
            }
        }

        private void Update()
        {
            if (enemyTargetFinder.GetTarget() != null)
            {
                rangedWeapon.Fire();
            }
            ApplyPositionOfBar();
        }

        public void OnHitReceive(int damagePoints)
        {
            healthControl.ReceiveDamage(damagePoints);
        }

        private void TurretWasDestroyed()
        {
            destroyedTurretEventChannel.Publish(new DestroyedTurretEvent());
        }

        public void ShowVisualHint(bool showHint)
        {
            GameObject hint = GetVisualHint();
            if (hint != null && healthControl.GetCurrentHealth()< healthControl.GetCurrentMaxHealth())
            {
                hint.SetActive(showHint);
            }
            else if (hint != null && healthControl.GetCurrentHealth() == healthControl.GetCurrentMaxHealth())
            {
                hint.SetActive(false);
            }
        }

        private GameObject GetVisualHint()
        {
            GameObject turret = gameObject.GetTopParent();
            IList<GameObject> liste = turret.GetAllChildrens();
            foreach (GameObject possibleHint in liste)
            {
                if (possibleHint.name.Equals("Hint"))
                {
                    return possibleHint;
                }
            }
            return null;
        }

        private void OnHealthChanged(int currentHealthPoints, int maxHealthPoints)
        {
            Vector3 newScale = new Vector3(currentHealthPoints * maxScale.x / maxHealthPoints, maxScale.y);
            healthBar.transform.localScale = newScale;
        }

        private void OnArmorChanged(int currentArmor, int maxArmor)
        {
            Vector3 newScale = new Vector3(currentArmor * maxScale.x / maxArmor, maxScale.y);
            armorBar.transform.localScale = newScale;
        }

        private void ApplyPositionOfBar()
        {
            if (healthArmorBarPrefab != null)
            {
                healthArmorBar.transform.position = new Vector3(transform.position.x, transform.position.y - 0.5f, transform.position.z);
            }
        }

        private void GetArmorAndHealthBar()
        {
            IList<GameObject> bars = healthArmorBar.GetAllChildrens();
            foreach (GameObject bar in bars)
            {
                if (bar.name.Equals("HealthBar"))
                {
                    healthBar = bar;
                }
                else if (bar.name.Equals("ArmorBar"))
                {
                    armorBar = bar;
                }
            }
        }
    }
}