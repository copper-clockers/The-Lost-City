﻿using Harmony;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace ProjetSynthese
{
    [AddComponentMenu("Game/Control/GlobalMenuController")]
    public class GlobalMenuController : GameScript
    {
        private const float SecondsBetweenCooldown = 0.25f;

        private PlayerInputSensor playerInputSensor;

        private bool isJoystickInCooldown;
        
        private void InjectGlobalMenuController([ApplicationScope] PlayerInputSensor playerInputSensor)
        {
            this.playerInputSensor = playerInputSensor;
        }

        private void Awake()
        {
            InjectDependencies("InjectGlobalMenuController");

            playerInputSensor.Players.OnUp += OnUp;
            playerInputSensor.Players.OnDown += OnDown;
            playerInputSensor.Players.OnConfirm += OnConfirm;

            playerInputSensor.Players.OnMoveX += OnMoveJoystickX;
            playerInputSensor.Players.OnMoveY += OnMoveJoystickY;
        }

        private void OnDestroy()
        {
            playerInputSensor.Players.OnUp -= OnUp;
            playerInputSensor.Players.OnDown -= OnDown;
            playerInputSensor.Players.OnConfirm -= OnConfirm;

            playerInputSensor.Players.OnMoveX -= OnMoveJoystickX;
            playerInputSensor.Players.OnMoveY -= OnMoveJoystickY;
        }

        private void OnUp()
        {
            Selectable currentSelectable = SelectableExtensions.GetCurrentlySelected();
            if (currentSelectable != null)
            {
                currentSelectable.SelectPrevious();
            }
        }

        private void OnDown()
        {
            Selectable currentSelectable = SelectableExtensions.GetCurrentlySelected();
            if (currentSelectable != null)
            {
                currentSelectable.SelectNext();
            }
        }

        private void OnSelectPreviousHorizontal()
        {
            Selectable currentSelectable = SelectableExtensions.GetCurrentlySelected();
            if (currentSelectable != null)
            {
                currentSelectable.SelectPreviousHorizontal();
            }
        }

        private void OnSelectNextHorizontal()
        {
            Selectable currentSelectable = SelectableExtensions.GetCurrentlySelected();
            if (currentSelectable != null)
            {
                currentSelectable.SelectNextHorizontal();
            }
        }

        private void OnConfirm()
        {
            Selectable currentSelectable = SelectableExtensions.GetCurrentlySelected();
            if (currentSelectable != null)
            {
                currentSelectable.Click();
            }
        }

        private void OnMoveJoystickX(float xAxis)
        {
            if (!isJoystickInCooldown)
            {
                if (xAxis > 0)
                {
                    OnSelectPreviousHorizontal();
                }
                else if (xAxis < 0)
                {
                    OnSelectNextHorizontal();
                }
                StartCoroutine(JoystickInputCooldown());
            }
        }

        private void OnMoveJoystickY(float yAxis)
        {
            if(!isJoystickInCooldown)
            {
                if (yAxis > 0)
                {
                    OnUp();
                }
                else if (yAxis < 0)
                {
                    OnDown();
                }
                StartCoroutine(JoystickInputCooldown());
            }
        }
        
        private IEnumerator JoystickInputCooldown()
        {
            isJoystickInCooldown = true;
            yield return new WaitForSecondsRealtime(SecondsBetweenCooldown);
            isJoystickInCooldown = false;
        }
    }
}