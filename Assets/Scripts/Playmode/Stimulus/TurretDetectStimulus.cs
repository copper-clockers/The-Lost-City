﻿using System;
using System.Collections;
using System.Collections.Generic;
using Harmony;
using UnityEngine;

namespace ProjetSynthese
{
    public delegate void TurretDetectStimulusEventHandler(GameObject player);

    [AddComponentMenu("Game/Stimulus/TurretDetectStimulus")]
    public class TurretDetectStimulus : GameScript
    {
        private new Collider2D collider2D;

        public event TurretDetectStimulusEventHandler OnTurretDetectEnter;
        public event TurretDetectStimulusEventHandler OnTurretDetectExit;

        private void InjectTurretDetectStimulus([GameObjectScope] Collider2D collider2D)
        {
            this.collider2D = collider2D;
        }

        private void Awake()
        {
            InjectDependencies("InjectTurretDetectStimulus");

            int layer = LayerMask.NameToLayer(R.S.Layer.TurretDetectSensor);
            if (layer == -1)
            {
                throw new Exception("In order to use a PlayerDetectStimulus, you must have a " + R.S.Layer.TurretDetectSensor + " layer.");
            }
            gameObject.layer = layer;
            collider2D.isTrigger = true;
        }

        private void OnEnable()
        {
            collider2D.Events().OnEnterTrigger += OnEnterTrigger;
            collider2D.Events().OnExitTrigger += OnExitTrigger;
        }

        private void OnDisable()
        {
            collider2D.Events().OnEnterTrigger -= OnEnterTrigger;
            collider2D.Events().OnExitTrigger -= OnExitTrigger;
        }

        private void OnEnterTrigger(Collider2D other)
        {
            TurretDetectSensor turretDetectSensor = other.GetComponent<TurretDetectSensor>();
            if (turretDetectSensor != null)
            {
                turretDetectSensor.TurretDetectEnter(this.gameObject);

                if (OnTurretDetectEnter != null) OnTurretDetectEnter(this.gameObject);
            }
        }

        private void OnExitTrigger(Collider2D other)
        {
            TurretDetectSensor turretDetectSensor = other.GetComponent<TurretDetectSensor>();
            if (turretDetectSensor != null)
            {
                turretDetectSensor.TurretDetectExit(this.gameObject);

                if (OnTurretDetectExit != null) OnTurretDetectExit(this.gameObject);
            }
        }
    }
}