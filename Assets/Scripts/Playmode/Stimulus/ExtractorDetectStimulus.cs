﻿using System;
using System.Collections;
using System.Collections.Generic;
using Harmony;
using UnityEngine;

namespace ProjetSynthese
{
    public delegate void ExtractorDetectStimulusEventHandler(GameObject player);

    [AddComponentMenu("Game/Stimulus/ExtractorDetectStimulus")]
    public class ExtractorDetectStimulus : GameScript
    {
        private new Collider2D collider2D;

        public event ExtractorDetectStimulusEventHandler OnExtractorDetectEnter;
        public event ExtractorDetectStimulusEventHandler OnExtractorDetectExit;

        private void InjectExtractorDetectStimulus([GameObjectScope] Collider2D collider2D)
        {
            this.collider2D = collider2D;
        }

        private void Awake()
        {
            InjectDependencies("InjectExtractorDetectStimulus");

            int layer = LayerMask.NameToLayer(R.S.Layer.ExtractorDetectSensor);
            if (layer == -1)
            {
                throw new Exception("In order to use a PlayerDetectStimulus, you must have a " + R.S.Layer.ExtractorDetectSensor + " layer.");
            }
            gameObject.layer = layer;
            collider2D.isTrigger = true;
        }

        private void OnEnable()
        {
            collider2D.Events().OnEnterTrigger += OnEnterTrigger;
            collider2D.Events().OnExitTrigger += OnExitTrigger;
        }

        private void OnDisable()
        {
            collider2D.Events().OnEnterTrigger -= OnEnterTrigger;
            collider2D.Events().OnExitTrigger -= OnExitTrigger;
        }

        private void OnEnterTrigger(Collider2D other)
        {
            ExtractorDetectSensor extractorDetectSensor = other.GetComponent<ExtractorDetectSensor>();
            if (extractorDetectSensor != null)
            {
                extractorDetectSensor.ExtractorDetectEnter(this.gameObject);

                if (OnExtractorDetectEnter != null) OnExtractorDetectEnter(this.gameObject);
            }
        }

        private void OnExitTrigger(Collider2D other)
        {
            ExtractorDetectSensor extractorDetectSensor = other.GetComponent<ExtractorDetectSensor>();
            if (extractorDetectSensor != null)
            {
                extractorDetectSensor.ExtractorDetectExit(this.gameObject);

                if (OnExtractorDetectExit != null) OnExtractorDetectExit(this.gameObject);
            }
        }
    }
}