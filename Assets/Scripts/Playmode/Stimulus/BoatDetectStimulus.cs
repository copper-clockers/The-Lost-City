﻿using System;
using UnityEngine;
using Harmony;

namespace ProjetSynthese
{
    public delegate void BoatDetectStimulusEventHandler(GameObject player);

    [AddComponentMenu("Game/Stimulus/BoatDetectStimulus")]
    public class BoatDetectStimulus : GameScript
    {
        private new Collider2D collider2D;

        public event BoatDetectStimulusEventHandler OnBoatDetectEnter;
        public event BoatDetectStimulusEventHandler OnBoatDetectExit;

        private void InjectBoatDetectStimulus([GameObjectScope] Collider2D collider2D)
        {
            this.collider2D = collider2D;
        }

        private void Awake()
        {
            InjectDependencies("InjectBoatDetectStimulus");

            int layer = LayerMask.NameToLayer(R.S.Layer.BoatDetectSensor);
            if (layer == -1)
            {
                throw new Exception("In order to use a BoatDetectStimulus, you must have a " + R.S.Layer.BoatDetectSensor + " layer.");
            }
            gameObject.layer = layer;
            collider2D.isTrigger = true;
        }

        private void OnEnable()
        {
            collider2D.Events().OnEnterTrigger += OnEnterTrigger;
            collider2D.Events().OnExitTrigger += OnExitTrigger;
        }

        private void OnDisable()
        {
            collider2D.Events().OnEnterTrigger -= OnEnterTrigger;
            collider2D.Events().OnExitTrigger -= OnExitTrigger;
        }

        private void OnEnterTrigger(Collider2D other)
        {
            BoatDetectSensor boatDetectSensor = other.GetComponent<BoatDetectSensor>();
            if (boatDetectSensor != null)
            {
                if (OnBoatDetectEnter != null) OnBoatDetectEnter(other.gameObject.GetTopParent());
            }
        }

        private void OnExitTrigger(Collider2D other)
        {
            BoatDetectSensor boatDetectSensor = other.GetComponent<BoatDetectSensor>();
            if (boatDetectSensor != null)
            {
                if (OnBoatDetectExit != null) OnBoatDetectExit(other.gameObject.transform.parent.gameObject);
            }
        }
    }
}
