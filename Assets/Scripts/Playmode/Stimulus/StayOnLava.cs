﻿using System.Collections;
using System.Collections.Generic;
using Harmony;
using UnityEngine;

namespace ProjetSynthese
{
    public class StayOnLava : GameScript
    {
        private GetAchievementForStayingOnLava getAchievementForStayingOnLava;

        private bool isOnLava;

        private void InjectStayOnLava([SceneScope] GetAchievementForStayingOnLava getAchievementForStayingOnLava)
        {
            this.getAchievementForStayingOnLava = getAchievementForStayingOnLava;
        }

        private void Awake()
        {
            InjectDependencies("InjectStayOnLava");
            isOnLava = false;
        }
        
        private void OnTriggerStay2D(Collider2D other)
        {
            if (other.gameObject.GetTopParent().tag == R.S.Tag.Player)
            {
                isOnLava = true;
                StartCoroutine(StayForTenSeconds());
            }
        }

        private void OnTriggerExit2D(Collider2D other)
        {
            isOnLava = false;
            StopCoroutine(StayForTenSeconds());
        }
        
        private IEnumerator StayForTenSeconds()
        {
            while(true)
            {
                yield return new WaitForSeconds(10f);

                if (isOnLava)
                {
                    getAchievementForStayingOnLava.StayingTenSecondsOnLava();
                }

            }
        }
    }

}