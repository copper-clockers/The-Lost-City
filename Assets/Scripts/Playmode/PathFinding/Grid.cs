﻿using System;
using System.Collections.Generic;
using Harmony;
using UnityEngine;
using System.IO;
using System.Threading;
using System.Linq;

namespace ProjetSynthese
{
    public enum PathfindingMethod
    {
        Astar,
        Dijkstra,
        Basic
    }
    [AddComponentMenu("Game/State/Grid")]
    public class Grid : GameScript
    {
        [Tooltip("Show neibors of all shown nodes")]
        [SerializeField]
        private bool showNeighbours;

        [Tooltip("Will prevent problems between the threads in A*")]
        [SerializeField]
        private bool dontUseIsVisited;

        [Tooltip("The pathfinding method that will be used")]
        [SerializeField]
        private PathfindingMethod pathfindingMethod;

        [Tooltip("Position en haut à gauche de la grille.")]
        [SerializeField]
        private GameObject lowerLeftStartingPoint;

        [Tooltip("Position en bas à droite de la grille.")]
        [SerializeField]
        private GameObject upperRightEndingPoint;

        [Tooltip("Taille d'un carré de la grille.")]
        [Range(0.35f, 100f)]
        [SerializeField]
        private float nodeSize = 1;

        //[HideInInspector]
        [SerializeField]
        [Tooltip("Nombre de ligne de la grille. Invisible dans l'inspecteur, car calculée.")]
        private int nbRows = 0;

        //[HideInInspector]
        [SerializeField]
        [Tooltip("Nombre de colonnes de la grille. Invisible dans l'inspecteur, car calculée.")]
        private int nbColumns = 0;

        [HideInInspector]
        [SerializeField]
        [Tooltip("Véritable grille. Invisible dans l'inspecteur, car calculée. Une seule dimension pour des questions de performances.")]
        //nodes dans l'ordre de bas en haut et de gauche à droite
        private Node[] grid = new Node[] { };

        private bool isAlive = true;

        public PathfindingMethod PathfindingMethod
        {
            get
            {
                return pathfindingMethod;
            }
        }

        public float NodeSize
        {
            get
            {
                return nodeSize;
            }

            set
            {
                nodeSize = value;
            }
        }

        public void Kill()
        {
            isAlive = false;
        }

        public void Resurect()
        {
            isAlive = true;
        }

        public void Create()
        {
            DeleteCurrentGrid();

            CreateNewGrid();
            Debug.Log("Grid done");
        }

        public void Clear()
        {
            DeleteCurrentGrid();
        }

        public Grid CopyGrid()
        {
            Grid temp = new Grid();
            temp.pathfindingMethod = pathfindingMethod;
            temp.grid = grid;
            temp.nbRows = nbRows;
            temp.nbColumns = nbColumns;
            temp.dontUseIsVisited = dontUseIsVisited;
            temp.NodeSize = NodeSize;
            temp.lowerLeftStartingPoint = lowerLeftStartingPoint;
            temp.upperRightEndingPoint = upperRightEndingPoint;
            temp.showNeighbours = showNeighbours;
            return temp;
        }

        public void CopyNodes()
        {
            Node[] temp = grid;
            grid = (Node[])temp.Clone();
            FindAdjacentNodes();
        }

        public void DrawDebug()
        {
            for (int i = 0; i < nbColumns; i++)
            {

                for (int j = 0; j < nbRows; j++)
                {
                    Node node = GetNodeInGrid(i, j);
                    Debug.DrawLine(node.Position - Vector2.one / 10,
                                   node.Position + Vector2.one / 10,
                                   node.IsOnWall ? Color.red : Color.green);
                    if (showNeighbours)
                    {
                        foreach (Node neighbour in node.Neighbours)
                        {
                            Debug.DrawLine(node.Position - Vector2.one / 10,
                                   node.Position + Vector2.one / 10,
                                   node.IsOnWall ? Color.red : Color.green);
                        }
                    }
                }

            }

        }

        public void DrawNeighbours()
        {
            foreach (Node node in grid)
            {
                for (int i = 0; i < node.Neighbours.Length; i++)
                {
                    Debug.DrawLine(node.Position,
                                   node.Neighbours[i].Position,
                                   Color.cyan);
                }
            }
        }

        public void UnvisitAllNodes()
        {
            foreach (Node node in grid)
            {
                node.IsVisited = false;
            }
        }

        private void DeleteCurrentGrid()
        {
#if UNITY_EDITOR

            //Make grid size 0
            nbRows = 0;
            nbColumns = 0;

            //Delete all nodes
            for (int i = grid.Length - 1; i >= 0; i--)
            {
                DestroyImmediate(grid[i]);
            }
            grid = new Node[] { };

            //Make a cleanup of the remaining nodes in the scene.
            Node[] allNodes = FindObjectsOfType<Node>();
            for (int i = allNodes.Length - 1; i >= 0; i--)
            {
                DestroyImmediate(allNodes[i]);
            }

#endif
        }

        private void CreateNewGrid()
        {
#if UNITY_EDITOR

            //We dont know how many nodes will be created. At the end, we'll convert this list to an array.
            List<Node> newGrid = new List<Node>();

            //Check from left to right.
            Vector2 currentNodePosition = lowerLeftStartingPoint.transform.position;
            while (currentNodePosition.x <= upperRightEndingPoint.transform.position.x)
            {
                int currentNbRows = 0;
                //Check from bottom to top.
                while (currentNodePosition.y <= upperRightEndingPoint.transform.position.y)
                {
                    Node node = ScriptableObject.CreateInstance<Node>();
                    node.Position = currentNodePosition;
                    node.IsOnWall = CheckIsOnWall(currentNodePosition);
                    newGrid.Add(node);

                    node.name = "Node" + newGrid.Count;
                    //Go to next position in Y
                    currentNodePosition.y += NodeSize;

                    //Increment number of columns
                    currentNbRows++;
                }

                //We can't know for sure how many rows we have for each column.
                //It should be the same every time.
                if (nbRows == 0)
                {
                    nbRows = currentNbRows;
                }
                else
                {
                    Debug.Assert(nbRows == currentNbRows, "Something is wrong with the grid. The amount of columns " +
                                                                "is not the same on each row. Please fix this.");
                }

                //Go to next position in X and reset Y
                currentNodePosition.x += NodeSize;
                currentNodePosition.y = lowerLeftStartingPoint.transform.position.y;

                //Increment number of collunm
                nbColumns++;
            }

            grid = newGrid.ToArray();
            FindAdjacentNodes();
#endif
        }

        private bool CheckIsOnWall(Vector2 position)
        {
            foreach (RaycastHit2D hit in Physics2D.BoxCastAll(position,
                                                              Vector2.one * NodeSize,
                                                              0,
                                                              Vector2.zero))
            {
                if (hit.transform.gameObject.layer == LayerMask.NameToLayer(R.S.Layer.WallDetection))
                {
                    return true;
                }
            }
            return false;
        }

        public Node GetNodeByPoint(Vector2 target)
        {
            for (int i = 0; i < grid.Length; i++)
            {
                if (!grid[i].IsOnWall)
                {
                    float distance = Mathf.Sqrt(Mathf.Pow(target.x - grid[i].Position.x, 2) + Mathf.Pow(target.y - grid[i].Position.y, 2));
                    if (distance <= NodeSize)
                    {
                        return grid[i];
                    }
                }
            }
            for (int i = 0; i < grid.Length; i++)
            {
                if (!grid[i].IsOnWall)
                {
                    float distance = Mathf.Sqrt(Mathf.Pow(target.x - grid[i].Position.x, 2) + Mathf.Pow(target.y - grid[i].Position.y, 2));
                    if (distance <= NodeSize*2)
                    {
                        return grid[i];
                    }
                }
            }
            throw new Exception("there is no node around position " + target);
        }

        private Node GetNodeInGrid(int x, int y)
        {
            return grid[x * nbRows + y];
        }

        /// <summary>
        /// instancie l'ensemble des nodes voisines de chaque nodes
        /// Si une node voisine est null, elle n'est pas insérée
        /// </summary>
        private void FindAdjacentNodes()
        {
            for (int i = 0; i < nbColumns; i++)
            {
                for (int j = 0; j < nbRows; j++)
                {
                    if (GetNodeInGrid(i, j).IsOnWall == false)
                    {
                        List<Node> voisins = new List<Node>();

                        if (j - 1 >= 0)
                        {
                            voisins.Add(GetNodeInGrid(i, j - 1));
                        }

                        if (j + 1 < nbRows)
                        {
                            voisins.Add(GetNodeInGrid(i, j + 1));
                        }

                        if (i - 1 >= 0)
                        {
                            voisins.Add(GetNodeInGrid(i - 1, j));
                        }

                        if (i + 1 < nbColumns)
                        {
                            voisins.Add(GetNodeInGrid(i + 1, j));
                        }

                        if (j - 1 >= 0 && i - 1 >= 0)
                        {
                            voisins.Add(GetNodeInGrid(i - 1, j - 1));
                        }

                        if (j + 1 < nbRows && i + 1 < nbColumns)
                        {
                            voisins.Add(GetNodeInGrid(i + 1, j + 1));
                        }



                        if (j - 1 >= 0 && i + 1 < nbColumns)
                        {
                            voisins.Add(GetNodeInGrid(i + 1, j - 1));
                        }


                        if (j + 1 < nbRows && i - 1 >= 0)
                        {
                            voisins.Add(GetNodeInGrid(i - 1, j + 1));
                        }



                        GetNodeInGrid(i, j).Neighbours = voisins.ToArray();
                    }
                }
            }
        }

        /// <summary>
        /// sort le chemin vers un node spécifique
        /// dois être appelé après avoir appelé UnvisitAllNodes()
        /// </summary>
        /// <param name="target">copie du fogParticle sur le target</param>
        /// <param name="current">copie du fogParticle courrant</param>
        public List<Node> getPath(Node target, Node current, PathfindingMethod method)
        {
            if (method == PathfindingMethod.Astar)
            {
                return getPathAstar(target, current);
            }
            else if (method == PathfindingMethod.Dijkstra)
            {
                return getPathDijkstra(target, current);
            }
            else
            {
                return getPathBasic(target, current);
            }
        }
        #region basic

        /// <summary>
        /// sort le chemin vers un node spécifique
        /// dois être appelé après avoir appelé UnvisitAllNodes()
        /// </summary>
        /// <param name="target">copie du fogParticle sur le target</param>
        /// <param name="current">copie du fogParticle courrant</param>
        public List<Node> getPathBasic(Node target, Node current)
        {
            if (!isAlive)
            {
                return null;
            }
            current.IsVisited = true;

            if (current == target)
            {
                List<Node> temp = new List<Node>();
                temp.Add(current);
                return temp;
            }
            List<Node> OrderedNeighbours = OrderNeighboursBasic(current.Neighbours, target);
            if (current.Neighbours.Length != OrderedNeighbours.Count)
            {
                Debug.Log("error");
            }
            for (int i = 0; i < OrderedNeighbours.Count; i++)
            {
                if (OrderedNeighbours[i].IsOnWall == false && !OrderedNeighbours[i].IsVisited)
                {
                    List<Node> temp = new List<Node>();
                    temp.Add(current);
                    List<Node> tempNext = getPathBasic(target, OrderedNeighbours[i]);
                    if (tempNext != null)
                    {
                        temp.AddRange(tempNext);
                        return temp;
                    }
                }
            }


            return null;
        }

        /// <summary>
        /// retourne la liste des nodes voisins dans l'ordre du plus proche au plus loin comparé au node de destination finale 
        /// </summary>
        /// <param name="neighbours">liste des voisins du node courant</param>
        /// <param name="target">destination finale du chemin</param>
        /// <returns>la liste des nodes voisins dans l'ordre du plus proche au plus loin comparé au node de destination finale</returns>
        private List<Node> OrderNeighboursBasic(Node[] neighbours, Node target)
        {
            double[] neighboursDistances = new double[neighbours.Length];
            for (int i = 0; i < neighboursDistances.Length; i++)
            {
                neighboursDistances[i] = Mathf.Sqrt(Mathf.Pow((neighbours[i].Position.x - target.Position.x), 2) + Mathf.Pow((neighbours[i].Position.y - target.Position.y), 2));
            }
            List<Node> newOrder = new List<Node>();
            //on passe du plus petit au plus grand
            for (int i = 0; i < neighboursDistances.Length; i++)
            {
                double smallestDistance = -1;
                int smallestDistancePosition = -1;
                //on compares les distances pour avoir le prochain plus petit
                for (int j = 0; j < neighboursDistances.Length; j++)
                {
                    bool isOk = true;
                    //on regarde si ce chiffre est déjà dans la liste
                    for (int k = 0; k < newOrder.Count; k++)
                    {
                        if (neighbours[j] == newOrder[k])
                        {
                            isOk = false;
                        }
                    }
                    if (isOk)
                    {
                        if (smallestDistance == -1 || smallestDistance > neighboursDistances[j])
                        {
                            smallestDistance = neighboursDistances[j];
                            smallestDistancePosition = j;
                        }
                    }
                }
                newOrder.Add(neighbours[smallestDistancePosition]);
            }
            return newOrder;
        }
        #endregion

        #region Astar
        /// <summary>
        /// sort le chemin vers un node spécifique avec l'agorythme A*
        /// </summary>
        /// <param name="target">node sur le target</param>
        /// <param name="start">node de départ</param>
        private List<Node> getPathAstar(Node target, Node start)
        {
            // The set of currently discovered nodes that are not evaluated yet.
            // Initially, only the start node is known.
            List<Node> openSet = new List<Node>();
            openSet.Add(start);

            bool[] vistedNodes = new bool[grid.Length];

            // For each node, which node it can most efficiently be reached from.
            // If a node can be reached from many nodes, cameFrom will eventually contain the
            // most efficient previous step.
            Node[] cameFrom = new Node[grid.Length];

            // For each node, the cost of getting from the start node to that node.
            float[] gScore = new float[grid.Length];

            // For each node, the total cost of getting from the start node to the goal
            // by passing by that node. That value is partly known, partly heuristic.
            float[] fScore = new float[grid.Length];
            //initialize all the lists
            for (int i = 0; i < grid.Length; i++)
            {
                cameFrom[i] = null;
                vistedNodes[i] = false;
                if (grid[i] == start)
                {
                    // The cost of going from start to start is zero.
                    gScore[i] = 0;
                    // For the first node, that value is completely heuristic.
                    fScore[i] = getDistance(start.Position, target.Position);
                }
                else
                {
                    gScore[i] = float.MaxValue;
                    fScore[i] = float.MaxValue;
                }
            }

            while (openSet.Count > 0)
            {
                if (!isAlive)
                {
                    return null;
                }
                int currentPosition = findPositionOfSmallest(fScore, vistedNodes);
                Node current = grid[currentPosition];
                if (current == target)
                {
                    return reconstruct_path(cameFrom, current);
                }
                openSet.Remove(current);
                if (dontUseIsVisited)
                {
                    vistedNodes[currentPosition] = true;
                }
                else
                {
                    current.IsVisited = true;
                }
                foreach (Node neighbor in current.Neighbours)
                {
                    int neighborPosition = findPositionInGrid(neighbor);
                    bool isNeighborVisited = false;
                    if (dontUseIsVisited)
                    {
                        isNeighborVisited = vistedNodes[neighborPosition];
                    }
                    else
                    {
                        isNeighborVisited = neighbor.IsVisited;
                    }
                    if (!isNeighborVisited)
                    {
                        if (!isNodeInList(openSet, current))
                        {
                            openSet.Add(current);
                        }
                        float tentative_gScore = gScore[currentPosition] + getDistance(current.Position, neighbor.Position);
                        if (!(tentative_gScore >= gScore[neighborPosition]))
                        {
                            cameFrom[neighborPosition] = current;
                            gScore[neighborPosition] = tentative_gScore;
                            fScore[neighborPosition] = tentative_gScore + getDistance(neighbor.Position, target.Position);
                        }
                    }
                }
            }
            Debug.Log("A* couldn't find the path between " + start.Position + " and " + target.Position);
            throw new Exception("A* couldn't find the path between " + start.Position + " and " + target.Position);
        }


        private List<Node> reconstruct_path(Node[] cameFrom, Node current)
        {
            List<Node> totalPath = new List<Node>();
            totalPath.Add(current);
            while (cameFrom[findPositionInGrid(current)] != null)
            {
                current = cameFrom[findPositionInGrid(current)];
                List<Node> temp = new List<Node>();
                temp.Add(current);
                temp.AddRange(totalPath);
                totalPath = temp;
            }
            return totalPath;
        }

        private int findPositionOfSmallest(float[] fScore, bool[] visitedNodes)
        {
            float smallestYet = float.MaxValue;
            int positionOfSmallest = -1;
            for (int i = 0; i < fScore.Length; i++)
            {
                if (dontUseIsVisited)
                {
                    if (!visitedNodes[i] && !grid[i].IsOnWall && fScore[i] < smallestYet)
                    {
                        smallestYet = fScore[i];
                        positionOfSmallest = i;
                    }
                }
                else
                {
                    if (!grid[i].IsVisited && !grid[i].IsOnWall && fScore[i] < smallestYet)
                    {
                        smallestYet = fScore[i];
                        positionOfSmallest = i;
                    }
                }
            }
            return positionOfSmallest;
        }

        private float getDistance(Vector2 position, Vector2 target)
        {
            return Mathf.Sqrt(Mathf.Pow((position.x - target.x), 2) + Mathf.Pow((position.y - target.y), 2));
        }

        private bool isNodeInList(List<Node> nodes, Node node)
        {
            for (int i = 0; i < nodes.Count; i++)
            {
                if (nodes[i] == node)
                {
                    return true;
                }
            }
            return false;
        }

        private int findPositionInGrid(Node node)
        {
            for (int i = 0; i < grid.Length; i++)
            {
                if (grid[i] == node)
                {
                    return i;
                }
            }
            throw new Exception("the given node (" + node.Position + ") isn't in the grid");
        }

        #endregion

        #region Dijkstra
        /// <summary>
        /// sort le chemin vers un node spécifique
        /// dois être appelé après avoir appelé UnvisitAllNodes()
        /// </summary>
        /// <param name="target">copie du fogParticle sur le target</param>
        /// <param name="current">copie du fogParticle courrant</param>
        public List<Node> getPathDijkstra(Node target, Node current)
        {

            int[] dist = new int[grid.Length];
            Node[] prev = new Node[grid.Length];
            List<Node> nodesList = new List<Node>();

            for (int i = 0; i < grid.Length; i++)
            {
                if (grid[i] == target)
                {
                    dist[i] = 0;
                }
                else
                {
                    dist[i] = int.MaxValue;
                }
                prev[i] = null;
                nodesList.Add(grid[i]);
            }

            int nodeCount = -1;

            while (nodesList.Count != 0)
            {
                if (!isAlive)
                {
                    return null;
                }
                int min = int.MaxValue;
                int index = 0;
                for (int i = 0; i < dist.Length; i++)
                {
                    if (nodesList.Contains(grid[i]) && grid[i].IsOnWall)
                    {
                        nodesList.Remove(grid[i]);
                    }
                    if (dist[i] < min && nodesList.Contains(grid[i]))
                    {
                        min = dist[i];
                        index = i;
                        break;
                    }
                }

                Node selectedNode = grid[index];

                selectedNode.IsVisited = true;

                nodesList.Remove(selectedNode);
                if (nodesList.Count == nodeCount)
                {
                    nodesList.Clear();
                }
                else
                {
                    nodeCount = nodesList.Count;
                }


                foreach (Node neighbor in selectedNode.Neighbours)
                {
                    int distance = dist[index] + GetDistance(selectedNode.Position, neighbor.Position);
                    int indexN = FindIndexInGrid(neighbor);
                    if (distance < dist[indexN] && !neighbor.IsVisited)
                    {
                        dist[indexN] = distance;
                        prev[indexN] = selectedNode;
                    }
                }
            }
            List<Node> path = new List<Node>();

            path.Add(current);

            while (path[path.Count - 1] != target)
            {
                Node toAdd = prev[FindIndexInGrid(path[path.Count - 1])];
                path.Add(toAdd);
            }
            return path;
        }

        private int FindIndexInGrid(Node node)
        {
            int index = -1;
            for (int i = 0; i < grid.Length; i++)
            {
                if (grid[i] == node)
                {
                    index = i;
                }
            }
            return index;
        }


        private int GetDistance(Vector2 pointA, Vector2 pointB)
        {
            int a = (int)(pointB.x - pointA.x);
            int b = (int)(pointB.y - pointA.y);

            return (int)Math.Sqrt(a * a + b * b);
        }
        #endregion
    }
}