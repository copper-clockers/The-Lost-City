﻿using System.Collections.Generic;
using UnityEngine;

namespace ProjetSynthese
{
    /// <summary>
    /// Classe Node pour créer des Noeuds pour la grille de pathfinding.
    /// </summary>
    public class Node : ScriptableObject
    {
        [SerializeField]
        private Vector2 position;

        [SerializeField]
        private bool isOnWall;

        [SerializeField]
        private Node[] neighbours;

        /// <summary>
        /// Vector2 représentant la position du noeud.
        /// </summary>
        public Vector2 Position
        {
            get { return position; }
            set { position = value; }
        }

        /// <summary>
        /// Booléen représentant si le noeud est sur un mur ou non.
        /// </summary>
        public bool IsOnWall
        {
            get { return isOnWall; }
            set { isOnWall = value; }
        }

        /// <summary>
        /// Tableaux de Node représentant les noeuds voisins du noeud concerné.
        /// </summary>
        public Node[] Neighbours
        {
            get { return neighbours; }
            set { neighbours = value; }
        }

        private bool isVisited;

        /// <summary>
        /// Booléen représentant si le noeud à été visité ou non.
        /// </summary>
        public bool IsVisited
        {
            get { return isVisited; }
            set { isVisited = value; }
        }

        /// <summary>
        /// Constructeur de Node
        /// </summary>
        public Node()
        {
            Neighbours = new Node[] { };
        }

        //TODO
        /// <summary>
        /// 
        /// </summary>
        /// <param name="other"></param>
        /// <returns></returns>
        protected bool Equals(Node other)
        {
            return Position.Equals(other.Position) && IsOnWall == other.IsOnWall;
        }

        //TODO
        /// <summary>
        /// 
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((Node) obj);
        }

        //TODO
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode()
        {
            // ReSharper disable once NonReadonlyMemberInGetHashCode
            return position.GetHashCode();
        }

        //TODO
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static bool operator ==(Node left, Node right)
        {
            return Equals(left, right);
        }

        //TODO
        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static bool operator !=(Node left, Node right)
        {
            return !Equals(left, right);
        }
    }
}