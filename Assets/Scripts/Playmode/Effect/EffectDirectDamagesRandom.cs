﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ProjetSynthese
{
    [CreateAssetMenu(fileName = "HitDamageRandom", menuName = "Game/Effects/HitDamageRandom")]
    public class EffectDirectDamagesRandom : Effect
    {
        [SerializeField]
        [Tooltip("Direct damage dealt to target.")]
        private int minimumDamage;

        [SerializeField]
        [Tooltip("Direct damage dealt to target.")]
        private int maximumDamage;

        public override void Apply(GameObject caster, GameObject target)
        {
            int finalDamages = caster.GetComponentInChildren<CharacterStatistics>().CalculateDamageDealt(Random.Range(minimumDamage,maximumDamage));
            target.GetComponentInChildren<KillableObject>().ReceiveDamage(finalDamages);
        }

    }

}

