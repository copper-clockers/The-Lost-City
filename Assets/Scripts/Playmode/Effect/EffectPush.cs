﻿using System.Collections;
using System.Collections.Generic;
using Harmony;
using UnityEngine;

namespace ProjetSynthese
{
    [CreateAssetMenu(fileName = "Push", menuName = "Game/Effects/Push")]
    public class EffectPush : Effect
    {
        [SerializeField]
        [Range(10,10000)]
        [Tooltip("The force at which the object is pushed")]
        protected float forceApplied;

        [SerializeField]
        [Tooltip("Should be the push done backward?")]
        protected bool pushIsBackward;

        public override void Apply(GameObject caster, GameObject target)
        {
            if (target.gameObject == caster.gameObject)
            {
                target.GetComponentInChildren<Rigidbody2D>().AddForce((target.transform.up) * forceApplied * (pushIsBackward ? -1 : 1));
            }
            else
            {
                float angle = Mathf.Atan((caster.transform.position.y - target.transform.position.y) /
                                         (caster.transform.position.x - target.transform.position.x)) * 180 / Mathf.PI + 90f;
                if (target.transform.position.x > caster.transform.position.x)
                {
                    angle += 180;
                }
                target.transform.eulerAngles = new Vector3(0, 0, angle);
                target.GetComponentInChildren<Rigidbody2D>().AddForce((target.transform.up) * forceApplied * (pushIsBackward ? -1 : 1));
            }
        }
    }

}

