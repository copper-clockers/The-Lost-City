﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ProjetSynthese
{
    [CreateAssetMenu(fileName = "HitHealing", menuName = "Game/Effects/HitHealing")]
    public class EffectDirectHeal : Effect
    {
        [SerializeField]
        [Tooltip("Direct damage dealt to target.")]
        private int initialHealing;

        public override void Apply(GameObject caster, GameObject target)
        {
            if (target.GetComponentInChildren<SubmarineController>() == null)
            {
                int finalHealing = caster.GetComponentInChildren<CharacterStatistics>().CalculateDamageDealt(initialHealing);
            target.GetComponentInChildren<KillableObject>().Heal(finalHealing);
            }
            
        }

    }

}

