﻿using System.Collections;
using System.Collections.Generic;
using Harmony;
using UnityEngine;

namespace ProjetSynthese
{
    [CreateAssetMenu(fileName = "LavaEffects", menuName = "Game/Effects/Lava")]
    public class EffectLava : Effect
    {
        [SerializeField]
        [Tooltip("List of all applied effects for a target not immune to lava.")]
        private Effect[] AppliedEffectsNonImmune;
        [SerializeField]
        [Tooltip("List of all applied effects for a target that is not immune and has lava absorbtion ability.")]
        private Effect[] AppliedEffectsAbsorbtion;


        public override void Apply(GameObject caster, GameObject target)
        {
            CharacterStatistics targetStats = target.GetTopParent().GetComponentInChildren<CharacterStatistics>();
            if (!targetStats.IsLavaImmune())
            {
                if (targetStats.CanAbsorbLava())
                {
                    foreach (Effect effect in AppliedEffectsAbsorbtion)
                    {
                        effect.Apply(caster, target);
                    }
                }
                else
                {
                    foreach (Effect effect in AppliedEffectsNonImmune)
                    {
                        effect.Apply(caster, target);
                    }
                }
            }
            
        }
    }

}


