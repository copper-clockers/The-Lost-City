﻿using System.Collections;
using System.Collections.Generic;
using Harmony;
using NSubstitute.Exceptions;
using UnityEngine;

namespace ProjetSynthese
{
    [CreateAssetMenu(fileName = "DamageOverTime", menuName = "Game/Effects/DoT")]
    public class EffectDamageOverTime: Effect
    {
        [SerializeField]
        [Tooltip("Duration of the DoT in number of ticks.")]
        private int totalTickDuration;

        [SerializeField]
        [Tooltip("Time in seconds spent between each DoT tick")]
        private float tickIntervalInSeconds;

        [SerializeField]
        [Range(0, 100)]
        [Tooltip("amount of damage per tick")]
        private int DamagesPerTick;

        public override void Apply(GameObject caster, GameObject target)
        {
            if(target.GetComponentInChildren<KillableObject>() != null)
            {
                KillableObject lifeControl = target.GetComponentInChildren<KillableObject>();
                lifeControl.StartCoroutine(DamageOverTime(
                lifeControl, totalTickDuration, tickIntervalInSeconds, DamagesPerTick));
            }
            
        }

        public IEnumerator DamageOverTime(KillableObject lifeControl, int tickDuration, float tickInverval, int potencity)
        {
            for (int i = 0; i < tickDuration; i++)
            {
                yield return new WaitForSeconds(tickInverval);
               lifeControl.ReceiveDamage(potencity);

            }

        }


    }

}


