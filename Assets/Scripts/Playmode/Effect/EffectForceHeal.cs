﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ProjetSynthese
{
    [CreateAssetMenu(fileName = "ForceHeal", menuName = "Game/Effects/ForceHealing")]
    public class EffectForceHeal : Effect
    {
        [SerializeField]
        [Tooltip("Direct healing dealt to target.")]
        private int initialHealing;

        public override void Apply(GameObject caster, GameObject target)
        {
            if (target.GetComponentInChildren<SubmarineController>() == null)
            {
                int finalHealing = caster.GetComponentInChildren<CharacterStatistics>().CalculateDamageDealt(initialHealing);
            target.GetComponentInChildren<KillableObject>().ForceHeal(finalHealing);
            }
            
        }

    }

}

