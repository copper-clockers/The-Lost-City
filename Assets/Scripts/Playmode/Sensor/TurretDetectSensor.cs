﻿using System;
using System.Collections;
using System.Collections.Generic;
using Harmony;
using UnityEngine;

namespace ProjetSynthese
{
    public delegate void TurretDetectSensorEventHandler(GameObject player);

    [AddComponentMenu("Game/Sensor/TurretDetectSensor")]
    public class TurretDetectSensor : GameScript
    {
        private new Collider2D collider2D;

        public event TurretDetectSensorEventHandler OnDetectTurretEnter;
        public event TurretDetectSensorEventHandler OnDetectTurretExit;

        private void InjectTurretDetectSensor([GameObjectScope] Collider2D collider2D)
        {
            this.collider2D = collider2D;
        }

        private void Awake()
        {
            InjectDependencies("InjectTurretDetectSensor");

            int layer = LayerMask.NameToLayer(R.S.Layer.TurretDetectSensor);
            if (layer == -1)
            {
                throw new Exception("In order to use a PlayerDetect, you must have a " + R.S.Layer.TurretDetectSensor + " layer.");
            }
            gameObject.layer = layer;
            collider2D.isTrigger = true;
        }

        public void TurretDetectEnter(GameObject turret)
        {
            if (OnDetectTurretEnter != null) OnDetectTurretEnter(turret);
        }

        public void TurretDetectExit(GameObject turret)
        {
            if (OnDetectTurretExit != null) OnDetectTurretExit(turret);
        }
    }

}


