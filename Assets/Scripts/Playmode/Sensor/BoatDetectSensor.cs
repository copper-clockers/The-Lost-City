﻿using System;
using Harmony;
using UnityEngine;

namespace ProjetSynthese
{
    public delegate void BoatSensorEventHandler(GameObject boat);

    [AddComponentMenu("Game/Sensor/BoatDetectSensor")]
    public class BoatDetectSensor : GameScript
    {
        private new Collider2D collider2D;

        public event BoatSensorEventHandler OnDetectBoatEnter;
        public event BoatSensorEventHandler OnDetectBoatExit;


        private void InjectBoatDetectSensor([GameObjectScope] Collider2D collider2D)
        {
            this.collider2D = collider2D;
        }

        private void Awake()
        {
            InjectDependencies("InjectBoatDetectSensor");

            int layer = LayerMask.NameToLayer(R.S.Layer.BoatDetectSensor);
            if (layer == -1)
            {
                throw new Exception("In order to use a BoatDetect, you must have a " + R.S.Layer.BoatDetectSensor + " layer.");
            }
            gameObject.layer = layer;
            collider2D.isTrigger = true;
        }

        public void BoatDetectEnter(GameObject boat)
        {
            if (OnDetectBoatEnter != null) OnDetectBoatEnter(boat);
        }

        public void BoatDetectExit(GameObject boat)
        {
            if (OnDetectBoatExit != null) OnDetectBoatExit(boat);
        }
    }
}
