﻿using System;
using System.Collections;
using System.Collections.Generic;
using Harmony;
using UnityEngine;

namespace ProjetSynthese
{
    public delegate void ExtractorDetectSensorEventHandler(GameObject player);

    [AddComponentMenu("Game/Sensor/ExtractorDetectSensor")]
    public class ExtractorDetectSensor : GameScript
    {
        private new Collider2D collider2D;

        public event ExtractorDetectSensorEventHandler OnDetectExtractorEnter;
        public event ExtractorDetectSensorEventHandler OnDetectExtractorExit;


        private void InjectExtractorDetectSensor([GameObjectScope] Collider2D collider2D)
        {
            this.collider2D = collider2D;
        }

        private void Awake()
        {
            InjectDependencies("InjectExtractorDetectSensor");

            int layer = LayerMask.NameToLayer(R.S.Layer.ExtractorDetectSensor);
            if (layer == -1)
            {
                throw new Exception("In order to use a PlayerDetect, you must have a " + R.S.Layer.ExtractorDetectSensor + " layer.");
            }
            gameObject.layer = layer;
            collider2D.isTrigger = true;
        }

        public void ExtractorDetectEnter(GameObject extractor)
        {
            if (OnDetectExtractorEnter != null) OnDetectExtractorEnter(extractor);
        }

        public void ExtractorDetectExit(GameObject extractor)
        {
            if (OnDetectExtractorExit != null) OnDetectExtractorExit(extractor);
        }
    }
}


