﻿using System.Collections;
using System.Collections.Generic;
using Harmony;
using UnityEngine;

namespace ProjetSynthese
{
    [AddComponentMenu("Game/Aspect/SpawnLeviathan")]
    public class SpawnLeviathan : GameScript
    {

        [SerializeField]
        [Tooltip("Patrol points for Leviathan in current map")]
        private GameObject[] patrolPoints;

        [SerializeField]
        [Tooltip("Patrol points for Leviathan in current map")]
        private GameObject leviathanPrefab;

        [SerializeField]
        [Tooltip("Initial delay before Leviathan appears in the world")]
        private float timeBeforeInitialSpawn;

        [SerializeField]
        [Tooltip("Delay after death before Leviathan spawns again.")]
        private float respawnTime;

        [SerializeField]
        [Tooltip("a game object with the collider of the whole roaming area")]
        private GameObject roamingAreaGameObject;

        private GameObject leviathan;
        private KillableObject leviathanHealth;
        private MusicController musicController;

        private bool mustNotSwitchMusicOnFirstSpawn;

        private void InjectSpawnLeviathan([SceneScope] MusicController musicController)
        {
            this.musicController = musicController;
        }

        private void Awake()
        {
            InjectDependencies("InjectSpawnLeviathan");
        }

        private void OnEnable()
        {
            mustNotSwitchMusicOnFirstSpawn = timeBeforeInitialSpawn <=5;
            StartCoroutine(SpawnLeviathanAfterDelay(timeBeforeInitialSpawn));

        }

        private void OnLeviathanDeath()
        {
            leviathanHealth.OnDeath -= OnLeviathanDeath;
            StartCoroutine(SpawnLeviathanAfterDelay(respawnTime));
            if (mustNotSwitchMusicOnFirstSpawn)
            {
                mustNotSwitchMusicOnFirstSpawn = false;
            }
            else
            {
                musicController.LeviathanDeath();
            }
        }

        private IEnumerator SpawnLeviathanAfterDelay(float delay)
        {
            yield return new WaitForSeconds(delay);
            leviathan = Instantiate(leviathanPrefab, transform.position, transform.rotation);
            leviathan.GetComponentInChildren<LeviathanController>().SetPatrolPoints(patrolPoints);
            leviathan.GetComponentInChildren<LeviathanController>().SetRoamingArea(roamingAreaGameObject);
            leviathanHealth = leviathan.GetComponentInChildren<KillableObject>();
            leviathanHealth.OnDeath += OnLeviathanDeath;
            if (!mustNotSwitchMusicOnFirstSpawn)
            {
                musicController.EncounterLeviathan();
            }
        }

    }

}


