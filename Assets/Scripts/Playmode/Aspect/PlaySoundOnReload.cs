﻿using System.Collections;
using System.Collections.Generic;
using Harmony;
using UnityEngine;


namespace ProjetSynthese
{
    public class PlaySoundOnReload : GameScript
    {
        [SerializeField]
        [Tooltip("The audio played when this weapon fires.")]
        private AudioSource reloadSound;

        private RangedWeapon weapon;

        private void InjectPlaySoundOnReload([GameObjectScope] RangedWeapon weapon)
        {
            this.weapon = weapon;
        }

        private void Awake()
        {
            InjectDependencies("InjectPlaySoundOnReload");
        }

        private void OnEnable()
        {
            weapon.OnReloading += OnReload;
            weapon.OnReloadEnding += OnReloadEnd;

        }

        private void OnDisable()
        {
            weapon.OnReloading -= OnReload;
            weapon.OnReloadEnding -= OnReloadEnd;
        }

        private void OnReload()
        {

            if (reloadSound.isPlaying)
            {
                reloadSound.Stop();
            }
            reloadSound.Play();
        }
        private void OnReloadEnd()
        {
            if (reloadSound.isPlaying)
            {
                reloadSound.Stop();
            }
        }
    }


}
