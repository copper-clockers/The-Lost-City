﻿using System.Collections;
using System.Collections.Generic;
using Harmony;
using ProjetSynthese;
using UnityEngine;

namespace ProjetSynthese
{
    public class BleedOnHealthLost : GameScript
    {
        [SerializeField]
        [Tooltip("Bleed particle effect")]
        private GameObject bleedEffect;

        private KillableObject healthControl;
        private int oldhealth = 0;

        private void InjectBleedOnHealthLost([EntityScope] KillableObject healthControl)
        {
            this.healthControl = healthControl;
        }

        private void Awake()
        {
            InjectDependencies("InjectBleedOnHealthLost");
        }

        private void OnEnable()
        {
            healthControl.OnHealthChanged += Bleed;
            healthControl.OnArmorChanged += Bleed;
        }

        private void OnDisable()
        {
            healthControl.OnHealthChanged -= Bleed;
            healthControl.OnArmorChanged -= Bleed;
        }

        private void Bleed(int current, int maximum)
        {
            if (oldhealth > current)
            {
                Instantiate(bleedEffect, transform);
            }
            oldhealth = current;
        }
    }

}


