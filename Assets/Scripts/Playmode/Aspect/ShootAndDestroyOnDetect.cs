﻿using System.Collections;
using System.Collections.Generic;
using Harmony;
using UnityEngine;

namespace ProjetSynthese
{
    [AddComponentMenu("Game/Aspect/ShootAndDestroyOnDetect")]
    public class ShootAndDestroyOnDetect : GameScript
    {
        private EntityDestroyer death;
        private RangedWeapon rangedWeapon;
        private TargetFinderSensor targetFinder;

        public void InjectSplitAfterDelay([EntityScope] EntityDestroyer entityDestroyer, 
            [EntityScope] RangedWeapon rangedWeapon,
            [EntityScope] TargetFinderSensor targetFinder)
        {
            death = entityDestroyer;
            this.rangedWeapon = rangedWeapon;
            this.targetFinder = targetFinder;
        }

        private void Awake()
        {
            InjectDependencies("InjectSplitAfterDelay");
        }

        private void OnEnable()
        {
            targetFinder.OnTargetFound += OnTargrtFound;
        }

        private void OnDisable()
        {
            targetFinder.OnTargetFound -= OnTargrtFound;
        }

        private void OnTargrtFound(GameObject target)
        {
            rangedWeapon.Fire();
            death.Destroy();
        }
    }
}
