﻿using System.Collections;
using System.Collections.Generic;
using Harmony;
using UnityEngine;

namespace ProjetSynthese
{
    public delegate void SpawnerReadyEventHandler(GameObject spawner);

    [AddComponentMenu("Game/Aspect/SpawnEnnemiAtInterval")]
    public class SpawnEnnemiAtInterval : GameScript
    {
        public event SpawnerReadyEventHandler OnSpawnerReady;

        [SerializeField]
        [Tooltip("Monsters to spawn")]
        private GameObject[] monsters;

        [SerializeField]
        [Tooltip("Time between each spawn")]
        private float interval;

        [SerializeField]
        [Tooltip("Maximum number of spawned monsters that can be alive")]
        private int nbMonstersMax;

        [SerializeField]
        [Tooltip("Will spawn the first ennemy as soon as the path si done")]
        private bool spawnImmediatlyWhenReady;

        private List<GameObject> spawnedMonsters;

        private Vector2 submarinePosition;

        private float currentTime;

        private List<Node> monsterPath;

        private AiPath pathfinder;

        private PathfindingMethod pathfindingMethod;

        private Random rnd;

        private void InjectMonsterSpawner([TagScope(R.S.Tag.Submarine)] GameObject submarine, [GameObjectScope] AiPath pathfinder)
        {
            this.submarinePosition = submarine.transform.position;
            this.pathfinder = pathfinder;
        }

        private void Awake()
        {
            InjectDependencies("InjectMonsterSpawner");
            spawnedMonsters = new List<GameObject>();
            currentTime = 0;
            if (spawnImmediatlyWhenReady)
            {
                currentTime = interval;
            }
        }

        private void Start()
        {
            pathfinder.SetNewPath(submarinePosition);
        }

        private void Update()
        {
            if (monsterPath != null)
            {
                RemoveDeadMonsters();
                if (spawnedMonsters.Count < nbMonstersMax)
                {
                    currentTime += Time.deltaTime;

                    if (currentTime >= interval)
                    {
                        Random.InitState(System.DateTime.Now.Millisecond);
                        int noMonsterToSpawn = Random.Range(0, monsters.Length);

                        GameObject currentMonster = Instantiate(monsters[noMonsterToSpawn], gameObject.transform.position, gameObject.transform.rotation);
                        currentMonster.GetComponentInChildren<AiPath>().InsertPath(monsterPath);
                        spawnedMonsters.Add(currentMonster);
                        currentTime = 0;
                    }
                }
            }
            else
            {
                CheckIfPathDone();
            }
        }

        private void CheckIfPathDone()
        {
            if (!pathfinder.IsThreadRunning())
            {
                monsterPath = pathfinder.currentPath;
                if (monsterPath == null)//failsafe
                {
                    Debug.Log("Spawner \"" + gameObject.name + "\" failed to make a path, restarting path");

                    pathfinder.SetNewPath(submarinePosition);
                }
                else
                {
                    if (OnSpawnerReady != null) OnSpawnerReady(this.gameObject);
                    Debug.Log("Spawner \"" + gameObject.name + "\" Ready");
                }
            }
        }

        private void RemoveDeadMonsters()
        {
            List<GameObject> monstersToRemove = new List<GameObject>();

            for (int i = 0; i < spawnedMonsters.Count; i++)
            {
                if (spawnedMonsters[i] == null)
                {
                    monstersToRemove.Add(spawnedMonsters[i]);
                }
            }
            for (int i = 0; i < monstersToRemove.Count; i++)
            {
                spawnedMonsters.Remove(monstersToRemove[i]);
            }
        }
    }
}
