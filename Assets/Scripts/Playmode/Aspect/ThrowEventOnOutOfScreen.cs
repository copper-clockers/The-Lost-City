﻿using System.Collections;
using System.Collections.Generic;
using Harmony;
using UnityEngine;

namespace ProjetSynthese
{
    public delegate void OutOfScreenEventHandler();

    [AddComponentMenu("Game/Aspect/ThrowEventOnOutOfScreen")]
    public class ThrowEventOnOutOfScreen : GameScript
    {

        public event OutOfScreenEventHandler OutOfScreen;

        private void OnBecameInvisible()
        {
            if (OutOfScreen != null) OutOfScreen();
        }

    }
}
