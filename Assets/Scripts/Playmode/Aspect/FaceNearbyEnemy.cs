﻿using System.Collections;
using System.Collections.Generic;
using Harmony;
using UnityEngine;

namespace ProjetSynthese
{
    public class FaceNearbyEnemy : GameScript
    {
        private const float FaceTargetUpdateRate = 0.2f;

        [SerializeField]
        [Tooltip("Degree variation: Sometimes a 90 degree fix is required.")]
        private float zVariationCorrection;

        private TargetFinderSensor targetFinder;

        private GameObject target;

        private Coroutine faceTargetCoroutine;

        private void InjectFaceNearbyEnemy([GameObjectScope] TargetFinderSensor targetFinder)
        {
            this.targetFinder = targetFinder;
        }

        private void Awake()
        {
            InjectDependencies("InjectFaceNearbyEnemy");
        }

        private void OnEnable()
        {
            targetFinder.OnTargetFound += OnTargetGained;
            targetFinder.OnTargetLost += OnTargetLost;
        }

        private void OnDisable()
        {
            targetFinder.OnTargetFound -= OnTargetGained;
            targetFinder.OnTargetLost -= OnTargetLost;
        }

        private void OnTargetGained(GameObject target)
        {
            this.target = target;
            faceTargetCoroutine = StartCoroutine(FaceTarget());
        }

        private void OnTargetLost(GameObject target)
        {
            StopCoroutine(faceTargetCoroutine);
            this.target = target;
            
        }

        public GameObject GetTarget()
        {
            return target;
        }

        private void UpdateAngle()
        {
            if (target != null)
            {
                float angle = Mathf.Atan((target.transform.position.y - transform.position.y) /
                                         (target.transform.position.x - transform.position.x)) * 180 / Mathf.PI + zVariationCorrection;
                if (transform.position.x > target.transform.position.x)
                {
                    angle += 180;
                }
                transform.parent.eulerAngles = new Vector3(0, 0, angle);
            }
        }

        private IEnumerator FaceTarget()
        {
            
            while (true)
            {
                UpdateAngle();
                yield return new WaitForSeconds(FaceTargetUpdateRate);
            }
        }


    }


}

