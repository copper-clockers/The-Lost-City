﻿using System.Collections;
using System.Collections.Generic;
using Harmony;
using UnityEngine;


namespace ProjetSynthese
{
    public class PlaySoundOnFire : GameScript
    {
        [SerializeField]
        [Tooltip("The audio played when this weapon fires.")]
        private AudioSource fireSound;

        private RangedWeapon weapon;

        private void InjectPlaySoundOnFire([GameObjectScope] RangedWeapon weapon)
        {
            this.weapon = weapon;
        }

        private void Awake()
        {
            InjectDependencies("InjectPlaySoundOnFire");
        }

        private void OnEnable()
        {
            weapon.OnBulletShot += OnFire;
        }

        private void OnDisable()
        {
            weapon.OnBulletShot -= OnFire;
        }

        private void OnFire()
        {

            if (fireSound.isPlaying)
            {
                fireSound.Stop();
            }
            fireSound.Play();
        }
    }


}
