﻿using UnityEngine;
using Harmony;

namespace ProjetSynthese
{
    public class PositionningCavasForPlayers : GameScript
    {
        private float xAxis = 0.5f;

        private float yAxis;

        [SerializeField]
        [Tooltip("If there is only one player, it's this Y axis that will be assigned to the canvas position.")]
        private float yAxisFor1Player;

        [SerializeField]
        [Tooltip("If there is two players or more, it's this Y axis that will be assigned to the canvas position.")]
        private float yAxisFor2PlayersOrMore;
        
        private RectTransform rectTransform;

        private GameActivityParameters gameActivityParameters;

        private int numberOfPlayers;

        private void InjectDrawCanvas([ApplicationScope] GameActivityParameters gameActivityParameters)
        {
            this.gameActivityParameters = gameActivityParameters;
        }

        private void Awake()
        {
            InjectDependencies("InjectDrawCanvas");
            rectTransform = GetComponent<RectTransform>();
            numberOfPlayers = gameActivityParameters.NumberOfPlayers;

            AssignTheRightYAxis();
        }

        private void Start()
        {
            rectTransform.anchoredPosition= new Vector2(rectTransform.anchoredPosition.x,yAxis);
        }

        private void AssignTheRightYAxis()
        {
            if (numberOfPlayers == 1)
            {
                rectTransform.anchorMin = new Vector2(0.5f,1);
                rectTransform.anchorMax = new Vector2(0.5f, 1);
                yAxis = -30;
            }
            else if (numberOfPlayers > 1)
            {
                rectTransform.anchorMin = new Vector2(0.5f, 0.5f);
                rectTransform.anchorMax = new Vector2(0.5f, 0.5f);
                yAxis = 0;
            }
        }
    }
}

