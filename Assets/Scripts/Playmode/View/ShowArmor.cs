﻿using UnityEngine;
using UnityEngine.UI;

namespace ProjetSynthese
{
    public class ShowArmor : GameScript
    {
        private KillableObject killableObject;

        private Image childImage;

        private GameObject assignedPlayer;

        public GameObject AssignedPlayer
        {
            get { return assignedPlayer; }
            set
            {
                assignedPlayer = value;
                killableObject = assignedPlayer.GetComponentInChildren<KillableObject>();
                killableObject.OnArmorChanged += ManagePlayerArmor;
                childImage = GetComponentInChildren<Image>();
                maxArmor = killableObject.GetCurrentMaxArmor();
                currentArmor = maxArmor;
            }
        }

        private int currentArmor;

        private int maxArmor;

        private void OnDestroy()
        {
            if (killableObject != null)
            {
                killableObject.OnArmorChanged -= ManagePlayerArmor;
            }
        }

        private void ManagePlayerArmor(int currentArmor, int uselessParameter)
        {
            float newCurrentArmor = (float)currentArmor;
            float newMaxArmor = (float)maxArmor;

            this.currentArmor = currentArmor;
            childImage.fillAmount = newCurrentArmor / newMaxArmor;
        }
    }
}
