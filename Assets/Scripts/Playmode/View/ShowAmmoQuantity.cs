﻿using UnityEngine;
using UnityEngine.UI;

namespace ProjetSynthese
{
    public class ShowAmmoQuantity : GameScript
    {
        private GameObject assignedPlayer;
        public GameObject AssignedPlayer
        {
            get { return assignedPlayer; }
            set
            {
                assignedPlayer = value;
                playerController = assignedPlayer.GetComponentInChildren<PlayerController>();
                childText = GetComponentInChildren<Text>();
                mainWeapon = playerController.GetMainWeapon;
                subWeapon = playerController.GetSubWeapon;

                mainWeapon.OnAmmoChanged += ShowAmmo;
                subWeapon.OnAmmoChanged += ShowAmmo;
            }
        }

        private PlayerController playerController;
        private RangedWeapon mainWeapon;
        private RangedWeapon subWeapon;

        private Text childText;

        private int currentClipAmmunition;

        private int clipSize;

        private void OnDestroy()
        {
            if (mainWeapon != null)
            {
                mainWeapon.OnAmmoChanged -= ShowAmmo;
            }
            if (subWeapon != null)
            {
                subWeapon.OnAmmoChanged -= ShowAmmo;
            }
        }

        private void ShowAmmo(int currentClipAmmunition, int clipSize, int totalAmmunition)
        {
            if (AssignedPlayer.name == "Specter(Clone)")
            {
                childText.text = totalAmmunition.ToString();
            }
            else
            {
                childText.text = totalAmmunition.ToString() + " " + currentClipAmmunition.ToString() + "/" + clipSize.ToString();
            }
        }
    }
}
