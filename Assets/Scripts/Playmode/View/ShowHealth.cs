﻿using UnityEngine;
using UnityEngine.UI;

namespace ProjetSynthese
{
    public class ShowHealth : GameScript
    {
        private KillableObject killableObject;

        private Image childImage;

        private Text childText;

        private GameObject assignedPlayer;

        public GameObject AssignedPlayer
        {
            get { return assignedPlayer; }
            set
            {
                assignedPlayer = value;

                killableObject = assignedPlayer.GetComponentInChildren<KillableObject>();
                killableObject.OnHealthChanged += ManagePlayerHealth;
                childImage = GetComponentInChildren<Image>();
                childText = GetComponentInChildren<Text>();
                maxHealth = killableObject.GetCurrentMaxHealth();
                currentHealth = maxHealth;
                childText.text = currentHealth.ToString() + "/" + maxHealth.ToString();
            }
        }
        
        private int currentHealth;

        private int maxHealth;

        private void OnDestroy()
        {
            if (killableObject != null)
            {
                killableObject.OnHealthChanged -= ManagePlayerHealth;
            }
        }

        private void ManagePlayerHealth(int currentHealth, int uselessParameter)
        {
            float newCurrentHealth = (float)currentHealth;
            float newMaxHealth = (float)maxHealth;

            this.currentHealth = currentHealth;
            childImage.fillAmount = newCurrentHealth / newMaxHealth;
            childText.text = currentHealth.ToString() + " / " + maxHealth.ToString();
        }
    }
}