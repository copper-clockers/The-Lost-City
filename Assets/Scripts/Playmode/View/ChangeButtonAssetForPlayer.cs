﻿using UnityEngine;
using UnityEngine.UI;
using Harmony;

namespace ProjetSynthese
{
    public class ChangeButtonAssetForPlayer : GameScript
    {
        [SerializeField]
        [Tooltip("The sprite assigned to the A button for the Scientist")]
        private Sprite buttonAScientist;

        [SerializeField]
        [Tooltip("The sprite assigned to the X button for the Scientist")]
        private Sprite buttonXScientist;


        [SerializeField]
        [Tooltip("The sprite assigned to the A button for the Specter")]
        private Sprite buttonASpecter;

        [SerializeField]
        [Tooltip("The sprite assigned to the X button for the Specter")]
        private Sprite buttonXSpecter;


        [SerializeField]
        [Tooltip("The sprite assigned to the A button for the Machinist")]
        private Sprite buttonAMachinist;

        [SerializeField]
        [Tooltip("The sprite assigned to the X button for the Machinist")]
        private Sprite buttonXMachinist;


        [SerializeField]
        [Tooltip("The sprite assigned to the A button for the Enforcer")]
        private Sprite buttonAEnforcer;

        [SerializeField]
        [Tooltip("The sprite assigned to the X button for the Enforcer")]
        private Sprite buttonXEnforcer;


        private Image imageButtonA;
        private Image imageButtonX;

        private GameObject assignedPlayer;
        public GameObject AssignedPlayer
        {
            get { return assignedPlayer; }
            set
            {
                assignedPlayer = value;
                InjectDependencies("InjectChangeButtonAssetForPlayer");
                AssignRightSprite();
            }
        }

        private void InjectChangeButtonAssetForPlayer([Named(R.S.GameObject.ImageButtonA)][ChildScope] Image imageButtonA,
                                                      [Named(R.S.GameObject.ImageButtonX)][ChildScope] Image imageButtonX)
        {
            this.imageButtonA = imageButtonA;
            this.imageButtonX = imageButtonX;
        }

        private void AssignRightSprite()
        {
            if (assignedPlayer.name == "Scientist(Clone)")
            {
                imageButtonA.sprite = buttonAScientist;
                imageButtonX.sprite = buttonXScientist;
            }
            else if (assignedPlayer.name == "Specter(Clone)")
            {
                imageButtonA.sprite = buttonASpecter;
                imageButtonX.sprite = buttonXSpecter;
            }
            else if (assignedPlayer.name == "Machinist(Clone)")
            {
                imageButtonA.sprite = buttonAMachinist;
                imageButtonX.sprite = buttonXMachinist;
            }
            else if (assignedPlayer.name == "Enforcer(Clone)")
            {
                imageButtonA.sprite = buttonAEnforcer;
                imageButtonX.sprite = buttonXEnforcer;
            }
        }

    }
}