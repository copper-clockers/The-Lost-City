﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ProjetSynthese
{
    public abstract class ReloadableWeapon : RangedWeapon
    {
        [SerializeField]
        [Tooltip("number of bullets the gun have.")]
        protected int clip;

        [SerializeField]
        [Tooltip("The time it takes to reload once you run out of ammo.")]
        protected float reloadDelay;

        protected int currentClip;

        protected new void Start()
        {
            currentClip = clip;
            base.Start();
            if (!IsAmmunitionInfinite)
            {
                currentAmmunition -= clip;
            }
            UpdateAmmunitionQuantity();
            
        }

        public override void UpdateAmmunitionQuantity()
        {
            NotifyAmmunitionChanged(currentAmmunition, clip, currentClip);
        }

        protected bool isReloading = false;
        

        protected void Reload()
        {
            StartCoroutine(ReloadWeapon());
        }

        private IEnumerator ReloadWeapon()
        {
            isReloading = true;
            NotifyReload();
            yield return new WaitForSeconds(reloadDelay);
            isReloading = false;
            NotifyReloadEnding();
            currentClip = (currentAmmunition < clip ? currentAmmunition : clip);
            currentAmmunition -= currentClip;
            NotifyAmmunitionChanged(currentAmmunition, clip, currentClip);
        }

    }
}


