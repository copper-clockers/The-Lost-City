﻿using System.Collections;
using System.Collections.Generic;
using Harmony;
using UnityEngine;

namespace ProjetSynthese
{
    public delegate void IsFiringEventHandler();

    public delegate void AmmunitionCountEventHandler(int currentClipAmmunition, int clipSize, int totalAmmunition);

    public abstract class RangedWeapon : GameScript
    {
        

        [SerializeField]
        [Tooltip("The delay (in seconds) between two gun shots.")]
        protected float fireRate;

        [SerializeField]
        [Range(0, 360)]
        [Tooltip("The weapon innaccuracy. 0 means perfect accuracy. 360 means that it could fire in any random direction, really.")]
        protected float spread;

        [SerializeField]
        [Range(1, 24)]
        [Tooltip("number of projectiles shot per tick.")]
        protected float multiFire = 1;

        [SerializeField]
        [Tooltip("The ammunition carried initially.")]
        protected int maximalAmmunition;

        [SerializeField]
        [Tooltip("The metal cost to refill ammunition")]
        protected int refillMetalCost;

        [SerializeField]
        [Tooltip("Check this boolean if you want the weapon to have ulimited ammo. Use only for NPCs or testing")]
        protected bool isAmmunitionInfinite = false;

        protected int currentAmmunition;

        public event IsFiringEventHandler OnWeaponFire;
        public event IsFiringEventHandler OnBulletShot;
        public event IsFiringEventHandler OnReloading;
        public event IsFiringEventHandler OnReloadEnding;
        public event AmmunitionCountEventHandler OnAmmoChanged;

        protected bool hasFired = false;

        protected CharacterStatistics stats;

        protected bool IsAmmunitionInfinite
        {
            get { return (stats.HasInfiniteAmmunition()); }
        }

        protected void Start()
        {
            //Obtaining the character stats is done here instead of in the injector because there's the need of a delay
            //The "CharacterStatistics simply isn't available yet on Awake();
            stats = gameObject.GetTopParent().GetComponentInChildren<CharacterStatistics>();
            currentAmmunition = maximalAmmunition;
            VerifyIfInfiniteAmmunition();
        }

        public abstract void Fire();

        public int GetRefillAmmoCost()
        {
            if (currentAmmunition == maximalAmmunition)
            {
                return 0;
            }
            return refillMetalCost;
        }

        public int RefillAmmunition()
        {
            if (currentAmmunition == maximalAmmunition)
            {
                return 0;
            }
            currentAmmunition = maximalAmmunition;
            return refillMetalCost;
        }

        public abstract void UpdateAmmunitionQuantity();

        protected void VerifyIfInfiniteAmmunition()
        {
            if (isAmmunitionInfinite)
            {
                stats.AddInfiniteAmmunitionBuff();
            }
        }

        protected void NotifyIsFiring()
        {
            if (OnWeaponFire != null) OnWeaponFire();
        }
        protected void NotifyBulletShot()
        {
            if (OnBulletShot != null) OnBulletShot();
        }

        protected void NotifyReload()
        {
            if (OnReloading != null) OnReloading();
        }

        protected void NotifyReloadEnding()
        {
            if (OnReloadEnding != null) OnReloadEnding();
        }

        protected void NotifyAmmunitionChanged(int currentTotalAmmunition, int clipSize = 0, int currentClipAmmunition = 0)
        {
            if (OnAmmoChanged != null) OnAmmoChanged(currentClipAmmunition,clipSize,currentTotalAmmunition);
        }

    }

}


