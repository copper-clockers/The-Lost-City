﻿using System.Collections;
using System.Collections.Generic;
using Harmony;
using ProjetSynthese;
using UnityEngine;

namespace ProjetSynthese
{

    [AddComponentMenu("Game/Actuator/BurstGun")]
    public class BurstGun : ReloadableWeapon
    {
        [SerializeField]
        [Range(2, 5)]
        [Tooltip("Number of bullets in the burst.")]
        private int burstSize;
        [SerializeField]
        [Tooltip("Delay between bullets of a burst weapon")]
        private float burstDelayInSeconds;

        

        private ProjectileShooter guntip;

        private void InjectGun([GameObjectScope] ProjectileShooter guntip)
        {
            this.guntip = guntip;
        }

        private void Awake()
        {
            InjectDependencies("InjectGun");
        }

        public override void Fire()
        {
            if (!hasFired && !isReloading)
            {
                StartCoroutine(FireWeapon());
            }
        }

        private IEnumerator FireWeapon()
        {

            if (currentClip > 0 || IsAmmunitionInfinite)
            {
                
                hasFired = true;
                NotifyIsFiring();
                for (int j = 0; j < burstSize; j++)
                {
                    NotifyBulletShot();
                    for (int i = 0; i < multiFire; i++)
                    {
                        GameObject bullet = guntip.Fire();
                        bullet.transform.Rotate(0, 0, Random.Range(-spread / 2f, spread / 2f));
                        bullet.GetComponentInChildren<CharacterStatistics>().ReceiveDamageClone(stats.CloneDamageBonus());

                    }
                    currentClip -= (IsAmmunitionInfinite) ? 0 : 1;
                    NotifyAmmunitionChanged(currentAmmunition, clip, currentClip);
                    yield return new WaitForSeconds(burstDelayInSeconds);
                    
                }
                yield return new WaitForSeconds(fireRate);
            }
            else if (currentAmmunition > 0)
            {
                Reload();
            }
            yield return new WaitForSeconds(fireRate);
            hasFired = false;
        }


    }
}
