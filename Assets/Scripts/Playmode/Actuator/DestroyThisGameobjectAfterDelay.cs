﻿using System.Collections;
using System.Collections.Generic;
using ProjetSynthese;
using UnityEngine;

namespace ProjetSynthese
{
    public class DestroyThisGameobjectAfterDelay : GameScript
    {
        [SerializeField]
        [Tooltip("Delay before destroying the component")]
        private float delay;

        private void Start()
        {
            StartCoroutine(DestructionCountDown(delay));
        }

        private IEnumerator DestructionCountDown(float delay)
        {
            yield return new WaitForSeconds(delay);
            Destroy(gameObject);
        }
    }

}


