﻿using System.Collections;
using System.Collections.Generic;
using Harmony;
using ProjetSynthese;
using UnityEngine;

namespace ProjetSynthese
{

    [AddComponentMenu("Game/Actuator/Gun")]
    public class Gun : ReloadableWeapon
    {
        

        private ProjectileShooter guntip;

        private void InjectGun([GameObjectScope] ProjectileShooter guntip)
        {
            this.guntip = guntip;
            
        }

        private void Awake()
        {
            InjectDependencies("InjectGun");
        }

        public override void Fire()
        {
            if (!hasFired && !isReloading)
            {
                StartCoroutine(FireWeapon());
            }
        }
        
        private IEnumerator FireWeapon()
        {
            
            if (currentClip > 0 || IsAmmunitionInfinite)
            {
                hasFired = true;
                NotifyIsFiring();
                NotifyBulletShot();
                for (int i = 0; i < multiFire; i++)
                {
                    GameObject bullet = guntip.Fire();
                    bullet.transform.Rotate(0, 0, Random.Range(-spread / 2f, spread / 2f));
                    bullet.GetComponentInChildren<CharacterStatistics>().ReceiveDamageClone(stats.CloneDamageBonus());

                }
                currentClip -= (IsAmmunitionInfinite) ? 0 : 1;
                NotifyAmmunitionChanged(currentAmmunition, clip, currentClip);
            }
            else if(currentAmmunition > 0)
            {
                Reload();
            }
            yield return new WaitForSeconds(fireRate);
            hasFired = false;
        }

        
    }
}
